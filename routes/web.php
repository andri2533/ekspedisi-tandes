<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get("master/login-from-user", function(){
// 	dd("SA");
// });
Route::get("master/login-from-user", "HomeController@loginFromUser");

Auth::routes();
/*Master Data untuk keperluan Authentication*/
	Route::group(['middleware' => 'auth'], function () {
		Route::get('/', 'HomeController@index');
		Route::get('home', 'HomeController@index')->name('home');
		Route::get('master/user', 'HomeController@user')->name('master.user');
		Route::get('master/user/edit/{id}', 'HomeController@edituser')->name('master.user');
		Route::post('master/user/edit/{id}', 'HomeController@edituser')->name('master.user.edit');
		Route::get('master/user/create', 'HomeController@createuser')->name('master.create');
		Route::post('master/user/create', 'HomeController@createuser')->name('master.create');
		Route::post('master/user/delete/{id}', 'HomeController@deleteUser');

		Route::get('master/user/profil/{id}', 'HomeController@edituser');
		Route::post('master/user/profil/{id}', 'HomeController@edituser');

		Route::get('master/permission', 'HomeController@permission')->name('master.permission');
		Route::get('master/permission/create', 'HomeController@createPermission')->name('master.permission.create');
		Route::post('master/permission/create', 'HomeController@postPermission');
		Route::get('master/permission/edit/{id}', 'HomeController@editPermission')->name('master.permission.edit');
		Route::post('master/permission/edit/{id}', 'HomeController@postPermission');
		Route::post('master/permission/delete/{id}', 'HomeController@deletePermission');

		Route::get('master/roles', 'HomeController@roles');
		Route::get('master/roles/create', 'HomeController@addRoles');
		Route::post('master/roles/create', 'HomeController@addRoles');
		Route::get('master/roles/edit/{id}', 'HomeController@editRoles');
		Route::post('master/roles/edit/{id}', 'HomeController@editRoles');
		Route::post('master/roles/delete/{id}', 'HomeController@deleteRoles');

		Route::get('master/user_role/{id}', 'HomeController@userRole');
		Route::post('master/user_role/{id}', 'HomeController@userRole');

		Route::get('master/permission_role/{id}', 'HomeController@permissionRole');
		Route::post('master/permission_role/{id}', 'HomeController@permissionRole');
	});
/*Master Data untuk keperluan Authentication*/


Route::get("transaction/periode/{enc_periode_id}/{enc_kontainer_id}/print", "Transaction@printPeriodeKontainer");
Route::group(['middleware' => 'auth'], function () {
	Route::get('transaction', 'Transaction@index');

	Route::get('transaction/pelabuhan', 'Transaction@pelabuhan');
	Route::post('transaction/pelabuhan/delete/{id}', 'Transaction@deletePelabuhan');
	Route::post('transaction/pelabuhan/submit', 'Transaction@pelabuhanSave');
	Route::post('transaction/kota/submit', 'Transaction@kotaSave');
	Route::post('transaction/negara/submit', 'Transaction@negaraSave');
	Route::post('transaction/kota/delete/{id}', 'Transaction@deleteKota');
	Route::post('transaction/negara/delete/{id}', 'Transaction@deleteNegara');

	Route::get('transaction/shiping', 'Transaction@shiping');
	Route::post('transaction/shiping/submit', 'Transaction@shipingSave');
	Route::post('transaction/shiping/delete/{id}', 'Transaction@deleteshiping');

	Route::get('transaction/komponen', 'Transaction@komponen');
	Route::post('transaction/komponen/submit', 'Transaction@komponenSave');
	Route::post('transaction/komponen/delete/{id}', 'Transaction@deleteKomponen');

	Route::get('transaction/periode', 'Transaction@periode');
	Route::get('transaction/periode/form', 'Transaction@periodeForm');
	Route::post('transaction/periode/form', 'Transaction@periodeForm');
	Route::get('transaction/periode/edit/{id}', 'Transaction@editPeriode');
	Route::post('transaction/periode/edit/{id}', 'Transaction@editPeriode');
	Route::post('transaction/periode/delete/{id}', 'Transaction@deletePeriode');
	Route::get('transaction/periode/copy/{id}', 'Transaction@editPeriode');
	Route::post('transaction/periode/copy/{id}', 'Transaction@periodeFormSubmit');

	Route::get('transaction/hpp/cs', 'Transaction@hppApplyCs');
	Route::get('transaction/search/cs', 'Transaction@searchCs');
	Route::post('transaction/search/cs', 'Transaction@drawSearchCSResult');

	Route::get('transaction/search', 'Transaction@search');
	Route::post('transaction/search', 'Transaction@drawSearchResult');


	Route::get('transaction/hpp/apply', 'Transaction@hppApply');
	Route::post('transaction/hpp/apply/delete/{id}', 'Transaction@deleteHppApply');
	Route::get('transaction/hpp/apply/download/{id}', 'Transaction@hppApplyDownload');
	Route::post('transaction/hpp/{id}', 'Transaction@hppView');
	Route::get('transaction/hpp/{id}', 'Transaction@hppView');
});
