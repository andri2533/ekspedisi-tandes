@extends('layouts.app')
@section('content')
<div class="row" style="min-height: 29em;">
    <div class="col-md-12">
    	<div class="card">
    		<div class="card-body profile-card pt-4 flex-column align-items-center">
                <table class="table">
                    @foreach($u->toArray() AS $i=>$v)
                        <tr>
                            <td>{{ $i }}</td>
                            <td>:</td>
                            <td>{{ $v }}</td>
                        </tr>
                    @endforeach
                </table>
    		</div>
		</div>
    </div>

    <div class="col-md-12">
        <div class="pagetitle">
            <h3>Roles</h3>
        </div>
        <div class="card">
            <div class="card-body profile-card pt-4 flex-column align-items-center">
                <form action="{{ Request::url() }}" method="POST">
                    {{ csrf_field() }}
                    <table class="table">
                        <tr>
                            <th>
                                <input type="checkbox" name="checkAll">
                            </th>
                            <th>No</th>
                            <th>Name</th>
                            <th>Display Name</th>
                            <th>Description</th>
                        </tr>
                        @foreach($permission AS $i=>$per)
                            <tr>
                                <td>
                                    <input type="checkbox" id="check_{{ $per->id }}" name="permissions[]" value="{{ $per->id }}">
                                </td>
                                <td>{{ $i+1 }}</td>
                                <td>{{ $per->name }}</td>
                                <td>{{ $per->display_name }}</td>
                                <td>{{ $per->route_permission }}</td>
                            </tr>
                        @endforeach
                    </table>
                    <button type="submit" class="btn btn-primary"><i class="bx bxs-save"></i> Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    input[type=checkbox]{
        cursor: pointer;
    }
</style>
@endsection
@section('script')
    <script type="text/javascript">
        $('input[name=checkAll]').click(function(event) {
            if($(this).prop('checked')==true){
                $("input[type=checkbox]").prop('checked', true);
            }else{
                $("input[type=checkbox]").prop('checked', false);
            }
        });
        const permissions_role = {!! json_encode($u->permission) !!};
        // console.log(permissions_role);
        $.map(permissions_role, function(item, index) {
            $("#check_"+item.id).prop('checked', true);
        });
    </script>
@endsection