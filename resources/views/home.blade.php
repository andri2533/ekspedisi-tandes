@extends('layouts.app')
@section('content')
<div class="row" style="min-height: 29em;">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>
            <div class="panel-body">
                @if(@\Auth::user()->roles()->where("dashboard", "!=", NULL)->first()==null)
                    You are logged in!
                @else
                    @include("pages/dashboard/".\Auth::user()->roles()->where("dashboard", "!=", "NULL")->first()->dashboard)
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
