<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Favicons -->
  <link href="/assets/img/favicon.png" rel="icon">
  <link href="/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="manifest" href="{{ url('') }}/pwa-manifest.json">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="/assets/css/style.css" rel="stylesheet">
  <!-- Vendor JS Files -->
  {{-- <script src="/assets/vendor/apexcharts/apexcharts.min.js"></script> --}}
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript" src="/jquery-v3.6.js"></script>
  <script type="text/javascript" src="/jquery-ui-v-1.13.1.js"></script>
  {{-- <script src="/assets/vendor/chart.js/chart.min.js"></script> --}}
  {{-- <script src="/assets/vendor/echarts/echarts.min.js"></script> --}}
  {{-- <script src="/assets/vendor/quill/quill.min.js"></script> --}}
  {{-- <script src="/assets/vendor/simple-datatables/simple-datatables.js"></script> --}}
  <script src="/assets/vendor/tinymce/tinymce.min.js"></script>
  {{-- <script src="/assets/vendor/php-email-form/validate.js"></script> --}}
  <!-- Template Main JS File -->
  <style type="text/css">
    .swal2-container{
      /*width: 300px;*/
      zoom:0.7;
    }
  </style>

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.2.2
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<?php
  $arrClass = [
    'transaction/hpp/{id}',
    'transaction/periode',
    'transaction/hpp/apply',
    'transaction/hpp/cs'
  ];
?>
<body>
  <!-- ======= Header ======= -->
  @include("layouts/header")
  <!-- End Header -->

  <!-- ======= Sidebar ======= -->
  @include("layouts/sidebar")
  <!-- End Sidebar-->

  <main id="main" class="main">
    <section class="section dashboard">
      <div class="pagetitle">
        <h1>
          {{ (@$current_permission->is_menu==0 ? @$current_permission->name : @$current_permission->display_name) }}
        </h1>
      </div>
      @yield("content")
    </section>
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright <strong><span>{{ config('app.name') }}</span></strong>. All Rights Reserved
    </div>
    <div class="credits">

    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <script src="/assets/js/main.js"></script>
  <link href="/css/jquery-ui.css" rel="stylesheet">
  <script type="text/javascript">
    let isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
    if(isMobile==true){
      $("body").removeClass('toggle-sidebar');
    }else if({{ (in_array(\Route::current()->uri, $arrClass) ? 1 : 0) }}==1){
      $("body").addClass('toggle-sidebar');
    }
    const dateIdn = (tgl)=>{ /*Y-m-d*/
      tgl = tgl.split("-");
      tgl[1] = dateIdnFormat.monthNames[parseInt(tgl[1])-1]
      return tgl[2]+' '+tgl[1]+' '+tgl[0];
    };
    var dateIdnFormat = {
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesShort: ["Mgu", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Mgu"],
      dayNamesMin: ["Mg", "Sn", "Sl", "Ra", "Ka", "Ju", "Sa", "Mg"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des"],
    }
    $(".dateIdn").datepicker({
      dateFormat: "dd MM yy",
      clearButton: true,
      autoclose: true,
      dayNames: dateIdnFormat.dayNames,
      dayNamesShort: dateIdnFormat.dayNamesShort,
      dayNamesMin: dateIdnFormat.dayNamesMin,
      monthNames: dateIdnFormat.monthNames,
      monthNamesShort: dateIdnFormat.monthNamesShort,
      today: "Hari Ini",
      clear: "Kosongkan"
    });
    function separateComma(val) {
      // remove sign if negative
      var sign = 1;
      if (val < 0) {
        sign = -1;
        val = -val;
      }
      // trim the number decimal point if it exists
      let num = val.toString().includes('.') ? val.toString().split('.')[0] : val.toString();
      let len = num.toString().length;
      let result = '';
      let count = 1;

      for (let i = len - 1; i >= 0; i--) {
        result = num.toString()[i] + result;
        if (count % 3 === 0 && count !== 0 && i !== 0) {
          result = ',' + result;
        }
        count++;
      }

      // add number after decimal point
      if (val.toString().includes('.')) {
        result = result + '.' + val.toString().split('.')[1];
      }
      // return result with - sign if negative
      return sign < 0 ? '-' + result : result;
    }
  </script>
  @yield("script")
</body>
</html>