<aside id="sidebar" class="sidebar">
	<ul class="sidebar-nav" id="sidebar-nav">
	    <li class="nav-item">
	    	<a class="nav-link " href="/">
	    		<i class="bi bi-grid"></i>
		        <span>Dashboard</span>
	    	</a>
	    </li>

	    @if(@$menus==null)
	    @else
		    @foreach($menus AS $i=>$menu)
			    <li class="nav-item">
			        <a 
			        	class="nav-link {{ (@$current_permission->parent_id == $menu->id ? '' : 'collapsed') }}"
			        	data-bs-target="#{{ "menu_parent_".$i }}"
			        	data-bs-toggle="collapse" href="#">
			          <i class="{{ $menu->class_icon }}"></i><span>{{ $menu->display_name }}</span>
			          <i class="bi bi-chevron-down ms-auto"></i>
			        </a>
			        @if($menu->child->count()>0)
			        	<ul id="{{ "menu_parent_".$i }}" class="nav-content collapse {{ (@$current_permission->parent_id == $menu->id ? 'show' : '') }}" data-bs-parent="#sidebar-nav">
			        		@foreach($menu->child AS $child)
						        <li>
						            <a class="{{ (@$current_permission->id == $child->id ? 'active' : '' ) }}" href="{{ url($child->route_permission) }}">
						        		<i class="{{ $child->class_icon }}"></i><span>{{ $child->display_name }}</span>
						            </a>
						        </li>
						    @endforeach
			        	</ul>
			        @endif
			    </li><!-- End Components Nav -->
			@endforeach
		@endif
  	</ul>
    <!-- End Dashboard Nav -->
</aside>