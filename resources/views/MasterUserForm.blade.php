@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <br>
            <div class="card-body profile-card">
                <form class="form-horizontal" method="POST" action="{{ Request::url() }}" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ @$usr->name }}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="username" class="col-md-4 control-label">Username</label>
                                <div class="col-md-12">
                                    <input id="username" type="username" class="form-control" name="username" value="{{ @$usr->username }}" required>
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ @$usr->email }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            @if(\Route::current()->uri=="master/user/profil/{id}") {{-- dari user profil --}}
                                <div class="form-group">
                                    <label for="password_old" class="col-md-4 control-label">Old Password</label>
                                    <div class="col-md-12">
                                        <input id="password_old" type="password" class="form-control" name="password_old">
                                        @if ($errors->has('password_old'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_old') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="password" class="col-md-4 control-label">Password</label>
                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control" name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-12">
                                    <input onkeyup="passwordCheck(this)" id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                </div>
                                <span style="color: red; display: none;" id="password-alert">
                                    Password tidak sesuai
                                </span>
                            </div>

                            @if(\Route::current()->uri=="master/user/edit/{id}") {{-- dari user edit manajemen akses --}}
                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Aktif</label>
                                <div class="col-md-12">
                                    <select class="form-control" name="active">
                                        <option value="1" {{ (@$usr->active==1 ? 'selected' : '') }} >Aktif</option>
                                        <option value="0" {{ (@$usr->active==0 ? 'selected' : '') }}>Non Aktif</option>
                                    </select>
                                </div>
                            </div>
                            @endif
                            <br>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="bi bi-save"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>

                        @if(\Route::current()->uri=="master/user/profil/{id}") {{-- dari user profil --}}
                            <div class="col-md-5 card-body profile-card pt-4 d-flex flex-column align-items-center">
                                <br><br>

                                @if(\Auth::user()->avatar==null)
                                <img src="/assets/img/profile-img.png" id="avatar" style="width: 120px !important; height: 120px;" alt="Profile" class="rounded-circle">
                                @else
                                <img src="/uploads/{{ \Auth::user()->avatar }}" id="avatar" style="width: 120px !important; height: 120px;" alt="Profile" class="rounded-circle">
                                @endif
                                <br>
                                <input type="file" name="avatar" class="form-control" style="width: 300px; height: auto;" onchange="preview_image(event)">
                                <span>File harus <b>jpg</b></span>
                                <br>
                                <span class="badge bg-info">{{ $usr->roles()->pluck('display_name')->implode(',') }}</span>
                            </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
	<script type="text/javascript">
		function passwordCheck(t){
			let p2 = $(t).val();
			let p1 = $("#password").val()
			if(p2!=p1){
				$("#password-alert").show();
			}else{
				$("#password-alert").hide();
			}
		}
        function preview_image(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('avatar');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
	</script>
@endsection