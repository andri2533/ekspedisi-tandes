<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    	let select2 = $(".select2").select2({
	        theme: "bootstrap-5"
    	});
    	select2.on('select2:open', ()=>{
            setTimeout(()=>{
                document.querySelector(".select2-search__field").focus();
            }, 100);
        });
    });
</script>