<link href="/datatable/datatables.css" rel="stylesheet"/>
<link href="/datatable/DataTables-1.12.1/css/jquery.dataTables.min.css" rel="stylesheet"/>
<link href="/datatable/DataTables-1.12.1/css/rowReorder.dataTables.min.css" rel="stylesheet"/>
<link href="/datatable/DataTables-1.12.1/css/responsive.dataTables.min.css" rel="stylesheet"/>
<script src="/datatable/datatables.js"></script>
<script src="/datatable/DataTables-1.12.1/js/dataTables.responsive.min.js"></script>
{{-- <script src="/datatable/DataTables-1.12.1/js/dataTables.rowReorder.min.js"></script> --}}
<script src="/datatable/DataTables-1.12.1/js/jquery.dataTables.min.js"></script>
{{ $slot }}
<style type="text/css">
	.dtr-details>.text-center{
		text-align: left !important;
	}
</style>