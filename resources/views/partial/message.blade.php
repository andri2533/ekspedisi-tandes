@if(Session::has('message'))
	<div class="alert alert-info ses-msg"> 
		<i class="bx bx-info-circle"></i> {{ @Session::get('message') }}
	</div>
@endif
@if(Session::has('success'))
	<div class="alert alert-success ses-msg"> 
		<i class="bx bx-check-circle"></i> {{ Session::get('success') }}
	</div> 
@endif
@if(Session::has('error')) 
	<div class="alert alert-danger ses-msg">
		<i class="bx bxs-error"></i> {{ Session::get('error') }}
	</div>
@endif
<script type="text/javascript">
	/*setTimeout(()=>{
		$(".ses-msg").slideUp();
	}, 6000);*/
</script>