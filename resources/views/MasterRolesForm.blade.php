@extends('layouts.app')
@section('content')
<div class="row" style="min-height: 29em;">
    <div class="col-md-12 col-md-offset-2">
    	<div class="card">
            <form id="form" action="{{ Request::url() }}" method="POST">
                {{ csrf_field() }}
        		<div class="card-body profile-card pt-4 flex-column align-items-center">
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="{{ @$role->name }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <label>Display Name</label>
                            <input type="text" name="display_name" class="form-control" value="{{ @$role->display_name }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <label>Deskripsi</label>
                            <textarea class="form-control" name="description">{{ @$role->description }}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="bi bi-save"></i> Simpan
                            </button>
                        </div>
                    </div>
        		</div>
            </form>
		</div>
    </div>
</div>
@endsection
@section('script')

@endsection