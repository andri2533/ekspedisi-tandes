@extends('layouts.app')
@section('content')
    <div class="row" style="min-height: 29em;">
        <div class="col-md-12 col-md-offset-2">
        	<div class="card">
                <form id="form" action="{{ Request::url() }}" method="POST" id="form-search">
                    {{ csrf_field() }}
            		<div class="card-body profile-card pt-4">
                        <div class="row">
                            <div class="col-md-5" style="margin-top: 0.5em;">
                                <div class="form-group">
                                    <select class="select_from form-control" name="from_port">
                                        {!! (@$from==null?'':'<option value='.$from->id.'>'.$from->text.'</option>') !!}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-5" style="margin-top: 0.5em;">
                                <div class="form-group">
                                    <select class="select_to form-control" name="to_port">
                                        {!! (@$to==null?'':'<option value='.$to->id.'>'.$to->text.'</option>') !!}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top: 0.5em;">
                                <button type="button" id="btn-search" class="btn btn-primary"><i class="bx bx-search-alt"></i>Cari</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="card-body profile-card pt-4" style="border-top: 1px dotted #959595">
                    <div class="row">
                        <div class="col-md-12 responsive">
                            <table class="table table-striped" id="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Shiping</th>
                                        <th>Kontainer</th>
                                        <th>Validity</th>
                                        <th>Valid Till</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @include('partial.select2')
    @component('partial.datatable')
        <style type="text/css">
            hr{
                margin: 1px !important;
                height: 1px !important;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                let ajaxUrl = '{{ Request::url() }}?route=drawSearchResult&length=50';
                let table = $("#table").DataTable({
                    dom: '<"toolbar dataTables_length">frtip',
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url : ajaxUrl,
                        method : 'POST',
                        data : {
                            _token:'{{ csrf_token() }}'
                        }
                    },
                    columns:[
                        {data:"id", class:'text-center align-middle', orderable: false, searchable: false, render:(data, type, row, meta)=>{
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }},
                        {data:"shiping", class:"align-middle font-weight-bold"},
                        {data:"jenis_kontainer", class:"align-middle", render:(data,type,row)=>{
                            return '<ul style="margin: 0px !important;"><li>'+data+'</li></ul>';
                        }},
                        {data:"valid_date_1", class:'align-middle', render:(data,type,row)=>{
                            return dateIdn(data);
                        }},
                        {data:"valid_date_2", class:'align-middle', render:(data,type,row)=>{
                            let rt = dateIdn(data);
                            console.log(row.tarif);
                            if(row.dayExpired>5){
                                return rt;
                            }else{
                                return `<span style="color: red; font-weight:bold;"> (${row.dayExpired}) <i style="zoom: 1;" class="bx bx-calendar-exclamation"></i>&nbsp;${rt}</span>`;
                            }
                        }},
                        {data:"id", class:'align-middle', render:(data,type,row)=>{
                            let btn = '<a class="btn btn-lg btn-default" href="{{ url('transaction/hpp') }}/'+data+'"><i class="bx bx-link-external"></i></a>';
                            return btn;
                        }},
                    ],
                    order : [[0, 'desc']]
                });

                var sortPrice = `Urutkan : <select class="form-control price_sort" name="price_sort">
                    <option value="asc">Harga Terendah</option>
                    <option value="desc">Harga Tertinggi</option>
                </select>`;

                $(".toolbar").append(sortPrice);
                $(".price_sort").change(function(){
                    var params           = table.ajax.params();
                    params['price_sort'] = $(this).val();
                    var newUrl           = ajaxUrl+$.param(params);
                    table.ajax.url(newUrl).load();
                });

                let from_select2 = $(".select_from").select2({
                    theme: "bootstrap-5",
                    ajax: {
                        delay: 500,
                        url: '{{ Request::url() }}?route=select2Port',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                type: 'public'
                            }
                          return query;
                        }
                    }
                });

                let to_select2 = $(".select_to").select2({
                    theme: "bootstrap-5",
                    ajax: {
                        delay: 500,
                        url: '{{ Request::url() }}?route=select2Port',
                        data: function (params) {
                            var query = {
                                search: params.term,
                                type: 'public'
                            }
                          return query;
                        }
                    }
                });

                from_select2.on('select2:open', ()=>{
                    setTimeout(()=>{
                        document.querySelector(".select2-search__field").focus();
                    }, 100);
                });

                to_select2.on('select2:open', ()=>{
                    setTimeout(()=>{
                        document.querySelector(".select2-search__field").focus();
                    }, 100);
                });

                $("#btn-search").click(function(event) {
                    ajaxUrl = ajaxUrl+'&from_port='+$(".select_from").val()+'&to_port='+$(".select_to").val()+"&price_sort="+$(".price_sort").val()+"&";
                    table.ajax.url(ajaxUrl).load();
                });
            });
        </script>
    @endcomponent
@endsection
