@extends('layouts.app')
@section('content')
    @include('partial.message')
    <div class="row" style="min-height: 29em;">
        <div class="col-md-12 col-md-offset-2">
        	<div class="card">
        		<div class="card-body profile-card pt-4 flex-column align-items-center">
                    <table id="example" class="display nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                {{-- <th>Provinsi</th> --}}
                                <th>Negara</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
        		</div>
    		</div>
        </div>
    </div>
@endsection
@section('script')
    @component('partial.datatable')
        <script type="text/javascript">
            var table = $('#example').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url : '{{ Request::url() }}'
                },
                responsive: true,
                columns:[
                    {data:"id", class:'text-center', orderable: false, searchable: false, render:(data, type, row, meta)=>{
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }},
                    {data:"nama"},
                    // {data:"nama_provinsi"},
                    {data:"nama_negara"},
                    {data:null, class:'text-center', orderable: false, searchable: false, render:(a,b,c)=>{
                        let btn = '<a class="btn btn-xs btn-warning btn-sm" title="edit" href="{{ url('transaction/kota/edit') }}/'+a.id+'"><i class="bi bi-pencil"></i></a>';
                        btn+='&nbsp;<a class="btn btn-xs btn-danger btn-sm" title="hapus" href="#" onclick="confirmDel('+a.id+')"><i class="bx bxs-trash"></i></a>';
                        return btn;
                    }},
                ],
                order : [[0, 'desc']],
                pageLength : 25
            });
            function confirmDel(id){
                Swal.fire({
                    title: "Data akan dihapus ?",
                    icon: "warning",
                    buttons: true,
                }).then((willDelete) => {
                    if (willDelete.isConfirmed) {
                        $.ajax({
                            url : '{{ Request::url() }}/delete/'+id,
                            type: 'POST',
                            data: {_token: '{{ csrf_token() }}'},
                        }).done(function(data) {
                            if(data=='success'){
                                table.ajax.reload();
                                Swal.fire({
                                    title: "Data berhasil dihapus!",
                                    icon: "success",
                                    timer: 1000
                                });
                            }
                        }).fail(function() {
                            console.log("error");
                        }).always(function() {
                            console.log("complete");
                        });
                    }
                });

                event.preventDefault();
            }
        </script>
    @endcomponent
@endsection