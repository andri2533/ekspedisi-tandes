<div class="card">
    <div class="card-body profile-card pt-4 flex-column align-items-center responsive">
        <div class="pagetitle">
            <h4>
                Daftar Kota
            </h4>
        </div>
        <table id="tabel-kota" class="display nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Negara</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="modalKota" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Kota</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="formKota" action="{{ url('transaction/kota/submit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="card-body profile-card pt-4 flex-column align-items-center">
                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <label>Nama Kota</label>
                                <input type="text" name="nama" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <label>Negara</label>
                                <select class="form-control s-kota-negara" name="negara_id">
                                    <option value="">---</option>
                                    @foreach($country AS $ct)
                                        <option value="{{ $ct->id }}">{{ $ct->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="submitKota()"><i class="bi bi-save"></i> Simpan</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    let btnModalKota = `<button type="button" class="btn btn-primary" onclick="openModalKota('new')">
                        <i class="bi bi-plus"></i>&nbsp;Kota
                    </button>`;
    var tableKota = $('#tabel-kota').DataTable({
        oLanguage: {
           sLengthMenu: `${btnModalKota}&nbsp;<select name="tabel-kota_length" class="form-control"><option value="10">10 Data</option><option value="25">25 Data</option><option value="50">50 Data</option><option value="100">100 Data</option></select>`
        },
        processing: true,
        serverSide: true,
        ajax: {
            url : '{{ Request::url() }}?route=drawKota'
        },
        responsive: true,
        columns:[
            {data:"id", class:'text-center', width: '40px', orderable: false, searchable: false, render:(data, type, row, meta)=>{
                return meta.row + meta.settings._iDisplayStart + 1;
            }},
            {data:"nama"},
            {data:"nama_negara"},
            {data:'id', class:'text-center', orderable: false, searchable: false, render:(data,type,row,meta)=>{
                let btn = `<button onclick="editKota(${row.id})" class="btn btn-xs btn-warning btn-sm" title="edit"><i class="bi bi-pencil"></i></button>`;
                btn+='&nbsp;<a class="btn btn-xs btn-danger btn-sm" title="hapus" href="#" onclick="confirmDelCity('+row.id+')"><i class="bx bxs-trash"></i></a>';
                return btn;
            }},
        ],
        order : [[0, 'desc']]
    });

    function confirmDelCity(id){
        Swal.fire({
            title: "Data akan dihapus ?",
            icon: "warning",
            buttons: true,
        }).then((willDelete) => {
            if (willDelete.isConfirmed) {
                $.ajax({
                    url : `{{ url("transaction/kota/delete") }}/${id}`,
                    type: 'POST',
                    data: {_token: '{{ csrf_token() }}'},
                }).done(function(data) {
                    if(data=='success'){
                        tableKota.ajax.reload();
                        Swal.fire({
                            title: "Data berhasil dihapus!",
                            icon: "success",
                            timer: 1000
                        });
                    }
                }).fail(function() {
                    console.log("error");
                }).always(function() {
                    console.log("complete");
                });
            }
        });
    }

    function editKota(id){
        let dataRow = {};
        tableKota.rows().data().map(function(row, i) {
            if(row.id==id){
                dataRow = row;
            }
        });

        $.each(dataRow, (field, value)=>{
            if($(`[name=${field}]`).length==0 && field=='id'){
                $("#formKota").prepend(`<input type="hidden" name="${field}" value="${value}">`);
            }else if($(`[name=${field}]`)[0]!=undefined){
                if($(`[name=${field}]`).is("select")){ /*jika element select (negara_id) */
                    $(`[name=${field}]`).val(value).change();
                }else{
                    $(`[name=${field}]`).val(value);
                }
            }
        });

        openModalKota('edit');
        setTimeout(()=>{
            $(`[name=kota_id]`).val(dataRow.kota_id).change();
        }, 2000);
    }

    function openModalKota(mode){
        if(mode=='new'){
            $('#formKota input[name=id]').remove();
            $('#formKota')[0].reset();
            $(".s-kota-negara").val('').change();
        }
        $("#modalKota").modal('show');
    }

    function submitKota(){
        let dataParam = {};
        let spanAlert = '<span style="color:red; font-weight:bold;">Wajib diisi</span>';

        $("#formKota").serializeArray().forEach((row)=>{
            let field = $(`#formKota [name=${row.name}]`);
            if(field.attr('required') && field.val()==""){ /*Validasi yg required*/
                field.before(spanAlert);
                field.focus();
                throw new Error(`${row.name} wajib diisi`);
            }
            dataParam[row.name] = row.value;
        });

        let loading = Swal.fire({
            title: 'Proses menyimpan',
            timerProgressBar: true,
            didOpen: () => {
                Swal.showLoading()
            },
            allowOutsideClick: false
        });


        $.post($("#formKota").attr('action'), dataParam, function(data, textStatus, xhr) {
            if(data=='success'){
                tableKota.ajax.reload();
                Swal.fire({
                    title: "Data berhasil disimpan!",
                    icon: "success",
                    timer: 800
                });
                $('#formKota')[0].reset();
                $("#modalKota").modal('hide');
            }else{
                Swal.fire({
                    title: "Gagal menyimpan",
                    icon: "error",
                });
            }
            loading.close();
        });
    }

    $(document).ready(function() {
        $(".s-kota-negara").select2({
            theme: "bootstrap-5",
            dropdownParent: $("#modalKota")
        });

        $(".s-kota-negara").on('select2:open', (d)=>{
            setTimeout(()=>{
                document.querySelector(".select2-search__field").focus();
            }, 300);
        });
    });
</script>