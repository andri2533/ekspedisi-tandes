@extends('layouts.app')
@section('content')
    @include('partial.message')
    <div class="row" style="min-height: 29em;">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="card-body profile-card pt-4 flex-column align-items-center">
                    <div class="pagetitle">
                        <h4>
                          Daftar Shiping
                        </h4>
                    </div>
                    <table id="example" class="display nowrap table-hovered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>currency</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalForm" tabindex="-1" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Komponen</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="form" action="{{ url('transaction/komponen/submit') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="card-body profile-card pt-4 flex-column align-items-center">
                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-4">
                                    <label>Nama Komponen</label>
                                    <input type="text" name="nama" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-4">
                                    <label>Currency</label>
                                    <select class="form-control" name="currency">
                                        <option value="USD">USD</option>
                                        <option value="IDR">IDR</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-submit">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <style type="text/css">
        .btn{
            padding-top: 4px;
            padding-bottom: 4px;
        }
    </style>
    @component('partial.datatable')
        <script type="text/javascript">
            let btnModal = `<button type="button" class="btn btn-primary" onclick="openModal('new')">
                                <i class="bi bi-plus"></i>&nbsp;Komponen
                            </button>`;
            var table = $('#example').DataTable({
                oLanguage: {
                   sLengthMenu: `${btnModal}&nbsp;<select name="example_length" class="form-control select2"><option value="10">10 Data</option><option value="25">25 Data</option><option value="50">50 Data</option><option value="100">100 Data</option></select>`
                },
                processing: true,
                serverSide: true,
                ajax: {
                    url : '{{ Request::url() }}'
                },
                responsive: true,
                columns:[
                    {data:"id", class:'text-center', orderable: false, width: '30px', searchable: false, render:(data, type, row, meta)=>{
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }},
                    {data:"nama"},
                    {data:"currency"},
                    {data:"id", class:'text-center', orderable: false, searchable: false, render:(data,type,row)=>{
                        let btn = `<button onclick="edit(${row.id})" class="btn btn-xs btn-warning btn-sm" title="edit"><i class="bi bi-pencil"></i></button>`;
                        btn+='&nbsp;<a class="btn btn-xs btn-danger btn-sm" title="hapus" href="#" onclick="confirmDel('+row.id+')"><i class="bx bxs-trash"></i></a>';
                        return btn;
                    }},
                ],
                order : [[0, 'desc']]
            });

            function confirmDel(id){
                Swal.fire({
                    title: "Data akan dihapus ?",
                    icon: "warning",
                    buttons: true,
                }).then((willDelete) => {
                    if (willDelete.isConfirmed) {
                        $.ajax({
                            url : '{{ Request::url() }}/delete/'+id,
                            type: 'POST',
                            data: {_token: '{{ csrf_token() }}'},
                        }).done(function(data) {
                            if(data=='success'){
                                table.ajax.reload();
                                Swal.fire({
                                    title: "Data berhasil dihapus!",
                                    icon: "success",
                                    timer: 800
                                });
                            }
                        }).fail(function() {
                            console.log("error");
                        }).always(function() {
                            console.log("complete");
                        });
                    }
                });

                event.preventDefault();
            }
            
            function openModal(mode){
                if(mode=='new'){
                    $('#form input[name=id]').remove();
                    $('#form')[0].reset();
                }
                $("#modalForm").modal('show');
            }

            function edit(id){
                let dataRow = {};
                table.rows().data().map(function(row, i) {
                    if(row.id==id){
                        dataRow = row;
                    }
                });

                $.each(dataRow, (field, value)=>{
                    if($(`[name=${field}]`).length==0 && field=='id'){
                        $("#form").prepend(`<input type="hidden" name="${field}" value="${value}">`);
                    }else{
                        $(`[name=${field}]`).val(value);
                    }
                });

                openModal('edit');
            }

            $(".btn-submit").click(function(event) {
                let dataParam = {};
                let spanAlert = '<span style="color:red; font-weight:bold;">Wajib diisi</span>';
                let loading = Swal.fire({
                    title: 'Proses menyimpan',
                    timerProgressBar: true,
                    didOpen: () => {
                        Swal.showLoading()
                    },
                    allowOutsideClick: false
                });

                $("#form").serializeArray().forEach((row)=>{
                    let field = $(`[name=${row.name}]`);
                    if(field.attr('required') && field.val()==""){ /*Validasi yg required*/
                        field.before(spanAlert);
                        field.focus();
                        throw new Error(`${row.name} wajib diisi`);
                    }
                    dataParam[row.name] = row.value;
                });


                $.post($("#form").attr('action'), dataParam, function(data, textStatus, xhr) {
                    if(data=='success'){
                        table.ajax.reload();
                        Swal.fire({
                            title: "Data berhasil disimpan!",
                            icon: "success",
                            timer: 800
                        });
                        $('#form')[0].reset();
                        $("#modalForm").modal('hide');
                    }else{
                        Swal.fire({
                            title: "Gagal menyimpan",
                            icon: "error",
                        });
                    }
                    loading.close();
                });
            });
        </script>
    @endcomponent
@endsection