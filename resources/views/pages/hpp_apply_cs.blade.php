@extends('layouts.app')
@section('content')
    @include('partial.message')
    <div class="row" style="min-height: 29em;">
        <div class="col-md-12 col-md-offset-2">
        	<div class="card">
        		<div class="card-body profile-card pt-4 flex-column align-items-center">
                    <table id="example" class="display nowrap table-hovered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal Apply</th>
                                <th>Shiping</th>
                                <th>Valid Till</th>
                                {{-- <th>Margin</th> --}}
                                <th>Kontainer</th>
                                <th>From</th>
                                <th>To</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
        		</div>
    		</div>
        </div>
    </div>
	<div class="modal fade" id="scrollingModal" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Preview HPP</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<iframe style="width: 100%; height: 400px; border: none;" src="" id="iframe"></iframe>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('script')
    @component('partial.datatable')
        <script type="text/javascript">
            $.fn.dataTable.ext.errMode = 'none';
            var tahun = '&nbsp;<select name="tahun" class="form-control" onchange="tableReload(this)">{!! yearOptions() !!}</select>';
            var bulan = '<select name="bulan" class="form-control" onchange="tableReload(this)">{!! monthOptions() !!}<option value="13">1 Tahun</option></select>';
            var page = '<select name="length" class="form-control" onchange="tableReload(this)"><option value="10">10 Data</option><option value="25">25 Data</option><option value="50">50 Data</option><option value="100">100 Data</option></select>&nbsp;';
            var table = $('#example').DataTable({
                dom: '<"toolbar dataTables_length">frtip',
                processing: true,
                serverSide: true,
                ajax: {
                    url : '{{ Request::url() }}'
                },
                responsive: true,
                columns:[
                    {data:"id", class:'text-center', orderable: false, searchable: false, render:(data, type, row, meta)=>{
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }},
                    {data:"applied_at", render:(data,type,row)=>{
                        return dateIdn(data);
                    }},
                    {data:"shiping"},
                    // {data:"valid_date_1", render:(data,type,row)=>{
                    //     return dateIdn(data);
                    // }},
                    {data:"valid_date_2", render:(data,type,row)=>{
                        let rt = dateIdn(data);
                        if(row.dayExpired>5){
                            return rt;
                        }else{
                            return `<span style="color: red; font-weight:bold;"> (${row.dayExpired}) <i style="zoom: 1;" class="bx bx-calendar-exclamation"></i>&nbsp;${rt}</span>`;
                        }
                    }},
                    {data:"kontainer"},
                    {data:"from_pelabuhan"},
                    {data:"to_pelabuhan"},
                    {data:"hap_id", class:'text-center', orderable: false, searchable: false, render:(a,b,c)=>{
                        // let btn = `<button onclick="openPreview(${c.hap_id})" class="btn btn-sm btn-info" href="{{ Request::url() }}/preview/${c.hap_id}" target="_blank"><i class="bx bx-link-external"></i></button>`;
                        let btn =`&nbsp;<a class="btn btn-success btn-sm" title="download PDF" href="{{ url('transaction/hpp/apply/download/') }}/${c.hap_id}" ><i class="bx bxs-file-pdf"></i></a>`;
                        return btn;
                    }},
                ],
                order : [[1, 'desc']]
            });

            $(".toolbar").append(page+bulan+tahun);
            
            $('#example').on('error.dt', function (e, settings, techNote, message) {
                setTimeout(()=>{
                    table.ajax.reload();
                }, 2000);
            });


            function tableReload(el){
                el                      = $(el);
                var url                 = '{{ Request::url() }}?';
                var params              = table.ajax.params();
                params[el.attr('name')] = parseInt(el.val());
                var newUrl              = url+$.param(params);
                table.ajax.url(newUrl).load();
            }

            function openPreview(hap_id){
                let url = '{{ Request::url() }}?route=previewHppApply&hap_id='+hap_id;
                $("iframe").attr('src', url);
            	$("#scrollingModal").modal('show');
            }
        </script>
    @endcomponent
@endsection