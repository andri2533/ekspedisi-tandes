@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="card">
            <div class="profile-card pt-4">
                <div class="row">
                    <div class="col-md-6">
                        <table class="table">
                            <tr>
                                <th style="width: 110px;">Shiping</th>
                                <th>:</th>
                                <td>
                                    {{ $periode->shiping->nama }} 
                                </td>
                            </tr>
                            <tr>
                                <th>Dari</th>
                                <th>:</th>
                                <td>
                                    {{ $periode->from->negara->code." ".$periode->from->kota->nama." | ".$periode->from->nama_pelabuhan }}
                                </td>
                            </tr>
                            <tr>
                                <th>Tujuan</th>
                                <th>:</th>
                                <td>
                                    {{ $periode->to->negara->code." ".$periode->to->kota->nama." | ".$periode->to->nama_pelabuhan }}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table">
                            <tr>
                                <th style="width: 110px;">Validity</th>
                                <th>:</th>
                                <td>
                                    {{ dateIdn($periode->valid_date_1) }}
                                </td>
                            </tr>
                            <tr>
                                <th>Valid Till</th>
                                <th>:</th>
                                <td>
                                    {{ dateIdn($periode->valid_date_2) }}
                                    <i>({{ $periode->dayExpired }} hari lagi)</i>
                                </td>
                            </tr>
                            <tr>
                                <th>Keterangan</th>
                                <th>:</th>
                                <td>
                                    {{ $periode->keterangan }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    @foreach($periode->detail AS $i=> $detail)
                        <div class="col-md-3">
                            <div class="accordion" style="margin: 0.5em;">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="heading{{ $i }}">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" aria-expanded="true" data-bs-target="#collapse{{ $i }}">
                                              <b>{{ $detail->kategori->nama }}</b>&nbsp;{{ $detail->ukuran->ukuran }}({{ $detail->kategori->satuan }})
                                        </button>
                                    </h2>
                                    <div class="accordion-body">
                                        <div id="collapse{{ $i }}" class="accordion-collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample" style="">
                                            <table style="width: 100%;">
                                                @foreach(['USD','IDR'] AS $cur)
                                                    @foreach($detail->detailKomponen AS $row)
                                                        @if(@$row->currency==$cur)
                                                            <tr>
                                                                <td>{{ $row->nama }}</td>
                                                                <td>:</td>
                                                                <td>{{ number_format($row->tarif) }}</td>
                                                                <td>{{ $row->currency }}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                    <tr>
                                                        <td style="border-top: 1px dotted #BFBFBF" colspan="4"></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Total</th>
                                                        <td>
                                                            {{ number_format($detail->total($cur)) }}
                                                        </td>
                                                        <td>{{ $cur }}</td>
                                                    </tr>
                                                    {{-- <tr>
                                                        <th colspan="2">Rata-rata</th>
                                                        <td>{{ number_format($detail->average($cur)) }}</td>
                                                        <td>{{ $cur }}</td>
                                                    </tr> --}}
                                                    <tr>
                                                        <th colspan="2">Margin</th>
                                                        <td>
                                                            <input
                                                                {{-- onkeyup="setMargin(this)" --}}
                                                                onchange="setMargin(this)"
                                                                
                                                                maxlength="3"
                                                                @if($detail->applyed==null || $detail->applyed->status==0)
                                                                    disabled
                                                                @endif
                                                                @if($detail->applyed!=null)
                                                                    value="{{ $detail->applyed->margin()->where('currency', $cur)->first()->margin }}"
                                                                @endif
                                                                data-detail_id="{{ $detail->id }}"
                                                                data-total="{{ $detail->total($cur) }}" data-target="#cprofit{{ $i }}-{{ $cur }}"
                                                                data-currency="{{ $cur }}"
                                                                style="width: 90px;" type="number" name="margin" class="form-control input-{{ $detail->id }}">
                                                        </td>
                                                        <td>%</td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Profit</th>
                                                        <td id="cprofit{{ $i }}-{{ $cur }}">
                                                            @if($detail->applyed!=null)
                                                                {{ number_format($detail->applyed->margin()->where('currency', $cur)->first()->margin/100*$detail->total($cur)) }}
                                                            @endif
                                                        </td>
                                                        <td>{{ $cur }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4"><br></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                        <br><br><br>
                                        <center>
                                            <input 
                                                onclick="btnApply({{$detail->id}}, this)
                                                "type="checkbox"
                                                class="toggle-switch-{{ $i }}" netliva-switch
                                                data-active-text="Applyed" data-passive-text="Apply"
                                                {{ ($detail->applyed!=null && $detail->applyed->status==1 ? 'checked' : '') }}
                                                />
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <link rel="stylesheet" type="text/css" href="/css/netliva_switch.css">
    <script type="text/javascript" src="/js/netliva_switch.js"></script>
    <style type="text/css">
        .netliva-switch label{
            width: 93px !important;
        }
    </style>
    <script type="text/javascript">
        function setMargin(el){
            let target  = $(el).attr('data-target');
            let total   = $(el).attr('data-total');
            let margin  = ($(el).val().length==0 ? 0 : $(el).val());
            let profit  = separateComma((margin/100*total).toFixed(0));
            $.ajax({
                url  : '{{ Request::url() }}?route=setMarginHpp',
                type : 'POST',
                data : {
                    _token   : '{{ csrf_token() }}',
                    periode_detail_id   : $(el).attr('data-detail_id'),
                    currency : $(el).attr('data-currency'),
                    margin   : margin
                },
            }).done(function(data) {
                if(data=='success'){
                    $(`${target}`).text(profit);
                    $(el).css('border', '1px solid #ced4da');
                }else{
                    $(el).css('border', '2px solid red');
                }
            }).fail(function() {
                $(el).css('border', '2px solid red');
            });
        }

        function btnApply(id, el){
            let checked = $(el).prop('checked');
            $.ajax({
                url : '{{ Request::url() }}?route=hppAp',
                type: 'POST',
                data: {
                    periode_detail_id: id,
                    apply_by: {{ \Auth::user()->id }},
                    created: checked,
                    _token: '{{ csrf_token() }}'
                },
                success:function(data){
                    console.log(data);
                    if(data=='success'){
                        let inputMargin = $(`.input-${id}`);
                        inputMargin.attr('disabled', (checked===true ? false : true));
                    }else{ /*jika response dari server bermasalah*/
                        if(checked){ /*mengembalikan checkbox ke status sebelumnya*/
                            $(el).prop('checked', false);
                        }else{
                            $(el).prop('checked', true);
                        }
                    }
                },
                error: function(error) {/*jika gagal mengirim ke server*/
                    if(checked){ /*mengembalikan checkbox ke status sebelumnya*/
                        $(el).prop('checked', false);
                    }else{
                        $(el).prop('checked', true);
                    }
                }
            });
        }
    </script>
@endsection