@extends('layouts.app')
@section('content')
	<script type="text/javascript">
		document.getElementsByTagName("BODY")[0].style.display = "none";
		$(document).ready(function() {
			$("#header").remove();
			$("#sidebar").remove();
			$("body").show();
		});
	</script>
	<div class="container-fluid">
        <div class="row">
        	<div class="card">
	            <div class="col-md-6">
	                <table class="table">
	                    <tr>
	                        <th style="width: 150px;">Shiping</th>
	                        <th>:</th>
	                        <td>
	                            {{ $periode->shiping->nama }} 
	                        </td>
	                    </tr>
	                    <tr>
	                        <th>Dari</th>
	                        <th>:</th>
	                        <td>
	                            {{ $periode->from->negara->code." ".$periode->from->kota->nama." | ".$periode->from->nama_pelabuhan }}
	                        </td>
	                    </tr>
	                    <tr>
	                        <th>Tujuan</th>
	                        <th>:</th>
	                        <td>
	                            {{ $periode->to->negara->code." ".$periode->to->kota->nama." | ".$periode->to->nama_pelabuhan }}
	                        </td>
	                    </tr>
	                </table>
	            </div>
	            <div class="col-md-6">
	                <table class="table">
	                    <tr>
	                        <th style="width: 150px;">Validity</th>
	                        <th>:</th>
	                        <td>
	                            {{ dateIdn($periode->valid_date_1) }}
	                        </td>
	                    </tr>
	                    <tr>
	                        <th>Valid Till</th>
	                        <th>:</th>
	                        <td>
	                            {{ dateIdn($periode->valid_date_2) }}
	                            <i>({{ $periode->dayExpired }} hari lagi)</i>
	                        </td>
	                    </tr>
	                    <tr>
	                        <th>Keterangan</th>
	                        <th>:</th>
	                        <td>
	                            {{ $periode->keterangan }}
	                        </td>
	                    </tr>
	                </table>
	            </div>
        		
        	</div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="accordion" style="margin: 0.5em;">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading1">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" aria-expanded="true" data-bs-target="#collapse1">
                                HPP Origin &nbsp;<b>{{ $detail->kategori->nama }}</b>&nbsp;{{ $detail->ukuran->ukuran }}({{ $detail->kategori->satuan }})
                            </button>
                        </h2>
                        <div class="accordion-body">
                            <div id="collapse1" class="accordion-collapse collapse" aria-labelledby="heading1">
                            	<div class="row">
                            		<div class="col-sm-3">&nbsp;</div>
                            		<div class="col-sm-6">
		                                <table style="width: 100%;">
		                                	@foreach(['USD','IDR'] AS $cur)
		                                    @foreach($detail->detailKomponen AS $row)
		                                        @if(@$row->currency==$cur && $row->tarif>0)
		                                            <tr>
		                                                <td>{{ $row->nama }}</td>
		                                                <td>:</td>
		                                                <td>{{ number_format($row->tarif) }}</td>
		                                                <td>{{ $row->currency }}</td>
		                                            </tr>
		                                        @endif
		                                    @endforeach
		                                    <tr>
		                                        <td style="border-top: 1px dotted #BFBFBF" colspan="4"></td>
		                                    </tr>
		                                    <tr>
		                                        <th colspan="2">Total</th>
		                                        <td>{{ number_format($detail->total($cur)) }}</td>
		                                        <td>{{ $cur }}</td>
		                                    </tr>
		                                    <tr>
		                                        <td colspan="4"><br><hr></td>
		                                    </tr>
		                                  @endforeach
		                                </table>
                            		</div>
                            		<div class="col-sm-3">&nbsp;</div>
                            	</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="accordion" style="margin: 0.5em;">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading2">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" aria-expanded="true" data-bs-target="#collapse2">
                                HPP Margin &nbsp;<b>{{ $detail->kategori->nama }}</b>&nbsp;{{ $detail->ukuran->ukuran }}({{ $detail->kategori->satuan }})
                            </button>
                        </h2>
                        <div class="accordion-body">
                            <div id="collapse2" class="accordion-collapse collapse" aria-labelledby="heading1">
	                        	<div class="row">
	                        		<div class="col-sm-3">&nbsp;</div>
	                        		<div class="col-sm-6">
			                            <div id="collapse2" class="accordion-collapse" aria-labelledby="heading2">
			                                <table style="width: 100%;">
			                                	@foreach(['USD','IDR'] AS $cur)
			                                        <tr>
			                                            <td style="border-top: 1px dotted #BFBFBF" colspan="4"></td>
			                                        </tr>
			                                        <tr>
			                                            <th colspan="2">Total</th>
			                                            <td>{{ number_format($detail->total($cur)+$hppApply->margin()->where('currency', $cur)->first()->margin/100*$detail->total($cur)) }}</td>
			                                            <td>{{ $cur }}</td>
			                                        </tr>
			                                        <tr>
			                                            <th colspan="2">Margin</th>
			                                            <td>{{ $hppApply->margin()->where('currency', $cur)->first()->margin }}</td>
			                                            <td>%</td>
			                                        </tr>
			                                        <tr>
														<th colspan="2">Profit</th>
														<td id="cprofit">
													    	{{ number_format($hppApply->margin()->where('currency', $cur)->first()->margin/100*$detail->total($cur)) }}
														</td>
														<td>{{ $cur }}</td>
			                                        </tr>
			                                        <tr>
			                                            <td colspan="4"><br><hr></td>
			                                        </tr>
			                                    @endforeach
			                                </table>
			                            </div>
			                        </div>
	                        		<div class="col-sm-3">&nbsp;</div>
			                    </div>
			                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="accordion" style="margin: 0.5em;">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading3">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" aria-expanded="true" data-bs-target="#collapse3">
                                Form Surat Penawaran
                            </button>
                        </h2>
                        <div class="accordion-body">
                            <div class="accordion-collapse collapse" id="collapse3">
                            	<form action="{{ Request::fullUrl() }}" method="POST" >
                            		{{ csrf_field() }}
	                            	<div class="form-group">
	                            		<label>Tanggal</label>
	                            		<input type="text"
	                            				name="tanggal"
	                            				class="form-control dateIdn"
	                            				placeholder="01 Januari 2022"
	                            				value="{{ dateIdn((@$hppApply->penawaran==null ? date('Y-m-d') : @$hppApply->penawaran->tanggal)) }}">
	                            	</div>
	                            	<div class="form-group">
	                            		<label>Kepada</label>
	                            		<input type="text" name="kepada" class="form-control" value="{{ @$hppApply->penawaran->kepada }}" placeholder="PT. ABCDE">
	                            	</div>
	                            	<div class="form-group">
	                            		<label>PIC</label>
	                            		<input type="text" name="pic" class="form-control" value="{{ @$hppApply->penawaran->pic }}" placeholder="Bpk/Ibu fulan">
	                            	</div>
	                            	<div class="form-group">
	                            		<label>Paragraf 1</label>
		                            	<textarea style="width: 100%; height: 200px;" class="form-control" name="paragraf1">{{ @$paragraf1 }}</textarea>
	                            	</div>
	                            	<div class="form-group">
	                            		<label>Paragraf 2</label>
		                            	<textarea style="width: 100%; height: 200px;" class="form-control" name="paragraf2">{{ @$paragraf2 }}</textarea>
	                            	</div>
	                            	<br>
	                            	<button type="submit" class="btn btn-primary">
	                            		<i class="bx bx-save"></i> Simpan
	                            	</button>
	                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="accordion" style="margin: 0.5em;">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading4">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" aria-expanded="true" data-bs-target="#collapse4">
                                di download oleh :
                            </button>
                        </h2>
                        <div class="accordion-body">
                            <div class="accordion-collapse collapse" id="collapse4">
		                		<a href="{{ url("transaction/hpp/apply/download") }}/{{ Input::get("hap_id") }}">
				                	<span class="badge bg-primary rounded-pill">
				                		download
				                	</span>
		                		</a>
		                		<br><br>
                            	<ol class="list-group list-group-numbered">
                                	@foreach($hppApply->downloaded AS $download)
						                <li class="list-group-item d-flex justify-content-between align-items-start">
						                	<div class="ms-2 me-auto">
							                    <div class="fw-bold">{{ $download->user->name }}</div>
							                	<span class="badge bg-primary rounded-pill">
							                		{{ dayIdn($download->created_at) }}
							                	</span>
							                    {{ dateIdn($download->created_at->format("Y-m-d")) }}
							                    {{ $download->created_at->format("H:i:s") }}
							                </div>
						                </li>
                                	@endforeach
                            	</ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="accordion" style="margin: 0.5em;">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading5">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" aria-expanded="true" data-bs-target="#collapse5">
                                di apply oleh :
                            </button>
                        </h2>
                        <div class="accordion-body">
                            <div class="accordion-collapse" id="collapse5">
                            	<ol class="list-group list-group-numbered">
					                <li class="list-group-item d-flex justify-content-between align-items-start">
					                	<div class="ms-2 me-auto">
						                    <div class="fw-bold">{{ $hppApply->user->name }}</div>
						                	<span class="badge bg-primary rounded-pill">
						                		{{ dayIdn($hppApply->created_at) }}
						                	</span>
						                    {{ dateIdn($hppApply->created_at->format("Y-m-d")) }}
						                    {{ $hppApply->created_at->format("H:i:s") }}
						                </div>
					                </li>
                            	</ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
@endsection