<!DOCTYPE html>
<html>
<head>
	<title>Hpp Apply</title>
</head>
<body>
<style type="text/css">
.container {
	width: 100%;
	padding-right: var(--bs-gutter-x, 0.75rem);
	padding-left: var(--bs-gutter-x, 0.75rem);
	margin-right: auto;
	margin-left: auto;
}
.row {
	--bs-gutter-x: 1.5rem;
	--bs-gutter-y: 0;
	display: flex;
	flex-wrap: wrap;
	margin-top: calc(-1 * var(--bs-gutter-y));
	margin-right: calc(-0.5 * var(--bs-gutter-x));
	margin-left: calc(-0.5 * var(--bs-gutter-x));
}
.col-md-3 {
	flex: 0 0 auto;
	width: 25%;
}
.col-md-6 {
	flex: 0 0 auto;
	width: 50%;
}
.col-md-12 {
	flex: 0 0 auto;
	width: 100%;
}
</style>
<body>
	<div class="container">
	    <div class="row">
	        <div class="card">
	            <div class="profile-card pt-4">
	                <div class="row">
	                    <div class="col-md-6">
	                        <table class="table" style="width: 100%;">
	                            <tr>
	                                <th style="width: 150px;">Shiping</th>
	                                <th style="width: 20px;">:</th>
	                                <td style="width: 500px;">
	                                    {{ $periode->shiping->nama }} 
	                                </td>
	                            </tr>
	                            <tr>
	                                <th>Dari</th>
	                                <th>:</th>
	                                <td>
	                                    {{ $periode->from->negara->code." ".$periode->from->kota->nama." | ".$periode->from->nama_pelabuhan }}
	                                </td>
	                            </tr>
	                            <tr>
	                                <th>Tujuan</th>
	                                <th>:</th>
	                                <td>
	                                    {{ $periode->to->negara->code." ".$periode->to->kota->nama." | ".$periode->to->nama_pelabuhan }}
	                                </td>
	                            </tr>
	                            <tr>
	                                <th>Validity</th>
	                                <th>:</th>
	                                <td>
	                                    {{ dateIdn($periode->valid_date_1) }}
	                                </td>
	                            </tr>
	                            <tr>
	                                <th>Valid Till</th>
	                                <th>:</th>
	                                <td>
	                                    {{ dateIdn($periode->valid_date_2) }}
	                                    <i>({{ $periode->dayExpired }} hari lagi)</i>
	                                </td>
	                            </tr>
	                            <tr>
	                                <th>Keterangan</th>
	                                <th>:</th>
	                                <td>
	                                    {{ $periode->keterangan }}
	                                </td>
	                            </tr>
	                        </table>
	                    </div>
	                </div>
	                <div class="row">
	                	<table style="width: 100%;">
	                		<tr>
	                			<td></td>
	                			<td style="width: 300px;">
		                            <table style="width: 100%;">
		                            	<tr>
		                            		<th align="left" colspan="4">
		                            			{{ $detail->kontainer->kategori->nama }}
		                            			{{ $detail->kontainer->ukuran->ukuran }}
		                            			<br><br>
		                            		</th>
		                            	</tr>
		                            	@foreach(['USD','IDR'] AS $curi => $cur)
			                                @foreach($detail->detailKomponen AS $row)
			                                    @if(@$row->currency==$cur)
			                                        <tr>
			                                            <td>{{ $row->nama }}</td>
			                                            <td>:</td>
			                                            <td>{{ number_format($row->tarif + $hppApply->margin()->where('currency', $cur)->first()->margin/100*$row->tarif) }}</td>
			                                            <td>{{ $row->currency }}</td>
			                                        </tr>
			                                    @endif
			                                @endforeach
			                                <tr>
			                                    <td style="border-top: 1px dotted #BFBFBF" colspan="4"></td>
			                                </tr>
			                                <tr>
			                                    <th colspan="2">Total</th>
			                                    <td>{{ number_format(($detail->total($cur) + $hppApply->margin()->where('currency', $cur)->first()->margin/100*$detail->total($cur))) }}</td>
			                                    <td>{{ $cur }}</td>
			                                </tr>
			                                {{-- <tr>
			                                    <th colspan="2">Margin</th>
			                                    <td>{{ $hppApply->margin()->where('currency', $cur)->first()->margin }}</td>
			                                    <td>%</td>
			                                </tr>
			                                <tr>
			                                	<th colspan="2">Profit</th>
			                                	<td id="cprofit">
		                                        	{{ number_format($hppApply->margin()->where('currency', $cur)->first()->margin/100*$detail->total($cur)) }}
			                                	</td>
			                                	<td>{{ $cur }}</td>
				                            </tr> --}}
			                            	@if($curi==0)
				                                <tr>
				                                    <td colspan="4"><br><hr></td>
				                                </tr>
			                            	@endif
		                           		@endforeach
		                            </table>
	                				
	                			</td>
	                			<td></td>
	                		</tr>
	                	</table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</body>
</body>
</html>