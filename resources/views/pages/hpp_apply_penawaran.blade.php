<!DOCTYPE html>
<html>
<head>
	<title>Hpp Apply</title>
</head>
<body>
<style type="text/css">
	body{
		margin: 0px !important;
		padding: 0px !important;
		margin-top: -30px !important;
	}
	.header p{
		line-height: 15px;
	}
	.header h2{
		line-height: 5px;
	}
	table th{
		text-align: center;
	}
</style>
<body>
	<table style="width: 100%;">
		<tr>
			<td style="width: 8.3%"></td>
			<td style="width: 8.3%"></td>
			<td style="width: 8.3%"></td>
			<td style="width: 8.3%"></td>
			<td style="width: 8.3%"></td>
			<td style="width: 8.3%"></td>
			<td style="width: 8.3%"></td>
			<td style="width: 8.3%"></td>
			<td style="width: 8.3%"></td>
			<td style="width: 8.3%"></td>
			<td style="width: 8.3%"></td>
			<td style="width: 8.3%"></td>
		</tr>
		<tr class="header">
			<td colspan="12" style="border-bottom: 1px solid black; border-spacing: 1px;">
				<img src="assets/img/logo.png" style="width: 120px; height: auto; float: left; margin: 10px;">
				<h2>PT.FASTLOG ERA MANDIRI</h2>
				<p>
					Freight Forwarder Domestic & International for Land, Sea &&nbsp;Air Logistics
					<br>Office : Victoria Main Street Grand Pakuwon RA-8, Banjarsugihan Surabaya
					<br>Phone : +62 31 9934 3392
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="12" style="border-top: 1px solid black;">
				<p style="margin-top: -1px; float: right;">
					Surabaya, {{ dateIdn($hppApply->penawaran->tanggal) }}
				</p>
				<p style="margin-top: -1px;">
					<b>
						{{ $hppApply->penawaran->kepada }}<br>
						{{ $hppApply->penawaran->pic }}
					</b>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="12">
				<h3 style="line-height: 5px; text-align: center;">PENAWARAN</h3>
				<p>
					{!! str_replace("\n", "<br>", $hppApply->penawaran->paragraf1) !!}
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="12">
				<table style="width: 100%; border-collapse: collapse;" border="1">
					<tr>
						<th width="20">No</th>
						<th>Keterangan</th>
						<th>Deskripsi</th>
						<th>Harga</th>
					</tr>
					<tr>
						<td>1</td>
						<td>
							{{ $periode->from->negara->code }} {{ $periode->from->kota->nama }} to {{ $periode->to->negara->code }} {{ $periode->to->kota->nama }}
							<br>
							Rate Validity
							<br>
							{{ str_replace("-","/", $periode->valid_date_1) }}
							-
							{{ str_replace("-","/", $periode->valid_date_2) }}
						</td>
						<td valign="top">
							{{ implode("", $detail->kontainer->ukuranDanKategori) }}
							{{ $periode->shiping->nama }}
						</td>
						<td valign="top" align="right">
							@foreach(['USD','IDR'] AS $curi => $cur)
								{{ number_format(($detail->total($cur) + $hppApply->margin()->where('currency', $cur)->first()->margin/100*$detail->total($cur))) }}
								{{ $cur }}
								<br>
							@endforeach
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="12">
				<p>
					{!! str_replace("\n", "<br>", $hppApply->penawaran->paragraf2) !!}
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="3" valign="top">
				<p>
					Hormat kami
					<br>PT. Fastlog Era Mandiri
					<br>
					<br>
					<br>
					<br>
					<br>
					{{ Auth::user()->name }}
					<br>{{ \Auth::user()->roles()[0]->display_name }}
				</p>
			</td>
			<td colspan="6"></td>
			<td colspan="3" valign="top">
				<p>
					Konfirmasi Customer,
				</p>
			</td>
		</tr>
	</table>
</body>
</html>