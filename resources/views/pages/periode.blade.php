@extends('layouts.app')
@section('content')
    @include('partial.message')
    <div class="row" style="min-height: 29em;">
        <div class="col-md-12 col-md-offset-2">
        	<div class="card">
        		<div class="card-body profile-card pt-4 flex-column align-items-center responsive">
                    <table id="example" class="display nowrap table-hovered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Shiping</th>
                                <th>Kontainer</th>
                                <th>Validity</th>
                                <th>Valid Till</th>
                                <th>From</th>
                                <th>To</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
        		</div>
    		</div>
        </div>
    </div>
@endsection
@section('script')
    @component('partial.datatable')
        <script type="text/javascript">
            $.fn.dataTable.ext.errMode = 'none';
            var table = $('#example').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url : '{{ Request::url() }}'
                },
                responsive: true,
                columns:[
                    {data:"id", class:'text-center', orderable: false, searchable: false, render:(data, type, row, meta)=>{
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }},
                    {data:"shiping"},
                    {data:"kontainer", render:(data,type,row)=>{
                        let arr = data.split('-');
                        let htmls = $.map(arr, function(item, index) {
                            let container = item.split('>');
                            let periode_id = btoa(btoa(btoa(btoa(row.id))));
                            let enc_container_id = btoa(btoa(btoa(btoa(container[0]))));
                            return `<li><a href='/transaction/periode/${periode_id}/${enc_container_id}/print' target='_blank'>${container[1]}</a></li>`
                        }).join("");
                        return '<ul>'+htmls+'</ul>';
                    }},
                    {data:"valid_date_1", render:(data,type,row)=>{
                        return dateIdn(data);
                    }},
                    {data:"valid_date_2", render:(data,type,row)=>{
                        let rt = dateIdn(data);
                        if(row.dayExpired>5){
                            return rt;
                        }else{
                            return `<span style="color: red; font-weight:bold;"> (${row.dayExpired}) <i style="zoom: 1;" class="bx bx-calendar-exclamation"></i>&nbsp;${rt}</span>`;
                        }
                    }},
                    {data:"from_pelabuhan"},
                    {data:"to_pelabuhan"},
                    {data:null, class:'text-center', orderable: false, searchable: false, render:(a,b,c)=>{
                        let btn = '<a class="btn btn-xs btn-warning btn-sm" title="edit" href="{{ Request::url() }}/edit/'+a.id+'"><i class="bi bi-pencil"></i></a>';
                        btn+='&nbsp;<a class="btn btn-xs btn-primary btn-sm" title="duplicate" href="{{ Request::url() }}/copy/'+a.id+'"><i class="bx bx-copy-alt"></i></a>'
                        btn+='&nbsp;<a class="btn btn-xs btn-danger btn-sm" title="hapus" href="#" onclick="confirmDel('+a.id+')"><i class="bx bxs-trash"></i></a>'
                        return btn;
                    }},
                ],
                order : [[0, 'desc']]
            });

            $('#example').on('error.dt', function ( e, settings, techNote, message ) {
                table.ajax.reload();
            });

            function confirmDel(id){
                Swal.fire({
                    title: "Data akan dihapus ?",
                    icon: "warning",
                    buttons: true,
                }).then((willDelete) => {
                    if (willDelete.isConfirmed) {
                        $.ajax({
                            url : '{{ Request::url() }}/delete/'+id,
                            type: 'POST',
                            data: {_token: '{{ csrf_token() }}'},
                        }).done(function(data) {
                            if(data=='success'){
                                table.ajax.reload();
                                Swal.fire({
                                    title: "Data berhasil dihapus!",
                                    icon: "success",
                                    timer: 1000
                                });
                            }
                        }).fail(function() {
                            console.log("error");
                        }).always(function() {
                            console.log("complete");
                        });
                    }
                });

                event.preventDefault();
            }
        </script>
    @endcomponent
@endsection