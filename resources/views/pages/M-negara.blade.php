<div class="card">
    <div class="card-body profile-card pt-4 flex-column align-items-center responsive">
        <div class="pagetitle">
            <h4>
                Daftar Negara
            </h4>
        </div>
        <table id="tabel-negara" class="display nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Kode</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div><div class="modal fade" id="modalNegara" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Negara</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="formNegara" action="{{ url('transaction/negara/submit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="card-body profile-card pt-4 flex-column align-items-center">
                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <label>Negara</label>
                                <input type="text" name="nama" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <label>Kode</label>
                                <input type="text" name="code" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="submitNegara()"><i class="bi bi-save"></i> Simpan</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    let btnModalNegara = `<button type="button" class="btn btn-primary" onclick="openModalNegara('new')">
                        <i class="bi bi-plus"></i>&nbsp;Negara
                    </button>`;
    var tableNegara = $('#tabel-negara').DataTable({
        oLanguage: {
           sLengthMenu: `${btnModalNegara}&nbsp;<select name="tabel-kota_length" class="form-control"><option value="10">10 Data</option><option value="25">25 Data</option><option value="50">50 Data</option><option value="100">100 Data</option></select>`
        },
        processing: true,
        serverSide: true,
        ajax: {
            url : '{{ Request::url() }}?route=drawNegara'
        },
        responsive: true,
        columns:[
            {data:"id", class:'text-center', orderable: false, searchable: false, render:(data, type, row, meta)=>{
                return meta.row + meta.settings._iDisplayStart + 1;
            }},
            {data:"nama"},
            {data:"code"},
            {data:'id', class:'text-center', orderable: false, searchable: false, render:(data, type, row, meta)=>{
                let btn = `<button onclick="editNegara(${row.id})" class="btn btn-xs btn-warning btn-sm" title="edit"><i class="bi bi-pencil"></i></button>`;
                btn+='&nbsp;<a class="btn btn-xs btn-danger btn-sm" title="hapus" href="#" onclick="confirmDelNegara('+row.id+')"><i class="bx bxs-trash"></i></a>';
                return btn;
            }},
        ],
        order : [[0, 'desc']]
    });
    function confirmDelNegara(id){
        Swal.fire({
            title: "Data akan dihapus ?",
            icon: "warning",
            buttons: true,
        }).then((willDelete) => {
            if (willDelete.isConfirmed) {
                $.ajax({
                    url : `{{ url("transaction/negara/delete") }}/${id}`,
                    type: 'POST',
                    data: {_token: '{{ csrf_token() }}'},
                }).done(function(data) {
                    if(data=='success'){
                        tableNegara.ajax.reload();
                        Swal.fire({
                            title: "Data berhasil dihapus!",
                            icon: "success",
                            timer: 1000
                        });
                    }
                }).fail(function() {
                    console.log("error");
                }).always(function() {
                    console.log("complete");
                });
            }
        });

        event.preventDefault();
    }

    function editNegara(id){
        let dataRow = {};
        tableNegara.rows().data().map(function(row, i) {
            if(row.id==id){
                dataRow = row;
            }
        });

        $.each(dataRow, (field, value)=>{
            if($(`[name=${field}]`).length==0 && field=='id'){
                $("#formNegara").prepend(`<input type="hidden" name="${field}" value="${value}">`);
            }else if($(`[name=${field}]`)[0]!=undefined){
                if($(`[name=${field}]`).is("select")){ /*jika element select (negara_id) */
                    $(`[name=${field}]`).val(value).change();
                }else{
                    $(`[name=${field}]`).val(value);
                }
            }
        });

        openModalNegara('edit');
        setTimeout(()=>{
            $(`[name=kota_id]`).val(dataRow.kota_id).change();
        }, 2000);
    }

    function openModalNegara(mode){
        if(mode=='new'){
            $('#formNegara input[name=id]').remove();
            $('#formNegara')[0].reset();
            $(".s-kota-negara").val('').change();
        }
        $("#modalNegara").modal('show');
    }

    function submitNegara(){
        let dataParam = {};
        let spanAlert = '<span style="color:red; font-weight:bold;">Wajib diisi</span>';

        $("#formNegara").serializeArray().forEach((row)=>{
            let field = $(`#formNegara [name=${row.name}]`);
            if(field.attr('required') && field.val()==""){ /*Validasi yg required*/
                field.before(spanAlert);
                field.focus();
                throw new Error(`${row.name} wajib diisi`);
            }
            dataParam[row.name] = row.value;
        });

        let loading = Swal.fire({
            title: 'Proses menyimpan',
            timerProgressBar: true,
            didOpen: () => {
                Swal.showLoading()
            },
            allowOutsideClick: false
        });


        $.post($("#formNegara").attr('action'), dataParam, function(data, textStatus, xhr) {
            if(data=='success'){
                tableNegara.ajax.reload();
                Swal.fire({
                    title: "Data berhasil disimpan!",
                    icon: "success",
                    timer: 800
                });
                $('#formNegara')[0].reset();
                $("#modalNegara").modal('hide');
                location.reload();
            }else{
                Swal.fire({
                    title: "Gagal menyimpan",
                    icon: "error",
                });
            }
            loading.close();
        });
    }
</script>