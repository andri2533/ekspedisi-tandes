<div class="card">
    <div class="card-body profile-card pt-4 flex-column align-items-center responsive">
        <div class="pagetitle">
            <h4>
              Daftar Pelabuhan
            </h4>
        </div>
        <table id="example" class="display nowrap table-hovered" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Pelabuhan</th>
                    <th>Negara</th>
                    <th>Kota</th>
                    <th>Kode</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="modalPelabuhan" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Pelabuhan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="formPelabuhan" action="{{ url('transaction/pelabuhan/submit') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="card-body profile-card pt-4 flex-column align-items-center">
                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <label>Nama Pelabuhan</label>
                                <input type="text" name="nama_pelabuhan" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <label>Negara</label>
                                <select class="form-control negara" name="negara_id" onchange="setCityOfCountry(this.value)">
                                    <option value="">---</option>
                                    @foreach($country AS $ct)
                                        <option value="{{ $ct->id }}">{{ $ct->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <label>Kota</label>
                                <select class="form-control kota" name="kota_id" required>
                                    <option value="">---</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <label>Kode</label>
                                <input type="text" name="kode_pelabuhan" class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="submitPelabuhan()">Save</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    let btnModal = `<button type="button" class="btn btn-primary" onclick="openModalPelabuhan('new')">
                        <i class="bi bi-plus"></i>&nbsp;Pelabuhan
                    </button>`;
    var tablePort = null;

    $(document).ready(function() {
        tablePort = $('#example').DataTable({
            oLanguage: {
               sLengthMenu: `${btnModal}&nbsp;<select name="example_length" class="form-control"><option value="10">10 Data</option><option value="25">25 Data</option><option value="50">50 Data</option><option value="100">100 Data</option></select>`
            },
            processing: true,
            serverSide: true,
            ajax: {
                url : '{{ Request::url() }}?route=drawPelabuhan'
            },
            responsive: true,
            columns:[
                {data:"id", class:'text-center', orderable: false, searchable: false, width: '30px', render:(data, type, row, meta)=>{
                    return meta.row + meta.settings._iDisplayStart + 1;
                }},
                {data:"nama_pelabuhan"},
                {data:"negara", class:'text-center'},
                {data:"kota"},
                {data:"kode_pelabuhan"},
                {data:"id", class:'text-center', orderable: false, searchable: false, render:(data,type,row,meta)=>{
                    let btn = `<button onclick="editPelabuhan(${row.id})" class="btn btn-xs btn-warning btn-sm" title="edit"><i class="bi bi-pencil"></i></button>`;
                    btn+='&nbsp;<a class="btn btn-xs btn-danger btn-sm" title="hapus" href="#" onclick="confirmDelPort('+row.id+')"><i class="bx bxs-trash"></i></a>'
                    return btn;
                }},
            ],
            order : [[0, 'desc']]
        });
    });

    function confirmDelPort(id){
        Swal.fire({
            title: "Data akan dihapus ?",
            icon: "warning",
            button: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url : '{{ Request::url() }}/delete/'+id,
                    type: 'POST',
                    data: {_token: '{{ csrf_token() }}'},
                }).done(function(data) {
                    if(data=='success'){
                        tablePort.ajax.reload();
                        Swal.fire({
                            title: "Data berhasil dihapus!",
                            icon: "success",
                            timer: 800
                        });
                    }
                }).fail(function() {
                    console.log("error");
                }).always(function() {
                    console.log("complete");
                });
            }
        });

        event.preventDefault();
    }

    function openModalPelabuhan(mode){
        $(".negara").select2({
            theme: "bootstrap-5",
            dropdownParent: $("#modalPelabuhan")
        });
        if(mode=='new'){
            $('#formPelabuhan input[name=id]').remove();
            $('#formPelabuhan')[0].reset();
            $("#formPelabuhan .negara").val('').change();
        }
        $("#modalPelabuhan").modal('show');
    }

    $("#formPelabuhan .negara").on('select2:open', (d)=>{
        setTimeout(()=>{
            document.querySelector(".select2-search__field").focus();
        }, 300);
    });

    function setCityOfCountry(negara_id){
        let option = ``;
        if(negara_id!=''){
            $.getJSON(`{{ Request::url() }}?route=cityOfCountry&negara_id=${negara_id}`, function(json, textStatus) {
                $.map(json.results, function(item, index) {
                    option+=`<option value="${item.id}">${item.text}</option>`;
                });
                $("#formPelabuhan .kota").html(option);
            });
        }else{
            option+=`<option value="">---</option>`;
            $("#formPelabuhan .kota").html(option);
        }
    }

    function editPelabuhan(id){
        let dataRow = {};
        tablePort.rows().data().map(function(row, i) {
            if(row.id==id){
                dataRow = row;
            }
        });

        $.each(dataRow, (field, value)=>{
            if($(`[name=${field}]`).length==0 && field=='id'){
                $("#formPelabuhan").prepend(`<input type="hidden" name="${field}" value="${value}">`);
            }else if($(`[name=${field}]`)[0]!=undefined){
                if($(`[name=${field}]`).is("select")){ /*jika element select (negara_id) */
                    $(`[name=${field}]`).val(value).change();
                }else{
                    $(`[name=${field}]`).val(value);
                }
            }
        });

        openModalPelabuhan('edit');
        setTimeout(()=>{
            $(`[name=kota_id]`).val(dataRow.kota_id).change();
        }, 2000);
    }

    function submitPelabuhan(){
        let dataParam = {};
        let spanAlert = '<span style="color:red; font-weight:bold;">Wajib diisi</span>';

        $("#formPelabuhan").serializeArray().forEach((row)=>{
            let field = $(`#formPelabuhan [name=${row.name}]`);
            if(field.attr('required') && field.val()==""){ /*Validasi yg required*/
                field.before(spanAlert);
                field.focus();
                throw new Error(`${row.name} wajib diisi`);
            }
            dataParam[row.name] = row.value;
        });

        let loading = Swal.fire({
            title: 'Proses menyimpan',
            timerProgressBar: true,
            didOpen: () => {
                Swal.showLoading()
            },
            allowOutsideClick: false
        });


        $.post($("#formPelabuhan").attr('action'), dataParam, function(data, textStatus, xhr) {
            if(data=='success'){
                tablePort.ajax.reload();
                Swal.fire({
                    title: "Data berhasil disimpan!",
                    icon: "success",
                    timer: 800
                });
                $('#formPelabuhan')[0].reset();
                $("#modalPelabuhan").modal('hide');
            }else{
                Swal.fire({
                    title: "Gagal menyimpan",
                    icon: "error",
                });
            }
            loading.close();
        });
    }
</script>