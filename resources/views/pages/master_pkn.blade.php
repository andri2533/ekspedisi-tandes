@extends('layouts.app')
@section('content')
    @include('partial.select2')
    @component('partial.datatable')
    @endcomponent
    @include('partial.message')
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            @include('pages/M-pelabuhan')
        </div>
    </div>
    <div class="row">
        <div class="col-md-7 col-md-offset-2">
            @include('pages/M-kota')
        </div>
        <div class="col-md-5 col-md-offset-2">
            @include('pages/M-negara')
        </div>
    </div>
@endsection
@section('script')
    <style type="text/css">
        .btn{
            padding-top: 4px;
            padding-bottom: 4px;
        }
    </style>
@endsection