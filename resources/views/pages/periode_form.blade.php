@extends('layouts.app')
@section('content')
    <div class="row" style="min-height: 29em;">
        <div class="col-md-12 col-md-offset-2">
        	<div class="card">
                <form id="form" action="{{ Request::url() }}" method="POST">
                    {{ csrf_field() }}
            		<div class="card-body profile-card pt-4">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Date Valid</label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control dateIdn" name="valid_date_1" placeholder="Validity" required>
                                    <span class="input-group-text">s/d</span>
                                    <input type="text" class="form-control dateIdn" name="valid_date_2" placeholder="Valid Till" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Nama Shiping</label>
                                <select class="form-control select_shiping" name="shiping_id" required>
                                    {!! (@$periode==null?'':'<option value="'.$periode->shiping->id.'">'.$periode->shiping->nama.'</option>') !!}
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Dari</label>
                                <select class="form-control select_from" name="from_pelabuhan_id" required>
                                    {!! (@$periode==null?'':'<option value="'.$from->id.'">'.$from->negara->code." ".$from->kota->nama." | ".$from->nama_pelabuhan.'</option>') !!}
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Tujuan</label>
                                <select class="form-control select_to" name="to_pelabuhan_id" required>
                                    {!! (@$periode==null?'':'<option value="'.$to->id.'">'.$to->negara->code." ".$to->kota->nama." | ".$to->nama_pelabuhan.'</option>') !!}
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Keterangan</label>
                                <textarea class="form-control" name="keterangan"></textarea>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            @foreach($kontainer AS $i=> $kon)
                                <div class="col-lg-6 col-md-6 col-xs-12">
                                    <div class="accordion" style="margin: 0.5em;">
                                        <div class="accordion-item" data-kontainer-id="{{ $kon->id }}">
                                            <h2 class="accordion-header" id="heading{{ $kon->id }}">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $kon->id }}">
                                                      {{ $kon->kategori->nama }} {{ $kon->ukuran->ukuran }} {{ $kon->kategori->satuan }}
                                                </button>
                                                <input type="hidden" class="kontainer_hidden" name="kontainer_id[]" value="{{ $kon->id }}">
                                            </h2>
                                            <div class="accordion-body">
                                                <div id="collapse{{ $kon->id }}" class="accordion" aria-labelledby="headingOne" data-bs-parent="#accordionExample" style="">
                                                    <div class="container-input-group">
                                                    </div>
                                                    <br>
                                                    <center>
                                                        <button class="btn btn-outline-primary" onclick="tambahKomponen(this)" type="button">
                                                            <i class="bx bx-plus"></i> Komponen
                                                        </button>
                                                    </center>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <br>

                        {{-- <div class="form-group">
                            <center>
                                <button class="btn btn-outline-primary add-component" id="btn-add-{{ $i }}" type="button">
                                    <i class="bx bx-plus"></i> Tambah Baru
                                </button>
                            </center>
                        </div> --}}
                        <br><br>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="bi bi-save"></i> Simpan
                                </button>
                            </div>
                        </div>
            		</div>
                </form>
    		</div>
        </div>
    </div>
@endsection
@section('script')
    @include('partial.select2')
    <style type="text/css">
        .accordion-button{
            /*color: #012970;*/
            background-color: #E6EEFF;
        }
        .input-group-text{
            transition: 2.5s;
            -o-transition: 2.5s;
            -moz-transition: 2.5s;
            -webkit-transition: 2.5s;
        }
    </style>
    <script type="text/javascript" src="/js/number.js"></script>
    <script type="text/javascript">
        const PERIODE      = {!! json_encode(@$periode) !!};
        const KOMPONEN     = {!! json_encode($komponen) !!};

        $(document).ready(function() {

            $(".currency").number(true, 0);
            let shiping_select2 = $(".select_shiping").select2({
                theme: "bootstrap-5",
                ajax: {
                    delay: 500,
                    url: '{{ Request::url() }}?route=select2Shiping',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public'
                        }
                      return query;
                    }
                }
            });

            let from_select2 = $(".select_from").select2({
                theme: "bootstrap-5",
                ajax: {
                    delay: 500,
                    url: '{{ Request::url() }}?route=select2Port',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public'
                        }
                      return query;
                    }
                }
            });

            let to_select2 = $(".select_to").select2({
                theme: "bootstrap-5",
                ajax: {
                    delay: 500,
                    url: '{{ Request::url() }}?route=select2Port',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: 'public'
                        }
                      return query;
                    }
                }
            });

            shiping_select2.on('select2:open', ()=>{
                setTimeout(()=>{
                    document.querySelector(".select2-search__field").focus();
                }, 100);
            });

            from_select2.on('select2:open', ()=>{
                setTimeout(()=>{
                    document.querySelector(".select2-search__field").focus();
                }, 100);
            });

            to_select2.on('select2:open', ()=>{
                setTimeout(()=>{
                    document.querySelector(".select2-search__field").focus();
                }, 100);
            });

            if(PERIODE!=null){
                setCurrency('el');
                $("input[name=valid_date_1]").val(dateIdn(PERIODE.valid_date_1));
                $("input[name=valid_date_2]").val(dateIdn(PERIODE.valid_date_2));
                $("textarea[name=keterangan]").val(PERIODE.keterangan);
                $.each(PERIODE.detail, function(index, pd) {
                    let btnAddComponent = $(`#heading${pd.master_kontainer_id}`).closest('.accordion-item').find('.btn-outline-primary');
                    if(pd.applicable.length>0){
                        let displayApply = '<br><p>Di Apply Oleh :<br>';
                        $.map(pd.applicable, function(apc, i) {
                            displayApply += `<span class="badge rounded-pill bg-info text-dark">${apc.user.name}</span>`;
                        });
                        btnAddComponent.parent('center').after(displayApply+'</p>');
                    }

                    $.each(pd.detail_komponen, function(index, kom) {
                        let inputEl = $("._"+pd.master_kontainer_id+'-'+kom.master_komponen_id);
                        tambahKomponen(btnAddComponent, {
                            komponen_id  : kom.master_komponen_id,
                            kontainer_id : pd.master_kontainer_id,
                            tarif : kom.tarif,
                            applyed : pd.applicable.length
                        });
                    });
                });
            }

            $(".accordion-body input[type=text]").keyup(function(event) {
                if($(this).val()==0){
                    $(this).next().css('background-color', '#e9ecef');
                }else{
                    $(this).next().css('background-color', '#BAFABA');
                }
            });

            $(".add-component").click(function(event) {
                let form = '<center><label>Nama Komponen</label><input type="text" name="nama" id="sf-1" class="form-control" value="" required><label>Mata Uang</label><select class="form-control" id="sf-2" name="currency"><option value="USD">USD</option><option value="IDR">IDR</option></select></center>';
                Swal.fire({
                    title: "Tambah Komponen",
                    icon: "info",
                    html: form,
                    confirmButtonText: 'Simpan',
                    cancelButtonText: 'Batal'
                }).then((submit) => {
                    if (result.isConfirmed) {
                        let param = {
                            _token : '{{ csrf_token() }}',
                            nama : $("#sf-1").val(),
                            currency : $("#sf-2").val(),
                            from : 'ajax'
                        };

                        $.post('{{ url("transaction/komponen/form") }}', param, function(data, textStatus, xhr) {
                            let obj = jQuery.parseJSON(data);
                            if(obj.status=='success'){
                                appendComponent(obj);
                            }
                        });
                    } else if (result.isDenied) {
                        Swal.fire('Changes are not saved', '', 'info')
                    }
                });
            });

            function appendComponent(obj){
                let label = obj.data.nama;

                $.each($('.accordion-item'), function(index, el) {
                    let kontainer_id       = $(el).attr('data-kontainer-id');
                    let master_komponen_id = obj.data.id;
                    let htmlClass          = `_${kontainer_id}-${master_komponen_id}`;
                    let name = `tarif[${kontainer_id}][${master_komponen_id}]`
                    let formGroup = `<div class="form-group"><label>${label}</label><br><div class="input-group mb-3"><input type="text" name="${name}" value="" class="${htmlClass} form-control currency" aria-describedby="basic-addon2"><span class="input-group-text currency-label" id="basic-addon2">${obj.data.currency}</span></div></div>`;

                    let lis = $(el).find(`.accordion-collapse .${obj.data.currency}`).last();
                    lis.after(formGroup);
                });
            }
        });
        
        function setCurrency(el){
            let val          = $(el).val();
            let input        = $(el).parents('.input-group').find('input');
            let kontainer_id = $(el).parents('.accordion-item').attr('data-kontainer-id');
            if(val==''){
                input.attr('disabled', true);
                input.attr('name', '');
                return false;
            }else{
                input.attr('disabled', false);
                KOMPONEN.map(function(row, index) {
                    if(row.id==val){
                        $(el).parents('.input-group').find('.currency-label').text(row.currency);
                        input.attr('name', `tarif[${kontainer_id}][${row.id}]`);
                    }
                });
            }
        }

        function hapusKomponen(el){
            el = $(el);
            el.parents('.input-group').remove();
        }

        function tambahKomponen(el, identity=null){
            let komponenOption = '<option value="">Komponen</option>';
            el                 = $(el);
            let targetInsertEl = el.parents('.accordion-item').find('.container-input-group');
            let kontainer_id   = el.parents('.accordion-item').attr('data-kontainer-id');
            let labelSpan = '';

            KOMPONEN.forEach((row)=>{
                if(identity!=null && identity.komponen_id==row.id){
                    komponenOption+=`<option selected value="${row.id}">${row.nama}</option>`;
                    labelSpan = `<span class="input-group-text currency-label">${ row.currency }</span>`;
                }else{
                    komponenOption+=`<option value="${row.id}">${row.nama}</option>`;
                }
            });

            let btnDellKomponen =`<span class="input-group-text" style="cursor: pointer;" onclick="hapusKomponen(this)">
                                        <i style="color: red;" class="bx bx-trash"></i>
                                    </span>`;
            let selectKomponen  = `<select class="form-control select2" onchange="setCurrency(this)" style="width: 50px !important;">
                                    ${komponenOption}
                                </select>`;
            let inputTarif      = '';

            if(identity!=null){
                inputTarif = `<input type="number" class="form-control" placeholder="Biaya" name="tarif[${identity.kontainer_id}][${identity.komponen_id}]" value="${identity.tarif}">`;
            }else{
                inputTarif = `<input type="number" class="form-control" placeholder="Biaya" disabled name="" value="">`;
                labelSpan = `<span class="input-group-text currency-label">???</span>`;
            }

            
            let inputGroupHtml = `<div class="input-group mb-3">
                                    ${btnDellKomponen}
                                    ${selectKomponen}
                                    ${inputTarif}
                                    ${labelSpan}
                                </div>`;
            targetInsertEl.append(inputGroupHtml);
        }
    </script>
@endsection