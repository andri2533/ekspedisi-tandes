@extends('layouts.app')
@section('content')
<div class="row" style="min-height: 29em;">
    <div class="col-md-12 col-md-offset-2">
        <div class="panel panel-default">
            <center>
                <div class="pagetitle" style="color: red; text-align: center;">
                    <i style="zoom: 3;" class="bx bx-error"></i>
                    <p>{{ @$error_message }}</p>
                </div>
            </center>
        </div>
    </div>
</div>
@endsection
