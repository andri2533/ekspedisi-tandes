@extends('layouts.app')
@section('content')
<div class="row" style="min-height: 29em;">
    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="{{ Request::url() }}" id="form">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Menu / Akses</label>
                <div class="col-md-6">
                    <select class="form-control" name="is_menu">
                        <option></option>
                        <option value="1">Menu</option>
                        <option value="0">Akses</option>
                    </select>
                </div>
            </div>

            <div class="form-group" id="name_cont" style="display: none;">
                <label for="name" class="col-md-4 control-label">Name</label>
                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ @$menu->name }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group" id="display_name_cont" style="display: none;">
                <label for="display_name" class="col-md-4 control-label"><b>Display Name</b></label>
                <div class="col-md-6">
                    <input id="display_name" type="text" class="form-control" name="display_name" value="{{ @$menu->display_name }}">
                    @if ($errors->has('display_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('display_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group" id="jenis_menu_cont" style="display: none;">
                <label>Menu Utama / Sub Menu</label>
                <div class="col-md-6">
                    <select class="form-control" name="jenis_menu">
                        <option></option>
                        <option value="0">Menu Utama</option>
                        <option value="1">Submenu</option>
                    </select>
                </div>
            </div>

            <div class="form-group" id="parent_id_cont" style="display: none">
                <label>Menu Parent</label>
                <div class="col-md-6">
                    <select class="form-control" name="parent_id">
                    </select>
                </div>
            </div>

            <div class="form-group" id="list_draggable" style="display: none; margin-left: 1em;">
                <label>Urutan Menu</label>
                <div class="col-md-6">
                    <ul id="main_menu" class="list-group">
                    </ul>
                </div>
            </div>

            <div class="form-group" id="route_permission_cont" style="display: none;">
                <label>Route Permission / URL</label>
                <div class="col-md-6">
                    <input type="text" name="route_permission" class="form-control" value="{{ @$menu->route_permission }}" required>
                </div>
            </div>

            <div class="form-group" id="active_cont" style="display: none;">
                <label for="password-confirm" class="col-md-4 control-label">Aktif</label>
                <div class="col-md-6">
                    <select class="form-control" name="active" required>
                        <option value="1">Aktif</option>
                        <option value="0">Non Aktif</option>
                    </select>
                </div>
            </div>

            <br>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="bi bi-save"></i> Simpan
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
    <style type="text/css">
        #main_menu li i{
            font-size: 6px;
            vertical-align: middle;
        }
    </style>
	<script type="text/javascript">
        $(document).ready(function() {
            const MN = {!! json_encode(@$menu) !!};
            let currentSequence = ((MN==null) ? 'max' : MN.sequence_number);
            let sequence_number = $("#main_menu").sortable({
                update:()=>{
                    let valueOfSequences = '';
                    $("#main_menu li").each(function(index, el) {
                        if(MN!=null && parseInt($(this).attr('data'))==MN.id){
                            $("input[name=sequence_number]").val((index+1));
                        }

                        valueOfSequences+=','+($(this).attr('data'));
                    });
                    if($("input[name=sequences]").length==1){
                        $("input[name=sequences]").val(valueOfSequences);
                    }else{
                        $("#main_menu").parent('div').append('<input type="hidden" name="sequences" value="'+valueOfSequences+'">')
                    }
                }
            });

            $("select[name=is_menu]").change(function(event) {
                if($(this).val()==1){ /*Menu / Submenu*/
                    $("#jenis_menu_cont").show();
                    $("input[name=display_name]").attr('required');
                }else{ /*Hak akses*/
                    $("#jenis_menu_cont").hide();
                    $("input[name=display_name]").removeAttr('required');
                }
            });

            $("select[name=jenis_menu]").change(function(event) {
                if($(this).val()==1){ /*sub menu*/
                    $.getJSON('{{ Request::url() }}?route=getMenuSequence&parent_id=0', function(json, textStatus) {
                        let list = $.map(json, function(item, index) {
                            let rt = '<option value="'+item.id+'">'+item.display_name+'</option>';
                            if(index==0){
                                let after = ()=>{
                                    let inputHidden = '<input type="hidden" name="sequence_number" value="'+currentSequence+'">';
                                    let currentId = (MN==null?currentSequence:MN.id);
                                    let html = '<li data="'+currentId+'" class="list-group-item active"><i class="bi bi-circle"></i> '+
                                                $("input[name=display_name]").val()+'</li>';
                                    ((MN==null) ? $("#main_menu").append(html) : '');
                                    $("#main_menu").parent('div').append(inputHidden);
                                };
                                sequenceMenu(item.id, after);
                            }
                            return rt;
                        });
                        $("select[name=parent_id]").html(list);
                        $("#parent_id_cont").show();
                    });
                    $("#list_draggable").show();
                    $("#route_permission_cont").show();
                    $("input[name=route_permission]").attr('required');
                }else if($(this).val()==0){ /*menu utama*/
                    $("#parent_id_cont").hide();
                    sequenceMenu(0, ()=>{
                        let inputHidden = '<input type="hidden" name="sequence_number" value="'+currentSequence+'">';
                        let currentId = (MN==null?currentSequence:MN.id);
                        let html = '<li data="'+currentId+'" class="list-group-item active"><i class="bi bi-circle"></i> '+
                                    $("input[name=display_name]").val()+'</li>';
                        ((MN==null) ? $("#main_menu").append(html) : '');
                        $("#main_menu").parent('div').append(inputHidden);
                    });
                    $("#list_draggable").show();
                    $("#route_permission_cont").hide();
                    $("input[name=route_permission]").removeAttr('required');
                }
            });

            $("select[name=parent_id]").change(function(event) {
                let after = ()=>{
                    let currentId = (MN==null?currentSequence:MN.id);
                    let html = '<li data="'+currentId+'" class="list-group-item active"><i class="bi bi-circle"></i> '+
                                $("input[name=display_name]").val()+'</li>';
                    $("#main_menu").append(html);
                };
                sequenceMenu($(this).val(), (MN==null?after:($(this).val()==MN.parent_id?null:after)));
            });

            $("select[name=is_menu]").change(function(event) {
                if($(this).val()==0){ /*Akses*/
                    $("#route_permission_cont").show();
                    $("#name_cont").show();
                    $("#display_name_cont").hide();
                    $("#active_cont").show();
                    $("#list_draggable").hide();
                }else{ /*Menu / Submenu*/
                    $("#route_permission_cont").hide();
                    $("#name_cont").show();
                    $("#display_name_cont").show();
                    $("#active_cont").show();
                    $("#list_draggable").show();
                }
            });

            $("input[name=display_name]").keyup(function(event) {
                $("#main_menu li[class='list-group-item active']").html('<i class="bi bi-circle"></i> '+$(this).val());
            });

            $("input[name=route_permission]").keyup(function(event) {
                $.getJSON('{{ Request::url() }}?route=checkPermissionroute', {route_permission: $(this).val()}, function(json, textStatus) {
                    console.log(json);
                });
            });

            function sequenceMenu(parent_id = 0, after = null){
                $.getJSON('{{ Request::url() }}?route=getMenuSequence&parent_id='+parent_id, function(json, textStatus) {
                    let list = $.map(json, function(item, index) {
                        let rt = '<li data="'+item.id+'" class="list-group-item"><i class="bi bi-circle"></i> '+item.display_name+'</li>';
                        if(item.id==window.location.pathname.split('/')[4]){
                            rt = '<li data="'+item.id+'" class="list-group-item active"><i class="bi bi-circle"></i> '+item.display_name+'</li>';
                        }
                        return rt;
                    });
                    $("#main_menu").html(list);
                    if(after!=null){
                        after();
                    }
                });
            }
            @if(request()->segment(3)=='edit')
                $("select[name=jenis_menu]").val((MN.parent_id==0?0:1)).change();
                $("select[name=active]").val(MN.active).change();
                if(MN.is_menu==1){ /*menu / submenu*/
                    $("select[name=is_menu]").val(1).change();
                    $("#jenis_menu_cont").show();
                    if(MN.parent_id!=0){
                        setTimeout(()=>{
                            $("select[name=parent_id]").val(MN.parent_id).change();
                            $("#route_permission_cont").show();
                        }, 1000);
                    }else{
                        $("#route_permission_cont").hide();
                    }
                }else{ /*hak akses*/
                    $("#route_permission_cont").show();
                    $("#list_draggable").hide();
                    $("select[name=is_menu]").val(0).change();
                    $("#jenis_menu_cont").hide()
                }
            @endif
        });
	</script>
@endsection