<?php
namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $table = 'roles';
    protected $guarded = [];

    public function role_user()
    {
        return $this->hasMany('App\Models\UserRole');
    }

    public function role_permission()
    {
        return $this->hasMany('App\Models\PermissionRole', 'role_id');
    }

    public function permission()
    {
        return $this->belongsToMany(Permission::class, 'permission_role');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user', 'user_id');
    }

    public function dashboard(){
        return $this->hasOne(Permission::class, 'dasboard', 'id');
    }

}
