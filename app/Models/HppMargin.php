<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HppMargin extends Model
{
    protected $table = 'hpp_margin';
    protected $guarded = [];
	public $timestamps = false;
}
