<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class HppApply extends Model
{
    protected $table   = 'hpp_apply';
    protected $guarded = [];
	public $timestamps = true;

	public function downloaded(){
		return $this->hasMany(HppDownload::class);
	}

	public function periodeDetail(){
		return $this->belongsTo(PeriodeDetail::class);
	}

	public function margin(){
		return $this->hasMany(HppMargin::class);
	}

	public function user(){
		return $this->belongsTo(User::class, 'apply_by');
	}

	public function penawaran(){
		return $this->hasOne(Penawaran::class, 'hap_id', 'id');
	}

}