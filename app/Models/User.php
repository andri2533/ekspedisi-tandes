<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, EntrustUserTrait;

    protected $_permissions;
    protected $_menus;

    protected $_whitelistUrl = [];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function role()
    {
        return $this->hasOne('App\Models\UserRole', 'user_id');
    }

    public function roles()
    {
        return $this->hasMany('App\Models\UserRole', 'user_id')->get()->map(function($r){
            return $r->role;
        });
    }

    public function current_role()
    {
        return $this->belongsTo('App\Models\Role', 'current_id_roles');
    }

    public function hasAccess($path = null)
    {
        if(in_array('Developer', $this->roles()->pluck('name')->toArray())){
            return true;
        }
        $sql = "SELECT 
                per.*
                FROM users AS us
                JOIN role_user AS ru ON ru.user_id = us.id
                JOIN permission_role AS pr ON pr.role_id = ru.role_id
                JOIN permissions AS per ON per.id = pr.permission_id
                WHERE per.route_permission = '$path' AND us.id = ".$this->id;
        $has = \DB::select($sql);
        return count($has)==0 ? false : true;
    }

    public function getMenus()
    {
        if(in_array('Developer', $this->roles()->pluck('name')->toArray())){ /*Root*/
            $menus = Permission::where('parent_id', 0)->where('is_menu', 1)->where('active', 1)->orderBy('sequence_number', 'asc')->get()->map(function($menu){
                $menu->child = Permission::where('parent_id', $menu->id)->where('active', 1)->orderBy('sequence_number', 'asc')->get();
                return $menu;
            });
            return $menus;
        }else{
            $sql = "SELECT 
                per.*
                FROM users AS us
                JOIN role_user AS ru ON ru.user_id = us.id
                JOIN permission_role AS pr ON pr.role_id = ru.role_id
                JOIN permissions AS per ON per.id = pr.permission_id
                WHERE per.active = 1 AND per.is_menu = 1 AND us.id = ".$this->id;
            $menus = \DB::select($sql." AND per.parent_id = 0 GROUP BY per.id ORDER BY per.sequence_number ASC");
            $menus = collect($menus)->map(function($menu) use ($sql){
                $menu->child = collect(\DB::select($sql." AND per.parent_id = ".$menu->id." GROUP BY per.id ORDER BY per.sequence_number ASC"));
                return $menu;
            });
            return $menus;
        }
    }

}
