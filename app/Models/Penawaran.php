<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Penawaran extends Model
{
    protected $table   = 'penawaran';
    protected $guarded = [];
	public $timestamps = true;
}
