<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterKomponen extends Model
{
    protected $table = 'master_komponen';
    protected $guarded = [];
	public $timestamps = true;
}
