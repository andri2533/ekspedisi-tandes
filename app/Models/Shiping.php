<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shiping extends Model
{
    protected $table = 'shiping';
    protected $guarded = [];
	public $timestamps = true;
}
