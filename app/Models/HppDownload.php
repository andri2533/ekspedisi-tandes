<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HppDownload extends Model
{
    protected $table   = 'hpp_downloaded';
    protected $guarded = [];
	public $timestamps = true;

	public function user(){
		return $this->belongsTo(User::class, 'downloaded_by');
	}
}
