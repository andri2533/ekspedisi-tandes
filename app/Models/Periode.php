<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Periode extends Model
{
    protected $table = 'periode';
    protected $guarded = [];
    protected $append = ['from','to','dayExpired'];

    public function shiping(){
    	return $this->belongsTo(Shiping::class);
    }

    public function getfromAttribute(){
    	return $this->hasOne(Pelabuhan::class, 'id', 'from_pelabuhan_id')->first();
    }

    public function gettoAttribute(){
    	return $this->hasOne(Pelabuhan::class, 'id', 'to_pelabuhan_id')->first();
    }

    public function detail(){
        return $this->hasMany(PeriodeDetail::class);
    }

    public function getdayExpiredAttribute(){
        $expired = Carbon::parse($this->valid_date_2);
        $selisihHari = Carbon::now()->diffInDays($expired)+1;
        if($expired->timestamp>Carbon::now()->timestamp){
            return $selisihHari;
        }else{
            return 0;
        }
    }
}
