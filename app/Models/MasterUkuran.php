<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterUkuran extends Model
{
    protected $table = 'master_ukuran';
    protected $guarded = [];
	public $timestamps = false;
}
