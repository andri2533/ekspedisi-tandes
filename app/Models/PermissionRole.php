<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{
    protected $table = 'permission_role';
    protected $fillable = ['permission_id', 'role_id'];
	public $timestamps = false;

    public function data()
    {
        return $this->belongsTo('App\Models\Permission', 'permission_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id');
    }
}
