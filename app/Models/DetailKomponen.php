<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailKomponen extends Model
{
    protected $table   = 'detail_komponen';
    protected $guarded = [];
    protected $append = ['nama','currency'];

    public function komponen(){
    	return $this->belongsTo(MasterKomponen::class, 'master_komponen_id');
    }

    public function getnamaAttribute(){
        return $this->hasOne(MasterKomponen::class, 'id', 'master_komponen_id')->first()->nama;
    }

    public function getcurrencyAttribute(){
        return @$this->hasOne(MasterKomponen::class, 'id', 'master_komponen_id')->first()->currency;
    }

    public function Kontainer(){
        return $this->belongsTo(DetailPeriode::class);
    }
}
