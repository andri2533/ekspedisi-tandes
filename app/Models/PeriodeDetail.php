<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PeriodeDetail extends Model
{
    protected $table   = 'periode_detail';
    protected $guarded = [];
    protected $append  = ['kategori','ukuran', 'applyed'];

    public function getkategoriAttribute(){
    	return $this->hasOne(MasterKontainer::class, 'id', 'master_kontainer_id')->first()->kategori;
    }

    public function getukuranAttribute(){
    	return $this->hasOne(MasterKontainer::class, 'id', 'master_kontainer_id')->first()->ukuran;
    }

    public function kontainer(){
        return $this->belongsTo(MasterKontainer::class, 'master_kontainer_id');
    }

    public function detailKomponen(){
        return $this->hasMany(DetailKomponen::class);
    }

    public function total($currency){
        $detail_id = $this->id;
        $sql = "SELECT SUM(dp.tarif) AS total FROM detail_komponen AS dp
                JOIN master_komponen AS mk ON mk.id = dp.master_komponen_id
                WHERE dp.periode_detail_id = $detail_id AND mk.currency = '$currency'";
        $total = \DB::select($sql)[0]->total;
        return $total;
    }

    public function average($currency){
        $detail_id = $this->id;
        $sql = "SELECT TRUNCATE(AVG(if(dp.tarif>0,dp.tarif,null)), 2) AS rata_rata FROM detail_komponen AS dp
                JOIN master_komponen AS mk ON mk.id = dp.master_komponen_id
                WHERE dp.periode_detail_id = $detail_id AND mk.currency = '$currency'";
        return \DB::select($sql)[0]->rata_rata;
    }

    public function getapplyedAttribute(){
        return $this->hasOne(HppApply::class)->where('apply_by', \Auth::user()->id)->first();
    }

    public function applicable(){
        return $this->hasMany(HppApply::class);
    }

    public function periode(){
        return $this->belongsTo(Periode::class);
    }
}
