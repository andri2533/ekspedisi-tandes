<?php
namespace App\Models;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    protected $table = 'permissions';
    protected $guarded = [];

    public function role_permission()
    {
        return $this->hasMany('App\Models\PermissionRole');
    }

    public function getChild()
    {
        return $this->where('parent_id', $this->id)->where('enabled', 1)->get();
    }

    public function getMenus()
    {
        return $this->where('is_menu', 1);
    }

}
