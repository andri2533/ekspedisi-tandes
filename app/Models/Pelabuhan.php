<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Pelabuhan extends Model
{
    protected $table = 'pelabuhan';
    protected $guarded = [];
    public $timestamps = true;
    protected $appends = array('negara');

    public function kota(){
        return $this->belongsTo(Kota::class);
    }

    public function getnegaraAttribute(){
        return $this->hasOne(Kota::class, 'id', 'kota_id')->first()->negara;
    }
}