<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterKontainer extends Model
{
    protected $table = 'master_kontainer';
    protected $guarded = [];
	public $timestamps = false;
    protected $append  = ['ukuranDanKategori'];

	public function ukuran(){
		return $this->belongsTo(MasterUkuran::class);
	}

	public function kategori(){
		return $this->belongsTo(MasterKategori::class);
	}

    public function getukuranDanKategoriAttribute(){
    	$ukuranDanKategori[] = $this->ukuran->ukuran;
    	$ukuranDanKategori[] = $this->kategori->nama;
    	return $ukuranDanKategori;
    }
}
