<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;
use Carbon\Carbon;
use Excel;
use PDF;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (@\Auth::user()->roles()->where("dashboard", "!=", NULL)->first()==null) {
            // code...
        } else {
            if(Input::get("draw")){
                return $this->drawPeriode();
            }
        }

        return view('home');
    }

    private function drawPeriode(){
        $pst     = (Object) Input::all();
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
        $mainSql = "SELECT
                        dp.id,
                        dp.valid_date_1,
                        dp.valid_date_2,
                        sp.nama AS shiping,
                        CONCAT(kf.nama,' | ',plf.nama_pelabuhan) AS from_pelabuhan,
                        CONCAT(kt.nama,' | ',plt.nama_pelabuhan) AS to_pelabuhan,
                        GROUP_CONCAT(ktr.jenis,' ', ktr.ukuran, ktr.satuan SEPARATOR '</li><li>') AS kontainer,
                        dp.keterangan
                    FROM
                    periode AS dp
                    JOIN shiping AS sp ON sp.id = dp.shiping_id
                    JOIN periode_detail AS pd ON pd.periode_id = dp.id
                    JOIN (
                        SELECT
                            mk.id, mkt.nama AS jenis, uk.ukuran, mkt.satuan
                        FROM master_kontainer AS mk
                        JOIN master_kategori AS mkt ON mkt.id = mk.kategori_id
                        JOIN master_ukuran AS uk ON uk.id = mk.ukuran_id
                    ) AS ktr ON ktr.id = pd.master_kontainer_id
                    JOIN pelabuhan AS plf ON plf.id = dp.from_pelabuhan_id
                    JOIN pelabuhan AS plt ON plt.id = dp.to_pelabuhan_id
                    JOIN kota AS kf ON kf.id = plf.kota_id
                    JOIN kota AS kt ON kt.id = plt.kota_id
                    GROUP BY dp.id
                    ";
        $sql     = "SELECT $select FROM ($mainSql) AS d WHERE ($search) ORDER BY ".$orderBy." LIMIT ".$pst->start.", ".$pst->length;
        $data    = collect(DB::select($sql))->map(function($row){
            $now     = Carbon::now();
            $expired = Carbon::parse($row->valid_date_2);
            if($expired->timestamp>$now->timestamp){
                $row->dayExpired = $now->diffInDays($expired)+1;
            }else{
                $row->dayExpired = 0;
            }

            return $row;
        });
        $total   = DB::select("SELECT COUNT(*) AS total FROM ($mainSql) AS d WHERE ($search)");
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => $total[0]->total,
            'recordsFiltered' => $total[0]->total,
            'data'            => $data
        ];
        return json_encode($return);
    }

    public function user(){
        if(Input::get("draw")){
            return $this->drawMasterUser();
        }
        return view('MasterUser');
    }

    public function drawMasterUser(){
        $pst     = (Object) Input::all();
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
        $sql     = "SELECT $select FROM users AS us WHERE ($search) ORDER BY ".$orderBy;
        $data    = DB::select($sql);
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => count($data),
            'recordsFiltered' => count($data),
            'data'            => $data
        ];
        return json_encode($return);
    }

    public function edituser(Request $req, $id){
        $usr = User::find($id);
        if(Input::isMethod('post')){
            $pst = Input::all();
            if(@$pst['avatar']!=null){
                $file          = $pst['avatar'];
                $imageName = time().'.'.$req->avatar->getClientOriginalExtension();

                $destinationPath = public_path('/uploads');
                $img = \Image::make($file->getRealPath());

                $img->fit(600, 600, function ($constraint) {
                    $constraint->upsize();
                });
                
                $img->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$imageName);
                $pst['avatar'] = $imageName;
            }

            if($pst['password']==''){
                unset($pst['_token'], $pst['password_confirmation'], $pst['password'], $pst['password_old']);
            }else{
                unset($pst['_token'], $pst['password_confirmation']);
                $pst['password'] = Hash::make($pst['password']);
                if(@$pst['password_old']){
                    if(Hash::check($pst['password_old'], $usr->password)==true){
                        unset($pst['password_old']);
                    }else{
                        return redirect()->back();
                    }
                }
            }
            $usr->update($pst);

            return redirect()->back()->with('success', 'Data telah disimpan !');
            // return redirect('master/user')->with('success', 'Data telah disimpan !');

        }
        if(\Route::current()->uri=="master/user/profil/{id}" AND \Auth::user()->id!=$id){
            $error_message = 'Anda tidak diperkenankan masuk ke halaman ini.';
            return response()->view('ErrorPage', compact('error_message'));
        }else{
            return view('MasterUserForm', compact('usr'));
        }
    }

    public function createuser(){
        if(Input::isMethod('post')){
            $pst = Input::all();
            unset($pst['_token'], $pst['password_confirmation']);
            $pst['password'] = Hash::make($pst['password']);
            User::create($pst);

            return redirect('master/user')->with('success', 'Data telah disimpan !');
        }
        return view('MasterUserForm');
    }

    public function deleteUser($id){
        \DB::select("DELETE FROM users WHERE id = $id");
        return 'success';
    }

    public function permission(){
        if(Input::get("draw")){
            return $this->drawMasterPermissions();
        }
        return view('MasterPermission');
    }

    private function drawMasterPermissions(){
        $pst     = (Object) Input::all();
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
        $sql     = "SELECT $select FROM permissions WHERE ($search) ORDER BY ".$orderBy." LIMIT ".$pst->start.", ".$pst->length;
        $data    = DB::select($sql);
        $total   = DB::select("SELECT COUNT(*) AS total FROM permissions WHERE ($search)");
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => $total[0]->total,
            'recordsFiltered' => $total[0]->total,
            'data'            => $data
        ];
        return json_encode($return);
    }

    public function editPermission($id){
        $route = Input::get("route");
        if($route!=''){
            return $this->$route();
        }
        $menu = Permission::find($id);
        return view('MasterPermissionForm', compact('menu'));
    }

    private function getMenuSequence(){
        return Permission::where('active', 1)
                    ->where('parent_id', Input::get('parent_id'))
                    ->where('is_menu', 1)
                    ->orderBy('sequence_number', 'asc')
                    ->get();
    }

    public function createPermission(){
        $route = Input::get("route");
        if($route!=''){
            return $this->$route();
        }
        return view("MasterPermissionForm");
    }

    public function postPermission(){
        $route = Input::get("route");
        if($route!=''){
            return $this->$route();
        }
        $data = Input::all();
        if($data['is_menu']=='1'){ /*Menu / Submenu*/
            if((int) $data['jenis_menu']==0){ /*Menu*/
                $data['parent_id'] = 0;
                $data['class_icon'] = 'bx bxs-component';
            }else{ /*sub menu*/
                $data['class_icon'] = 'bi bi-circle';
            }
            if($data['sequence_number']=='max'){
                if(@$data['sequences']==null){
                    $sql = 'SELECT MAX(sequence_number) AS max FROM permissions WHERE is_menu = 1 AND parent_id = '.$data['parent_id'];
                    $max = collect(DB::select($sql))[0]->max;
                    $data['sequence_number'] = $max+1;
                }else{
                    $data['sequence_number'] = array_search('max', explode(',', $data['sequences']));
                }
            }
        }else{ /*Hak akses saja*/
            $data['parent_id'] = 0;
        }
        $sequences = @$data['sequences'];
        unset($data['jenis_menu'], $data['_token'], $data['sequences']);
        if(\Request::segment(4)==null){
            $per = Permission::create($data);
        }else{
            $per = Permission::find(\Request::segment(4));
            $data['sequence_number'] = ($data['sequence_number']=='null' ? null : $data['sequence_number']);
            $per->update($data);
        }
        if(@$sequences){
            $sequences = collect(explode(',', $sequences))->map(function($v, $i) use ($data){
                if((int) $v>0){
                    $p = Permission::find($v);
                    $p->sequence_number = $i;
                    $p->save();
                }
            });
        }
        return redirect('master/permission')->with('success', 'Data telah disimpan !');
    }

    private function checkPermissionroute(){
        return "check url apakah ada di database";
    }

    public function deletePermission($id){
        $p = Permission::find($id);
        $p->delete();
        return 'success';
    }

    public function addRoles(){
        if(Input::isMethod('post')){
            $data = Input::all();
            unset($data['_token']);
            Role::create($data);
            return redirect('master/roles')->with('success', 'Data telah disimpan !');
        }

        return view("MasterRolesForm");
    }

    public function roles(){
        if(Input::get('draw')){
            return $this->drawMasterRoles();
        }
        return view("MasterRoles");
    }

    private function drawMasterRoles(){
        $pst     = (Object) Input::all();
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
        $sql     = "SELECT $select FROM roles WHERE ($search) ORDER BY ".$orderBy." LIMIT ".$pst->start.", ".$pst->length;
        $data    = DB::select($sql);
        $total   = DB::select("SELECT COUNT(*) AS total FROM roles WHERE ($search)");
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => $total[0]->total,
            'recordsFiltered' => $total[0]->total,
            'data'            => $data
        ];
        return json_encode($return);
    }

    public function deleteRoles($id){
        $r = Role::find($id);
        $r->delete();
        return 'success';
    }

    public function editRoles($id){
        $role = Role::find($id);
        if(Input::isMethod('post')){
            $data = Input::all();
            unset($data['_token']);
            $role->update($data);
            return redirect('master/roles')->with('success', 'Data telah disimpan !');   
        }

        return view("MasterRolesForm", compact('role'));
    }

    public function userRole($id){
        $u     = User::find($id);
        $roles = Role::all();
        if(Input::isMethod('post')){
            $data = Input::all();
            $rnew = collect(Input::get('roles'))->map(function($r) use ($id){
                return ['user_id'=>$id,'role_id'=>$r];
            });
            DB::select('DELETE FROM role_user WHERE user_id = '.$id);
            UserRole::insert($rnew->toArray());
            return redirect('master/user')->with('success', 'Data telah disimpan !');
        }

        return view("MasterUserRole", compact('u', 'roles'));
    }

    public function permissionRole($id){
        $u = Role::find($id);
        $permission = Permission::all();
        if(Input::isMethod('post')){
            $data = Input::all();
            $rnew = collect(Input::get('permissions'))->map(function($r) use ($id){
                return ['role_id'=>$id,'permission_id'=>$r];
            });
            DB::select('DELETE FROM permission_role WHERE role_id = '.$id);
            PermissionRole::insert($rnew->toArray());
            return redirect('master/roles')->with('success', 'Data telah disimpan !');
        }

        return view("MasterPermissionRole", compact('u', 'permission'));
    }

    public function loginFromUser(){
        if(Input::get("draw")) {
            $pst     = (Object) Input::all();
            $orderBy = dataTableColumnOrder();
            $select  = dataTableColumnDefine()->implode(', ');
            $search  = dataTableColumnSearch()->implode("OR ");
            $mainSql = 'SELECT us.id, us.username, us.name, GROUP_CONCAT(rol.name) AS roles, us.active FROM users AS us
                        LEFT JOIN role_user AS ru ON ru.user_id = us.id
                        LEFT JOIN roles AS rol ON rol.id = ru.role_id GROUP BY us.id';
            $sql     = "SELECT $select FROM ($mainSql) AS us WHERE ($search) ORDER BY ".$orderBy;
            $data    = DB::select($sql);
            $return = [
                'draw'            => $pst->draw,
                'recordsTotal'    => count($data),
                'recordsFiltered' => count($data),
                'data'            => $data
            ];
            return json_encode($return);
        }else if(Input::get("login")){
            $userLogin = User::find(Input::get("login"));
            \Auth::login($userLogin);
            return redirect("/");
        }
        return view("MasterLoginFromUser");
    }
}
