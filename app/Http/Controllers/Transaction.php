<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Pelabuhan;
use App\Models\Negara;
use App\Models\Provinsi;
use App\Models\Kota;
use App\Models\MasterKomponen;
use App\Models\MasterKategori;
use App\Models\MasterUkuran;
use App\Models\MasterKontainer;
use App\Models\PeriodeDetail;
use App\Models\DetailKomponen;
use App\Models\Shiping;
use App\Models\Periode;
use App\Models\HppApply;
use App\Models\Penawaran;
use Carbon\Carbon;
use PDF;
use DB;
use Auth;

class Transaction extends Controller
{
	public function index($segment = null){
		return view("pages/negara_kota");
	}

	public function pelabuhan(){
		if(Input::get('route')){
			$route = Input::get("route");
			return $this->$route();
		}
		$country = Negara::all();
		return view('pages/master_pkn', compact('country'));
	}

	private function drawPelabuhan(){
		$pst = Input::all();
        $pst     = (Object) Input::all();
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
        $port = "SELECT
				        pel.id,
				        ct.nama AS negara,
				        pel.nama_pelabuhan,
				        kt.nama AS kota,
				        pel.alamat,
				        pel.kode_pelabuhan,
				        ct.id AS negara_id,
				        pel.kota_id
				    FROM pelabuhan AS pel
				    LEFT JOIN kota AS kt ON kt.id = pel.kota_id
				    LEFT JOIN negara AS ct ON ct.id = kt.negara_id";
        $sql     = "SELECT $select, kota_id, negara_id FROM ($port) AS pelabuhan WHERE ($search) ORDER BY ".$orderBy." LIMIT ".$pst->start.", ".$pst->length;
        $data    = DB::select($sql);
        $total = Pelabuhan::all()->count();
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => $total,
            'recordsFiltered' => $total,
            'data'            => $data
        ];
        return json_encode($return);
	}

	public function pelabuhanSave(){
		$data = Input::all();
		unset($data['_token'], $data['negara_id']);
		if(@$data['id']){
			$port = Pelabuhan::find($data['id']);
			$port->update($data);
		}else{
			Pelabuhan::create($data);
		}

		return 'success';

		return redirect('transaction/pelabuhan')->with('success', 'Data telah disimpan');
	}

	private function cityOfCountry(){
		$search    = @Input::get('q');
		$negara_id = Input::get('negara_id');
		$kota      = Kota::selectRaw('id, nama AS text')->where('negara_id', $negara_id)->where('nama','like',"%$search%")->orderBy('nama', 'ASC')->limit(100)->get();
		return [
			'results' => $kota,
		];
	}

	public function deletePelabuhan($id){
    	\DB::select("DELETE FROM pelabuhan WHERE id = $id");
        return 'success';
	}

    private function drawNegara(){
    	$pst     = (Object) Input::all();
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
    	$sql     = "SELECT $select FROM negara WHERE ($search) ORDER BY ".$orderBy." LIMIT ".$pst->start.", ".$pst->length;
        $data    = DB::select($sql);
        $total   = DB::select("SELECT COUNT(*) AS total FROM negara WHERE ($search)");
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => $total[0]->total,
            'recordsFiltered' => $total[0]->total,
            'data'            => $data
        ];
        return json_encode($return);
    }

    public function deleteNegara($id){
    	\DB::select("DELETE FROM negara WHERE id = $id");
        return 'success';
    }

    public function deleteKota($id){
    	\DB::select("DELETE FROM kota WHERE id = $id");
        return 'success';
    }

    private function drawKota(){
    	$pst     = (Object) Input::all();
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
        $mainSql = "SELECT
						kt.id,
						kt.nama,
						ngr.nama as nama_negara,
						kt.negara_id
					FROM
						kota as kt
					JOIN negara as ngr on ngr.id = kt.negara_id";
    	$sql     = "SELECT $select, negara_id FROM ($mainSql) as kota WHERE ($search) ORDER BY ".$orderBy." LIMIT ".$pst->start.", ".$pst->length;
        $data    = DB::select($sql);
        $total   = DB::select("SELECT COUNT(*) AS total FROM ($mainSql) as kota WHERE ($search)");
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => $total[0]->total,
            'recordsFiltered' => $total[0]->total,
            'data'            => $data
        ];
        return json_encode($return);
    }

    public function kotaSave(){
        $data = Input::all();
        unset($data['_token']);
		if(@$data['id']){
            $kota = Kota::find($data['id']);
            $kota->update($data);
        }else{
            Kota::create($data);
        }

        return 'success';
	}

	public function negaraSave(){
        $data = Input::all();
        unset($data['_token']);
		if(@$data['id']){
            $negara = Negara::find($data['id']);
            $negara->update($data);
        }else{
            Negara::create($data);
        }

        return 'success';
	}

	public function komponen(){
		if(Input::get('draw')){
			return $this->drawKomponen();
		}
		return view("pages/komponen");
	}

	private function drawKomponen(){
        $pst     = (Object) Input::all();
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
        $sql     = "SELECT $select FROM master_komponen WHERE ($search) ORDER BY ".$orderBy." LIMIT ".$pst->start.", ".$pst->length;
        $data    = DB::select($sql);
        $total   = DB::select("SELECT COUNT(*) AS total FROM master_komponen WHERE ($search)");
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => $total[0]->total,
            'recordsFiltered' => $total[0]->total,
            'data'            => $data
        ];
        return json_encode($return);
    }

	public function komponenSave(){
		$data = Input::all();
		unset($data['_token']);
		if(@$data['id']){
			$komponen = MasterKomponen::find($data['id']);
			$komponen->update($data);
		}else{
			$komponen = MasterKomponen::create($data);
		}

		return 'success';
	}

	public function editKomponen($id){
		$komponen = MasterKomponen::find($id);
		if(Input::isMethod('post')){
			$a = Input::all();
			unset($a['_token']);
			$komponen->update($a);
			return redirect('transaction/komponen')->with('success','Data telah disimpan !');
		}
		return view('pages/komponen_form', compact('komponen'));
	}

    public function deleteKomponen($id){
        $komponen = MasterKomponen::find($id);
        $komponen->delete();
        return 'success';
    }

	public function shiping(){
		if(Input::get('draw')){
			return $this->drawshiping();
		}
		return view("pages/shiping");
	}

	private function drawshiping(){
        $pst     = (Object) Input::all();
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
        $sql     = "SELECT $select FROM shiping WHERE ($search) ORDER BY ".$orderBy." LIMIT ".$pst->start.", ".$pst->length;
        $data    = DB::select($sql);
        $total   = DB::select("SELECT COUNT(*) AS total FROM shiping WHERE ($search)");
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => $total[0]->total,
            'recordsFiltered' => $total[0]->total,
            'data'            => $data
        ];
        return json_encode($return);
	}

	public function shipingSave(){
		$a = Input::all();
		if(@$a['id']==null){
			unset($a['_token']);
			Shiping::create($a);
		}else{
			$shiping = Shiping::find($a['id']);
			unset($a['_token']);
			$shiping->update($a);
		}
		return 'success';
	}

    public function deleteshiping($id){
        $shiping = Shiping::find($id);
        $shiping->delete();
        return 'success';
    }

	public function periode(){
		if(Input::get('draw')){
			return $this->drawPeriode();
		}
		if(Input::get('route')){
			$route = Input::get("route");
			return $this->$route();
		}
		return view("pages/periode");
	}

	private function drawPeriode(){
        $pst     = (Object) Input::all();
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
        $mainSql = "SELECT
					    dp.id,
					    dp.valid_date_1,
					    dp.valid_date_2,
					    sp.nama AS shiping,
					    CONCAT(kf.nama,' | ',plf.nama_pelabuhan) AS from_pelabuhan,
					    CONCAT(kt.nama,' | ',plt.nama_pelabuhan) AS to_pelabuhan,
					    GROUP_CONCAT(ktr.id, '>', ktr.jenis,' ', ktr.ukuran, ktr.satuan SEPARATOR '-') AS kontainer,
					    dp.keterangan
					FROM
					periode AS dp
					JOIN shiping AS sp ON sp.id = dp.shiping_id
					JOIN periode_detail AS pd ON pd.periode_id = dp.id
					JOIN (
					    SELECT
					        mk.id, mkt.nama AS jenis, uk.ukuran, mkt.satuan
					    FROM master_kontainer AS mk
					    JOIN master_kategori AS mkt ON mkt.id = mk.kategori_id
					    JOIN master_ukuran AS uk ON uk.id = mk.ukuran_id
					) AS ktr ON ktr.id = pd.master_kontainer_id
					JOIN pelabuhan AS plf ON plf.id = dp.from_pelabuhan_id
					JOIN pelabuhan AS plt ON plt.id = dp.to_pelabuhan_id
					JOIN kota AS kf ON kf.id = plf.kota_id
					JOIN kota AS kt ON kt.id = plt.kota_id
					GROUP BY dp.id";
        $sql     = "SELECT $select FROM ($mainSql) AS d WHERE ($search) ORDER BY ".$orderBy." LIMIT ".$pst->start.", ".$pst->length;
        $data    = collect(DB::select($sql))->map(function($row){
        	$now     = Carbon::now();
	        $expired = Carbon::parse($row->valid_date_2);
	        if($expired->timestamp>$now->timestamp){
	        	$row->dayExpired = $now->diffInDays($expired)+1;
	        }else{
	        	$row->dayExpired = 0;
	        }

        	return $row;
        });
        $total   = DB::select("SELECT COUNT(*) AS total FROM ($mainSql) AS d WHERE ($search)");
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => $total[0]->total,
            'recordsFiltered' => $total[0]->total,
            'data'            => $data
        ];
        return json_encode($return);
	}

	public function periodeForm(){
		if(Input::isMethod('post')){
			return $this->periodeFormSubmit();
		}
		if(Input::get('route')){
			$route = Input::get("route");
			return $this->$route();
		}
		$kontainer = MasterKontainer::all();
		$komponen  = MasterKomponen::orderBy('nama','asc')->get();
		return view('pages/periode_form', compact('kontainer','komponen'));
	}

	public function editPeriode($id){
		$periode = Periode::find($id);
		if(Input::isMethod('post')){
			return $this->periodeFormEditSubmit();
		}
		if(Input::get('route')){
			$route = Input::get("route");
			return $this->$route();
		}
		$kontainer = MasterKontainer::all();
		$komponen  = MasterKomponen::orderBy('nama','asc')->get();
		$from      = $periode->from;
		$to        = $periode->to;
		$periode->komponen;
		$periode->detail->map(function($detail){
			$detail->kategori;
			if($detail->applicable!=null){
				$detail->applicable->map(function($apl){
					$apl->user = $apl->user()->select(['id','name'])->first();
					return $apl;
				});
			}
			$detail->detailKomponen->map(function($c){
				$c->komponen;
				return $c;
			});
			return $detail;
		});

		return view('pages/periode_form', compact('kontainer','komponen','periode','from','to'));
	}

	public function periodeFormSubmit(){
		$pst    = Input::all();
		$data   = [
			'valid_date_1'      => dateIdnToYmd($pst['valid_date_1']),
			'valid_date_2'      => dateIdnToYmd($pst['valid_date_2']),
			'shiping_id'        => $pst['shiping_id'],
			'from_pelabuhan_id' => $pst['from_pelabuhan_id'],
			'to_pelabuhan_id'   => $pst['to_pelabuhan_id'],
			'keterangan'        => $pst['keterangan'],
			'created_by'        => \Auth::user()->id
		];
		$tarif = collect($pst['tarif'])->map(function($row, $i){
			$row = collect($row)->filter()->map(function($v){
				return (int) str_replace(',', '', $v);
			});
			return $row;
		})->map(function($c){
			return ($c->count()>0 ? $c : null);
		})->filter();

		$header = Periode::create($data);

		foreach ($tarif as $kontainer_id => $komponens) {
			$periodeDetail = $header->detail()->create([
				'master_kontainer_id' => $kontainer_id
			]);
			foreach ($komponens as $komponen_id => $tarif) {
				$tarif = (int) str_replace(',', '', $tarif);
				if($tarif>0){
					$periodeDetail->detailKomponen()->create([
						'periode_detail_id'  => $periodeDetail->id,
						'master_komponen_id' => $komponen_id,
						'tarif'              => $tarif
					]);
				}
			}
		}

		return redirect('transaction/periode')->with('success', 'Data telah disimpan!');
	}

	private function periodeFormEditSubmit(){
		$pst    = Input::all();
		$data   = [
			'valid_date_1'      => dateIdnToYmd($pst['valid_date_1']),
			'valid_date_2'      => dateIdnToYmd($pst['valid_date_2']),
			'shiping_id'        => $pst['shiping_id'],
			'from_pelabuhan_id' => $pst['from_pelabuhan_id'],
			'to_pelabuhan_id'   => $pst['to_pelabuhan_id'],
			'keterangan'        => $pst['keterangan'],
		];
		$tarif = collect($pst['tarif'])->map(function($row, $i){
			$row = collect($row)->filter()->map(function($v){
				return (int) str_replace(',', '', $v);
			});
			return $row;
		})->map(function($c){
			return ($c->count()>0 ? $c : null);
		})->filter();

		$header = Periode::find(request()->segment(4));
		$header->update($data);

		foreach ($tarif as $kontainer_id => $komp) {
			$periodeDetail = $header->detail()->where('master_kontainer_id', $kontainer_id)->first();
			if($periodeDetail==null){
				$periodeDetail = $header->detail()->create(['master_kontainer_id' => $kontainer_id]);
			}

			$deleted = $periodeDetail->detailKomponen()->whereNotIn('master_komponen_id', array_keys($komp->toArray()))->delete();
			foreach ($komp as $komponen_id => $tarif) {
				$tarif = (int) str_replace(',', '', $tarif);
				$kom   = $periodeDetail->detailKomponen()
						->where('periode_detail_id', $periodeDetail->id)
						->where('master_komponen_id', $komponen_id)->first();
				if($kom!=null){
					$kom->update(['tarif'=>$tarif]);
				}else{
					$periodeDetail->detailKomponen()->create([
						'master_komponen_id' => $komponen_id,
						'tarif' => $tarif
					]);
				}
			}
		}
		return redirect('transaction/periode')->with('success', 'Data telah disimpan!');
	}

	private function select2Port(){
		$search = @Input::get('search');
		$sql = "SELECT
					pel.id,
					CONCAT(ct.code, ' ',kota.nama, ' | ', pel.nama_pelabuhan) AS text,
					ct.nama AS negara
					FROM pelabuhan AS pel
					LEFT JOIN kota ON kota.id = pel.kota_id
					LEFT JOIN negara AS ct ON ct.id = kota.negara_id
					WHERE 
					pel.nama_pelabuhan LIKE '%$search%' OR
					kota.nama LIKE '%$search%' OR
					ct.nama LIKE '%$search%'
					 ORDER BY pel.nama_pelabuhan ASC LIMIT 20";
		$port = \DB::select($sql);
		return [
			'results'=>$port,
		];
	}

	private function select2Shiping(){
		$search = @Input::get('search');
		$shiping = Shiping::selectRaw('id, nama AS text')->where('nama','like',"%$search%")->limit(20)->get();
		return [
			'results' => $shiping,
		];
	}

    public function deletePeriode($id){
        $periode = Periode::find($id);
        PeriodeDetail::where('periode_id', $periode->id)->get()->map(function($p){
        	$p->detailKomponen()->delete();
        });
        PeriodeDetail::where('periode_id', $periode->id)->delete();
        $periode->delete();
        return 'success';
    }

	public function search(){
		if(Input::get('route')){
			$route = Input::get("route");
			return $this->$route();
		}
		return view('pages/search');
	}

	public function drawSearchResult(){
		$pst = (Object) Input::all();
		if(@$pst->to_port==null){
	        return [
	            'draw'            => $pst->draw,
	            'recordsTotal'    => 0,
	            'recordsFiltered' => 0,
	            'data'            => []
	        ];
		}
		$sort         = (@Input::get('price_sort')==null ? 'asc' : Input::get('price_sort'));
		$whereShiping = (@$pst->shiping==null ? '' : ' AND sh.id = '.$pst->shiping);
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
		$sql = "SELECT
					per.id,
					sh.nama AS shiping,
					portf.nama_pelabuhan AS from_pelabuhan,
					portt.nama_pelabuhan AS to_pelabuhan,
					per.valid_date_1,
					per.valid_date_2,
					GROUP_CONCAT(
						jk.nama,
						' ',
						jk.ukuran,
						'(',jk.satuan,')'
						ORDER BY jk.nama ASC SEPARATOR '</li><li>'
					) AS jenis_kontainer,
					GROUP_CONCAT(pd.id) AS detail_id
				FROM periode AS per
				JOIN periode_detail AS pd ON pd.periode_id = per.id
				JOIN (
				    SELECT
				    mkr.id, mki.nama, mu.ukuran, mki.satuan
				    FROM master_kontainer AS mkr
				    JOIN master_ukuran AS mu ON mu.id = mkr.ukuran_id
				    JOIN master_kategori AS mki ON mki.id = mkr.kategori_id
				    ORDER BY mki.nama ASC
				) AS jk ON jk.id = pd.master_kontainer_id
				JOIN shiping AS sh ON sh.id = per.shiping_id
				JOIN pelabuhan AS portf ON portf.id = per.from_pelabuhan_id
				JOIN pelabuhan AS portt ON portt.id = per.to_pelabuhan_id
				WHERE portf.id = ".@$pst->from_port." AND portt.id = ".@$pst->to_port." $whereShiping
				GROUP BY per.id";
				
		$data = \DB::select("SELECT $select, a.detail_id FROM ($sql) AS a WHERE $search ORDER BY ".$orderBy." LIMIT ".$pst->start.", ".$pst->length);

		$data = collect($data)->map(function($row) use ($sort){
			$tarif = collect(explode(",", $row->detail_id))->map(function($id){
				$tarif = DB::select("SELECT SUM(dk.tarif) AS tarif, mk.currency FROM detail_komponen AS dk
									JOIN master_komponen AS mk ON mk.id = dk.master_komponen_id
									WHERE dk.periode_detail_id = $id AND mk.currency = 'USD'");
				return $tarif[0];
			});
			if($sort=="desc"){
				$tarif = $tarif->max('tarif');
			}else{
				$tarif = $tarif->min('tarif');
			}
			$row->tarif = (int) $tarif;

        	$now     = Carbon::now();
	        $expired = Carbon::parse($row->valid_date_2);
	        if($expired->timestamp>$now->timestamp){
	        	$row->dayExpired = $now->diffInDays($expired)+1;
	        }else{
	        	$row->dayExpired = 0;
	        }

        	return $row;
		});
		$data = ($sort=="asc" ? $data->sortBy('tarif') : $data->sortByDesc('tarif'));
		$data = $data->values();
		$total = \DB::select("SELECT COUNT(*) AS total FROM ($sql) AS d WHERE ($search)");
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => $total[0]->total,
            'recordsFiltered' => $total[0]->total,
            'data'            => $data
        ];
        return json_encode($return);
	}

	public function hppView($id){
		$periode = Periode::find($id);
		if(Input::get('route')){
			$route = Input::get("route");
			return $this->$route();
		}
		
		return view("pages/hpp_view", compact('periode'));
	}

	private function hppAp(){
		$input           = Input::all();
		unset($input['_token'], $input['created'], $input['route']);
		$hpp             = HppApply::where($input)->first();
		$input['status'] = (Input::get('created')=='true' ? 1 : 0);
		if($hpp==null){
			$hpp = HppApply::create($input);
			$hpp->margin()->create(['currency'=>'USD','margin'=>0]);
			$hpp->margin()->create(['currency'=>'IDR','margin'=>0]);
		}else{
			$hpp->update($input);
		}
		return 'success';
	}

	public function hppApply(){
        if(Input::get('draw')){
            return $this->drawHppApply();
        }
		if(Input::get('route')){
			$route = Input::get("route");
			return $this->$route();
		}
        return view("pages/hpp_apply");
    }

	public function hppApplyCs(){
        if(Input::get('draw')){
            return $this->drawHppApply();
        }
		if(Input::get('route')){
			$route = Input::get("route");
			return $this->$route();
		}
        return view("pages/hpp_apply_cs");
    }

    private function drawHppApply(){
    	$pst     = (Object) Input::all();
    	$bulan   = (@$pst->bulan==null ? date("m") : ((int) $pst->bulan==13 ? "1,2,3,4,5,6,7,8,9,10,11,12" : $pst->bulan));
    	$tahun   = (@$pst->tahun==null ? date("Y") : $pst->tahun);
        $orderBy = dataTableColumnOrder();
        $select  = dataTableColumnDefine()->implode(', ');
        $search  = dataTableColumnSearch()->implode("OR ");
        $mainSql = "SELECT
						periode.*,
						hap.id AS hap_id, pd.id AS pd_id, pd.periode_id, 20 AS margin, hap.created_at AS applied_at,
						CONCAT(kontainer.jenis,' ', kontainer.ukuran, kontainer.satuan) AS kontainer
						FROM hpp_apply AS hap
						JOIN periode_detail AS pd ON pd.id = hap.periode_detail_id
						JOIN (
						    SELECT
						        mk.id, mkt.nama AS jenis, uk.ukuran, mkt.satuan
						    FROM master_kontainer AS mk
						    JOIN master_kategori AS mkt ON mkt.id = mk.kategori_id
						    JOIN master_ukuran AS uk ON uk.id = mk.ukuran_id
						) AS kontainer ON kontainer.id = pd.master_kontainer_id
						JOIN (
						    SELECT
						        dp.id,
						        dp.valid_date_1,
						        dp.valid_date_2,
						        sp.nama AS shiping,
						        CONCAT(kf.nama,' | ',plf.nama_pelabuhan) AS from_pelabuhan,
						        CONCAT(kt.nama,' | ',plt.nama_pelabuhan) AS to_pelabuhan,
						        dp.keterangan
						    FROM
						    periode AS dp
						    JOIN shiping AS sp ON sp.id = dp.shiping_id
						    JOIN pelabuhan AS plf ON plf.id = dp.from_pelabuhan_id
						    JOIN pelabuhan AS plt ON plt.id = dp.to_pelabuhan_id
						    JOIN kota AS kf ON kf.id = plf.kota_id
						    JOIN kota AS kt ON kt.id = plt.kota_id
						) as periode ON periode.id = pd.periode_id WHERE (YEAR(hap.created_at) = $tahun AND MONTH(hap.created_at) IN ($bulan))";
		$mainSql .= (request()->segment(3)=='cs' ? '' : " AND hap.apply_by = ".\Auth::user()->id);
    	$sql     = "SELECT $select FROM ($mainSql) as kota WHERE ($search) ORDER BY ".$orderBy." LIMIT ".$pst->start.", ".$pst->length;
        $data    = collect(DB::select($sql))->map(function($row){
        	$now     = Carbon::now();
	        $expired = Carbon::parse($row->valid_date_2);
	        if($expired->timestamp>$now->timestamp){
	        	$row->dayExpired = $now->diffInDays($expired)+1;
	        }else{
	        	$row->dayExpired = 0;
	        }

	        $row->applied_at = Carbon::parse($row->applied_at)->format("d M");

        	return $row;
        });
        $total   = DB::select("SELECT COUNT(*) AS total FROM ($mainSql) as kota WHERE ($search)");
        $return = [
            'draw'            => $pst->draw,
            'recordsTotal'    => $total[0]->total,
            'recordsFiltered' => $total[0]->total,
            'data'            => $data
        ];
        return json_encode($return);
    }

    public function deleteHppApply($id){
    	$hppApply = HppApply::find($id);
    	$hppApply->delete();
    	return 'success';
    }

    private function setMarginHpp(){
    	$hppApply          = PeriodeDetail::find(Input::get('periode_detail_id'))->applyed;
    	$hppMargin         = $hppApply->margin()->where('currency', Input::get('currency'))->first();
    	$hppMargin->margin = Input::get('margin');
    	$hppMargin->save();
    	return 'success';
    }

    private function previewHppApply(){
		$data = Input::all();
		$check = Penawaran::where("hap_id", $data['hap_id'])->first();
    	$hppApply = HppApply::find(Input::get('hap_id'));
    	$periode  = $hppApply->periodeDetail->periode;
    	$detail   = $hppApply->periodeDetail;

    	$from = $periode->from->nama_pelabuhan.' '.$periode->from->kota->nama.' ('.$periode->from->negara->nama.')';
    	$to = $periode->to->nama_pelabuhan.' '.$periode->to->kota->nama.' ('.$periode->to->negara->nama.')';
    	$pt = ($check==null ? '' : $check->kepada);

    	$paragraf1 = "Salam Hormat,\nDengan ini kami berikan update harga handling kegiatan Export dari pelabuhan $from ke $to sebagai berikut :\n";
    	$paragraf2 = "Keterangan :\n• Biaya yang tertera di atas adalah biaya estimasi CY to CY dari $from ke $to\n• Harga tersebut belum termasuk trucking\n• Harga diatas belum termasuk Ppn\n• $pt pembayaran tempo 30 hari terhitung setelah Invoice diterima.\n\nDemikian surat penawaran ini kami ajukan, atas perhatian dan kerjasamanya yang baik kami ucapkan terimakasih.";
    	if(Input::isMethod("POST")){
    		unset($data['_token'], $data['route']);
    		$data['tanggal'] = dateIdnToYmd($data['tanggal']);
    		if($check==null){
	    		$check = Penawaran::create($data);
    		}else{
    			$check->update($data);
    		}

    		return redirect()->back()->with('success', 'Data telah disimpan.');
    	}
    	if ($check!=null) {
    		$paragraf1 = $check->paragraf1;
    		$paragraf2 = $check->paragraf2;
    	}

    	return view("pages/hpp_apply_preview", compact("detail", "periode", "hppApply", "paragraf1", "paragraf2"));
    }

    public function hppApplyDownload($id){
    	$hppApply = HppApply::find($id);
    	$periode  = $hppApply->periodeDetail->periode;
    	$detail   = $hppApply->periodeDetail;


        if(@$hppApply->penawaran==null){
	        $pdf = PDF::loadView("pages/hpp_apply_download", compact("detail", "periode", "hppApply"))->setPaper("A4", "potrait");
        }else{
	        $pdf = PDF::loadView("pages/hpp_apply_penawaran", compact("detail", "periode", "hppApply"))->setPaper("A4", "potrait");
        }

    	$hppApply->downloaded()->create(['downloaded_by'=>Auth::user()->id]);

        return $pdf->download("HPP Apply.pdf");
        // return $pdf->stream("HPP Apply.pdf");
    }

	public function searchCs(){
		if(Input::get('route')){
			$route = Input::get("route");
			return $this->$route();
		}
		return view('pages/searchCs');
	}

	public function drawSearchCSResult(){
		return $this->drawSearchResult();
	}

	public function printPeriodeKontainer($enc_periode_id, $enc_kontainer_id){
		$enc_periode_id   = base64_decode(base64_decode(base64_decode(base64_decode($enc_periode_id))));
		$enc_kontainer_id = base64_decode(base64_decode(base64_decode(base64_decode($enc_kontainer_id))));
		$periode          = Periode::find($enc_periode_id);
		if(@$periode==null){
			return "url not found";
		}

    	$detail           = @$periode->detail()->where("master_kontainer_id", $enc_kontainer_id)->first();
		if(@$detail==null){
			return "url not found";
		}

        $pdf = PDF::loadView("pages/calon_hpp", compact("detail", "periode"))->setPaper("A4", "potrait");
        return $pdf->stream("Preview.pdf");
	}
}
