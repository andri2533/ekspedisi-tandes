<?php
	function dataTableColumnSearch(){
		return collect(Illuminate\Support\Facades\Input::get('columns'))->map(function($c){
			if($c['searchable']=='true'){
				return $c['data']." LIKE '%".Illuminate\Support\Facades\Input::get('search')['value']."%' ";
			}
		})->filter()->values();
	}

	function dataTableColumnDefine(){
		return collect(Illuminate\Support\Facades\Input::get('columns'))->map(function($c){
			return $c['data'];
		})->filter()->values();
	}

	function dataTableColumnOrder(){
		$pst = (Object) Illuminate\Support\Facades\Input::all();
		return dataTableColumnDefine()[$pst->order[0]['column']]." ".$pst->order[0]['dir'];
	}

	function dataTableColumnSelect(){
		return dataTableColumnDefine()->map(function($c){
            return $c." AS '$c'";
        })->implode(", ");
	}

	function monthIdn($month = null){ /*$month : Mei*/
	    $bulan = array(
	        1 => "Januari",
	        2 => "Februari",
	        3 => "Maret",
	        4 => "April",
	        5 => "Mei",
	        6 => "Juni",
	        7 => "Juli",
	        8 => "Agustus",
	        9 => "September",
	        10 => "Oktober",
	        11 => "November",
	        12 => "Desember",
	    );

	    if($month==null) return $bulan;
	    return @$bulan[(int)$month];
	}

	function dayIdn($date){/*$date : Carbon, dayOfWeek : integer 0-6*/
		$days = [
			"Minggu",
			'Senin',
			'Selasa',
			'Rabu',
			'Kamis',
			"Jum'at",
			"Sabtu",
		];

		return $days[$date->dayOfWeek];
	}

	function dateIdnToYmd($string){	 /*$string : 01 Mei 2022 */
	    $string = explode(' ', $string);
	    return collect(monthIdn())->map(function($v, $k) use ($string){
	        if(strtolower($v)==strtolower($string[1])){
	            return $string[2].'-'.str_pad('' . $k, 2, '0', STR_PAD_LEFT).'-'.$string[0];
	        }
	    })->filter()->values()->toArray()[0];
	}

	function dateIdn($date, $delimiter = '-') /*Y m d*/{
	    if($date=='' OR $date=='0000-00-00' OR $date==null OR substr($date, 0,1)=='-'){
	        return '';
	    }else{
	        $date = explode($delimiter, $date);
	        if (count($date) < 3) {
	            $date = explode('-', $date[0]);
	            if (count($date < 3)) {
	                $date = explode('-', '2016-01-01');
	            } else {

	            }
	        }
	        $month = monthIdn($date[1]);
	        $final = $date[2] . " " . $month . " " . $date[0];
	        return $final;
	    }
	}

	function yearOptions($startYear = 2022){
	    $selected = \Input::get('tahun') ? \Input::get('tahun') : date("Y");
	    $str      = '';

	    for ($i=date('Y'); $i >= $startYear  ; $i--) {
	        if($selected==$i){
	            $str .= '<option value="'.$i.'" selected>'.$i.'</option>';
	        }else{
	            $str .= '<option value="'.$i.'">'.$i.'</option>';
	        }
	    }

	    return $str;
	}

	function monthOptions(){
		$str = "";
		$bulan = monthIdn();
		foreach ($bulan as $i => $value) {
			if($i==date("m")){
	            $str .= '<option value="'.$i.'" selected>'.$value.'</option>';
			}else{
	            $str .= '<option value="'.$i.'">'.$value.'</option>';
			}
		}

	    return $str;
	}
?>