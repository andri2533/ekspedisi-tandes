<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use App\Models\Permission;
use Route;

class Authenticate
{
    /**
     * The authentication factory implementation.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(AuthFactory $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->user() || ! $request->session()) {
            return redirect()->guest('login');
        }else{
            $menus = $this->auth->user()->getMenus();
            view()->share([
                'menus'              => $menus,
                'current_permission' => Permission::where('route_permission', \Route::current()->uri)->first()
            ]);

            $currentRoute = \Route::current()->uri;
            if($currentRoute!='/' && !\Auth::user()->hasAccess($currentRoute)){
                $error_message = 'Anda tidak diperkenankan masuk ke halaman ini.';
                return response()->view('ErrorPage', compact('error_message'));
            }
        }

        // if (! $request->session()->has('password_hash') && $this->auth->viaRemember()) {
        //     $this->logout($request);
        // }

        // if (! $request->session()->has('password_hash')) {
        //     $this->storePasswordHashInSession($request);
        // }

        // if ($request->session()->get('password_hash') !== $request->user()->getAuthPassword()) {
        //     $this->logout($request);
        // }

        return tap($next($request), function () use ($request) {
            $this->storePasswordHashInSession($request);
        });
    }

    /**
     * Store the user's current password hash in the session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function storePasswordHashInSession($request)
    {
        if (! $request->user()) {
            return;
        }

        $request->session()->put([
            'password_hash' => $request->user()->getAuthPassword(),
        ]);
    }
}
