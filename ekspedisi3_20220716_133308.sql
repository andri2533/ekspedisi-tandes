-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE TABLE "detail_komponen" ------------------------------
CREATE TABLE `detail_komponen` ( 
	`id` Int( 255 ) AUTO_INCREMENT NOT NULL,
	`master_komponen_id` Int( 255 ) NOT NULL,
	`tarif` Int( 255 ) NOT NULL,
	`created_at` DateTime NOT NULL,
	`updated_at` DateTime NOT NULL,
	`periode_detail_id` Int( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 154;
-- -------------------------------------------------------------


-- CREATE TABLE "hpp_apply" ------------------------------------
CREATE TABLE `hpp_apply` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`periode_detail_id` Int( 11 ) NOT NULL,
	`apply_by` Int( 11 ) NULL,
	`created_at` DateTime NULL,
	`updated_at` DateTime NULL,
	`status` Smallint( 1 ) NOT NULL DEFAULT 1,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `id` UNIQUE( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 6;
-- -------------------------------------------------------------


-- CREATE TABLE "hpp_downloaded" -------------------------------
CREATE TABLE `hpp_downloaded` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`hpp_apply_id` Int( 11 ) NULL,
	`created_at` DateTime NULL,
	`updated_at` DateTime NULL,
	`downloaded_by` Int( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `id` UNIQUE( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 9;
-- -------------------------------------------------------------


-- CREATE TABLE "hpp_margin" -----------------------------------
CREATE TABLE `hpp_margin` ( 
	`id` Int( 255 ) AUTO_INCREMENT NOT NULL,
	`hpp_apply_id` Int( 255 ) NOT NULL,
	`currency` VarChar( 5 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`margin` Int( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 11;
-- -------------------------------------------------------------


-- CREATE TABLE "kota" -----------------------------------------
CREATE TABLE `kota` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama` VarChar( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`provinsi_id` Int( 11 ) NULL DEFAULT 0,
	`negara_id` Smallint( 5 ) NULL DEFAULT 0,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `id` UNIQUE( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = MyISAM
AUTO_INCREMENT = 207;
-- -------------------------------------------------------------


-- CREATE TABLE "master_kategori" ------------------------------
CREATE TABLE `master_kategori` ( 
	`id` Int( 255 ) AUTO_INCREMENT NOT NULL,
	`nama` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Dry' COMMENT 'value: (Dry/Reefer)',
	`created_at` DateTime NULL,
	`updated_at` DateTime NULL,
	`satuan` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- CREATE TABLE "master_komponen" ------------------------------
CREATE TABLE `master_komponen` ( 
	`id` Smallint( 5 ) AUTO_INCREMENT NOT NULL,
	`nama` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`currency` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`created_at` DateTime NULL,
	`updated_at` DateTime NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 17;
-- -------------------------------------------------------------


-- CREATE TABLE "master_kontainer" -----------------------------
CREATE TABLE `master_kontainer` ( 
	`id` Int( 255 ) AUTO_INCREMENT NOT NULL,
	`ukuran_id` Int( 3 ) NOT NULL,
	`kategori_id` Int( 3 ) NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT 'penghubung antara tabel ukuran dan kategori'
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- CREATE TABLE "master_ukuran" --------------------------------
CREATE TABLE `master_ukuran` ( 
	`id` Int( 255 ) AUTO_INCREMENT NOT NULL,
	`ukuran` Int( 255 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- CREATE TABLE "migrations" -----------------------------------
CREATE TABLE `migrations` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`migration` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`batch` Int( 11 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 6;
-- -------------------------------------------------------------


-- CREATE TABLE "negara" ---------------------------------------
CREATE TABLE `negara` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
	`code` VarChar( 10 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `id` UNIQUE( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 202;
-- -------------------------------------------------------------


-- CREATE TABLE "password_resets" ------------------------------
CREATE TABLE `password_resets` ( 
	`email` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`token` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- CREATE TABLE "pelabuhan" ------------------------------------
CREATE TABLE `pelabuhan` ( 
	`id` Smallint( 6 ) AUTO_INCREMENT NOT NULL,
	`nama_pelabuhan` VarChar( 28 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
	`kota_id` Smallint( 4 ) NULL,
	`alamat` VarChar( 168 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
	`kode_pelabuhan` VarChar( 4 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
	`created_at` DateTime NULL,
	`updated_at` DateTime NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 17;
-- -------------------------------------------------------------


-- CREATE TABLE "periode" --------------------------------------
CREATE TABLE `periode` ( 
	`id` Int( 255 ) AUTO_INCREMENT NOT NULL,
	`valid_date_1` Date NOT NULL,
	`valid_date_2` Date NOT NULL,
	`from_pelabuhan_id` Int( 10 ) NOT NULL,
	`to_pelabuhan_id` Int( 10 ) NOT NULL,
	`shiping_id` Int( 10 ) NOT NULL,
	`keterangan` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`created_at` DateTime NOT NULL,
	`updated_at` DateTime NOT NULL,
	`created_by` Int( 11 ) NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 6;
-- -------------------------------------------------------------


-- CREATE TABLE "periode_detail" -------------------------------
CREATE TABLE `periode_detail` ( 
	`id` Int( 255 ) AUTO_INCREMENT NOT NULL,
	`periode_id` Int( 255 ) NOT NULL,
	`master_kontainer_id` Int( 255 ) NOT NULL,
	`created_at` DateTime NOT NULL,
	`updated_at` DateTime NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 14;
-- -------------------------------------------------------------


-- CREATE TABLE "permission_role" ------------------------------
CREATE TABLE `permission_role` ( 
	`permission_id` Int( 10 ) UNSIGNED NOT NULL,
	`role_id` Int( 10 ) UNSIGNED NOT NULL,
	PRIMARY KEY ( `permission_id`, `role_id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- CREATE TABLE "permissions" ----------------------------------
CREATE TABLE `permissions` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`display_name` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`description` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	`parent_id` Int( 255 ) NOT NULL,
	`is_menu` Int( 11 ) NOT NULL,
	`active` Int( 255 ) NOT NULL,
	`sequence_number` Int( 255 ) NULL,
	`route_permission` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`class_icon` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `permissions_name_unique` UNIQUE( `name` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 63;
-- -------------------------------------------------------------


-- CREATE TABLE "role_user" ------------------------------------
CREATE TABLE `role_user` ( 
	`role_id` Int( 10 ) UNSIGNED NOT NULL,
	`user_id` Int( 10 ) UNSIGNED NOT NULL,
	PRIMARY KEY ( `user_id`, `role_id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- CREATE TABLE "roles" ----------------------------------------
CREATE TABLE `roles` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`display_name` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`description` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	`dashboard` Int( 255 ) NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `roles_name_unique` UNIQUE( `name` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 9;
-- -------------------------------------------------------------


-- CREATE TABLE "shiping" --------------------------------------
CREATE TABLE `shiping` ( 
	`id` Int( 255 ) AUTO_INCREMENT NOT NULL,
	`nama` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`created_at` DateTime NULL,
	`updated_at` DateTime NULL,
	`kode` VarChar( 5 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = MyISAM
AUTO_INCREMENT = 26;
-- -------------------------------------------------------------


-- CREATE TABLE "users" ----------------------------------------
CREATE TABLE `users` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`username` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`email` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`password` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`remember_token` VarChar( 100 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	`active` Int( 1 ) NOT NULL DEFAULT 1,
	`avatar` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `users_username_unique` UNIQUE( `username` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 8;
-- -------------------------------------------------------------


-- Dump data of "detail_komponen" --------------------------
INSERT INTO `detail_komponen`(`id`,`master_komponen_id`,`tarif`,`created_at`,`updated_at`,`periode_detail_id`) VALUES 
( '1', '1', '10', '2022-07-04 13:02:27', '2022-07-11 20:22:13', '1' ),
( '2', '2', '12', '2022-07-04 13:02:27', '2022-07-04 13:02:27', '1' ),
( '3', '3', '12', '2022-07-04 13:02:27', '2022-07-04 13:02:27', '1' ),
( '4', '4', '23000', '2022-07-04 13:02:27', '2022-07-04 15:40:13', '1' ),
( '16', '16', '23000', '2022-07-04 13:02:27', '2022-07-04 13:02:27', '1' ),
( '17', '1', '22', '2022-07-04 13:02:28', '2022-07-04 15:40:13', '2' ),
( '18', '2', '21', '2022-07-04 13:02:28', '2022-07-04 15:40:13', '2' ),
( '31', '15', '21', '2022-07-04 13:02:28', '2022-07-04 13:02:28', '2' ),
( '32', '16', '40000', '2022-07-04 13:02:28', '2022-07-04 13:02:28', '2' ),
( '66', '2', '9', '2022-07-04 13:16:17', '2022-07-04 13:16:17', '5' ),
( '70', '6', '8', '2022-07-04 13:16:17', '2022-07-04 13:16:17', '5' ),
( '81', '1', '7', '2022-07-04 13:16:17', '2022-07-04 13:16:17', '6' ),
( '82', '2', '7', '2022-07-04 13:16:17', '2022-07-04 13:16:17', '6' ),
( '83', '3', '7', '2022-07-04 13:16:17', '2022-07-04 13:16:17', '6' ),
( '129', '1', '70', '2022-07-04 15:44:57', '2022-07-04 15:44:57', '9' ),
( '130', '2', '70', '2022-07-04 15:44:57', '2022-07-04 15:44:57', '9' ),
( '131', '3', '70', '2022-07-04 15:44:57', '2022-07-04 15:44:57', '9' ),
( '132', '4', '70', '2022-07-04 15:44:57', '2022-07-04 15:44:57', '9' ),
( '133', '16', '79000', '2022-07-04 15:44:57', '2022-07-04 15:44:57', '9' ),
( '134', '1', '100', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '135', '2', '90', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '136', '3', '40', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '137', '4', '50', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '138', '7', '40', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '139', '8', '20', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '140', '9', '10', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '141', '10', '30', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '142', '11', '40', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '143', '12', '12', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '144', '14', '24', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '145', '15', '189', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '146', '16', '100000', '2022-07-11 20:07:45', '2022-07-11 20:07:45', '10' ),
( '147', '2', '9', '2022-07-13 19:41:29', '2022-07-13 19:41:29', '11' ),
( '148', '6', '8', '2022-07-13 19:41:29', '2022-07-13 19:41:29', '11' ),
( '149', '1', '7', '2022-07-13 19:41:29', '2022-07-13 19:41:29', '12' ),
( '150', '2', '7', '2022-07-13 19:41:29', '2022-07-13 19:41:29', '12' ),
( '151', '3', '7', '2022-07-13 19:41:29', '2022-07-13 19:41:29', '12' ),
( '152', '2', '82', '2022-07-13 19:41:29', '2022-07-13 19:41:29', '13' ),
( '153', '16', '300000', '2022-07-13 19:41:29', '2022-07-13 19:41:29', '13' );
-- ---------------------------------------------------------


-- Dump data of "hpp_apply" --------------------------------
INSERT INTO `hpp_apply`(`id`,`periode_detail_id`,`apply_by`,`created_at`,`updated_at`,`status`) VALUES 
( '1', '1', '1', '2022-07-04 13:03:40', '2022-07-05 21:26:50', '1' ),
( '3', '9', '1', '2022-07-04 15:45:45', '2022-07-04 15:45:45', '1' ),
( '4', '2', '1', '2022-07-05 21:26:54', '2022-07-05 21:26:57', '0' ),
( '5', '9', '6', '2022-07-06 12:30:46', '2022-07-11 19:59:44', '1' );
-- ---------------------------------------------------------


-- Dump data of "hpp_downloaded" ---------------------------
INSERT INTO `hpp_downloaded`(`id`,`hpp_apply_id`,`created_at`,`updated_at`,`downloaded_by`) VALUES 
( '1', '1', '2022-07-04 19:46:24', '2022-07-04 19:46:24', '5' ),
( '2', '2', '2022-07-05 05:45:00', '2022-07-05 05:45:00', '5' ),
( '3', '3', '2022-07-05 19:14:31', '2022-07-05 19:14:31', '1' ),
( '4', '3', '2022-07-05 19:14:48', '2022-07-05 19:14:48', '1' ),
( '5', '3', '2022-07-06 07:34:21', '2022-07-06 07:34:21', '1' ),
( '6', '3', '2022-07-06 07:38:28', '2022-07-06 07:38:28', '1' ),
( '7', '1', '2022-07-06 07:39:00', '2022-07-06 07:39:00', '1' ),
( '8', '5', '2022-07-08 11:38:04', '2022-07-08 11:38:04', '6' );
-- ---------------------------------------------------------


-- Dump data of "hpp_margin" -------------------------------
INSERT INTO `hpp_margin`(`id`,`hpp_apply_id`,`currency`,`margin`) VALUES 
( '1', '1', 'USD', '12' ),
( '2', '1', 'IDR', '50' ),
( '3', '2', 'USD', '2' ),
( '4', '2', 'IDR', '30' ),
( '5', '3', 'USD', '20' ),
( '6', '3', 'IDR', '16' ),
( '7', '4', 'USD', '0' ),
( '8', '4', 'IDR', '0' ),
( '9', '5', 'USD', '10' ),
( '10', '5', 'IDR', '10' );
-- ---------------------------------------------------------


-- Dump data of "kota" -------------------------------------
INSERT INTO `kota`(`id`,`nama`,`provinsi_id`,`negara_id`) VALUES 
( '1', 'Kabul', '0', '1' ),
( '2', 'Tirana (Tirane)', '0', '2' ),
( '3', 'Algiers', '0', '3' ),
( '4', 'Andorra la Vella', '0', '4' ),
( '5', 'Luanda', '0', '5' ),
( '6', 'Saint John\'s', '0', '6' ),
( '7', 'Buenos Aires', '0', '7' ),
( '8', 'Yerevan', '0', '8' ),
( '9', 'Canberra', '0', '9' ),
( '10', 'Vienna', '0', '10' ),
( '11', 'Baku', '0', '11' ),
( '12', 'Nassau', '0', '12' ),
( '13', 'Manama', '0', '13' ),
( '14', 'Dhaka', '0', '14' ),
( '15', 'Bridgetown', '0', '15' ),
( '16', 'Minsk', '0', '16' ),
( '17', 'Brussels', '0', '17' ),
( '18', 'Belmopan', '0', '18' ),
( '19', 'Porto Novo', '0', '19' ),
( '20', 'Thimphu', '0', '20' ),
( '21', 'La Paz (administrative), Sucre (official)', '0', '21' ),
( '22', 'Sarajevo', '0', '22' ),
( '23', 'Gaborone', '0', '23' ),
( '24', 'Brasilia', '0', '24' ),
( '25', 'Bandar Seri Begawan', '0', '25' ),
( '26', 'Sofia', '0', '26' ),
( '27', 'Ouagadougou', '0', '27' ),
( '28', 'Gitega[3]', '0', '28' ),
( '29', 'Phnom Penh', '0', '29' ),
( '30', 'Yaounde', '0', '30' ),
( '31', 'Ottawa', '0', '31' ),
( '32', 'Praia', '0', '32' ),
( '33', 'Bangui', '0', '33' ),
( '34', 'N\'Djamena', '0', '34' ),
( '35', 'Santiago', '0', '35' ),
( '36', 'Beijing', '0', '36' ),
( '37', 'Bogota', '0', '37' ),
( '38', 'Moroni', '0', '38' ),
( '39', 'Kinshasa', '0', '39' ),
( '40', 'Brazzaville', '0', '40' ),
( '41', 'San Jose', '0', '41' ),
( '42', 'Yamoussoukro', '0', '42' ),
( '43', 'Zagreb', '0', '43' ),
( '44', 'Havana', '0', '44' ),
( '45', 'Nicosia', '0', '45' ),
( '46', 'Prague', '0', '46' ),
( '47', 'Copenhagen', '0', '47' ),
( '48', 'Djibouti', '0', '48' ),
( '49', 'Roseau', '0', '49' ),
( '50', 'Santo Domingo', '0', '50' ),
( '51', 'Dili', '0', '51' ),
( '52', 'Quito', '0', '52' ),
( '53', 'Cairo', '0', '53' ),
( '54', 'San Salvador', '0', '54' ),
( '55', 'London', '0', '55' ),
( '56', 'Malabo', '0', '56' ),
( '57', 'Asmara', '0', '57' ),
( '58', 'Tallinn', '0', '58' ),
( '59', 'Mbabana', '0', '59' ),
( '60', 'Addis Ababa', '0', '60' ),
( '61', 'Palikir', '0', '61' ),
( '62', 'Suva', '0', '62' ),
( '63', 'Helsinki', '0', '63' ),
( '64', 'Paris', '0', '64' ),
( '65', 'Libreville', '0', '65' ),
( '66', 'Banjul', '0', '66' ),
( '67', 'Tbilisi', '0', '67' ),
( '68', 'Berlin', '0', '68' ),
( '69', 'Accra', '0', '69' ),
( '70', 'Athens', '0', '70' ),
( '71', 'Saint George\'s', '0', '71' ),
( '72', 'Guatemala City', '0', '72' ),
( '73', 'Conakry', '0', '73' ),
( '74', 'Bissau', '0', '74' ),
( '75', 'Georgetown', '0', '75' ),
( '76', 'Port au Prince', '0', '76' ),
( '77', 'Tegucigalpa', '0', '77' ),
( '78', 'Budapest', '0', '78' ),
( '79', 'Reykjavik', '0', '79' ),
( '80', 'New Delhi', '0', '80' ),
( '81', 'Jakarta', '0', '81' ),
( '82', 'Tehran', '0', '82' ),
( '83', 'Baghdad', '0', '83' ),
( '84', 'Dublin', '0', '84' ),
( '85', 'Jerusalem', '0', '85' ),
( '86', 'Rome', '0', '86' ),
( '87', 'Kingston', '0', '87' ),
( '88', 'Tokyo', '0', '88' ),
( '89', 'Amman', '0', '89' ),
( '90', 'Nur-Sultan', '0', '90' ),
( '91', 'Nairobi', '0', '91' ),
( '92', 'Tarawa Atoll', '0', '92' ),
( '93', 'Pristina', '0', '93' ),
( '94', 'Kuwait City', '0', '94' ),
( '95', 'Bishkek', '0', '95' ),
( '96', 'Vientiane', '0', '96' ),
( '97', 'Riga', '0', '97' ),
( '98', 'Beirut', '0', '98' ),
( '99', 'Maseru', '0', '99' ),
( '100', 'Monrovia', '0', '100' ),
( '101', 'Tripoli', '0', '101' ),
( '102', 'Vaduz', '0', '102' ),
( '103', 'Vilnius', '0', '103' ),
( '104', 'Luxembourg', '0', '104' ),
( '105', 'Antananarivo', '0', '105' ),
( '106', 'Lilongwe', '0', '106' ),
( '107', 'Kuala Lumpur', '0', '107' ),
( '108', 'Male', '0', '108' ),
( '109', 'Bamako', '0', '109' ),
( '110', 'Valletta', '0', '110' ),
( '111', 'Majuro', '0', '111' ),
( '112', 'Nouakchott', '0', '112' ),
( '113', 'Port Louis', '0', '113' ),
( '114', 'Mexico City', '0', '114' ),
( '115', 'Chisinau', '0', '115' ),
( '116', 'Monaco', '0', '116' ),
( '117', 'Ulaanbaatar', '0', '117' ),
( '118', 'Podgorica', '0', '118' ),
( '119', 'Rabat', '0', '119' ),
( '120', 'Maputo', '0', '120' ),
( '121', 'Nay Pyi Taw', '0', '121' ),
( '122', 'Windhoek', '0', '122' ),
( '123', 'No official capital', '0', '123' ),
( '124', 'Kathmandu', '0', '124' ),
( '125', 'Amsterdam', '0', '125' ),
( '126', 'Wellington', '0', '126' ),
( '127', 'Managua', '0', '127' ),
( '128', 'Niamey', '0', '128' ),
( '129', 'Abuja', '0', '129' ),
( '130', 'Pyongyang', '0', '130' ),
( '131', 'Skopje', '0', '131' ),
( '132', 'Belfast', '0', '132' ),
( '133', 'Oslo', '0', '133' ),
( '134', 'Muscat', '0', '134' ),
( '135', 'Islamabad', '0', '135' ),
( '136', 'Melekeok', '0', '136' ),
( '137', 'Panama City', '0', '137' ),
( '138', 'Port Moresby', '0', '138' ),
( '139', 'Asuncion', '0', '139' ),
( '140', 'Lima', '0', '140' ),
( '141', 'Manila', '0', '141' ),
( '142', 'Warsaw', '0', '142' ),
( '143', 'Lisbon', '0', '143' ),
( '144', 'Doha', '0', '144' ),
( '145', 'Bucharest', '0', '145' ),
( '146', 'Moscow', '0', '146' ),
( '147', 'Kigali', '0', '147' ),
( '148', 'Basseterre', '0', '148' ),
( '149', 'Castries', '0', '149' ),
( '150', 'Kingstown', '0', '150' ),
( '151', 'Apia', '0', '151' ),
( '152', 'San Marino', '0', '152' ),
( '153', 'Sao Tome', '0', '153' ),
( '154', 'Riyadh', '0', '154' ),
( '155', 'Edinburgh', '0', '155' ),
( '156', 'Dakar', '0', '156' ),
( '157', 'Belgrade', '0', '157' ),
( '158', 'Victoria', '0', '158' ),
( '159', 'Freetown', '0', '159' ),
( '160', 'Singapore', '0', '160' ),
( '161', 'Bratislava', '0', '161' ),
( '162', 'Ljubljana', '0', '162' ),
( '163', 'Honiara', '0', '163' ),
( '164', 'Mogadishu', '0', '164' ),
( '165', 'Pretoria, Bloemfontein, Cape Town', '0', '165' ),
( '166', 'Seoul', '0', '166' ),
( '167', 'Juba', '0', '167' ),
( '168', 'Madrid', '0', '168' ),
( '169', 'Sri Jayawardenapura Kotte', '0', '169' ),
( '170', 'Khartoum', '0', '170' ),
( '171', 'Paramaribo', '0', '171' ),
( '172', 'Stockholm', '0', '172' ),
( '173', 'Bern', '0', '173' ),
( '174', 'Damascus', '0', '174' ),
( '175', 'Taipei', '0', '175' ),
( '176', 'Dushanbe', '0', '176' ),
( '177', 'Dodoma', '0', '177' ),
( '178', 'Bangkok', '0', '178' ),
( '179', 'Lome', '0', '179' ),
( '180', 'Nuku\'alofa', '0', '180' ),
( '181', 'Port of Spain', '0', '181' ),
( '182', 'Tunis', '0', '182' ),
( '183', 'Ankara', '0', '183' ),
( '184', 'Ashgabat', '0', '184' ),
( '185', 'Funafuti', '0', '185' ),
( '186', 'Kampala', '0', '186' ),
( '187', 'Kyiv or Kiev', '0', '187' ),
( '188', 'Abu Dhabi', '0', '188' ),
( '189', 'London', '0', '189' ),
( '190', 'Washington D.C.', '0', '190' ),
( '191', 'Montevideo', '0', '191' ),
( '192', 'Tashkent', '0', '192' ),
( '193', 'Port Vila', '0', '193' ),
( '194', 'Vatican City', '0', '194' ),
( '195', 'Caracas', '0', '195' ),
( '196', 'Hanoi', '0', '196' ),
( '198', 'Sana\'a', '0', '198' ),
( '199', 'Lusaka', '0', '199' ),
( '200', 'Harare', '0', '200' ),
( '201', 'Surabaya', '0', '81' ),
( '202', 'Semarang', '0', '81' ),
( '203', 'Horisima', '0', '88' ),
( '204', 'Gresik', '0', '81' );
-- ---------------------------------------------------------


-- Dump data of "master_kategori" --------------------------
INSERT INTO `master_kategori`(`id`,`nama`,`created_at`,`updated_at`,`satuan`) VALUES 
( '1', 'Dry', NULL, NULL, 'ft' ),
( '2', 'Reefer', NULL, NULL, 'rf' );
-- ---------------------------------------------------------


-- Dump data of "master_komponen" --------------------------
INSERT INTO `master_komponen`(`id`,`nama`,`currency`,`created_at`,`updated_at`) VALUES 
( '1', 'OF', 'USD', NULL, NULL ),
( '2', 'THCL', 'USD', NULL, NULL ),
( '3', 'LSS', 'USD', NULL, NULL ),
( '4', 'OWS', 'USD', NULL, NULL ),
( '5', 'ISOCC', 'USD', NULL, NULL ),
( '6', 'RC SHIPPING', 'USD', NULL, NULL ),
( '7', 'RC SHIPPER', 'USD', NULL, NULL ),
( '8', 'AMS', 'USD', NULL, NULL ),
( '9', 'THCD', 'USD', NULL, NULL ),
( '10', 'BK', 'USD', NULL, NULL ),
( '11', 'LSA', 'USD', NULL, NULL ),
( '12', 'CHENAL FEE', 'USD', NULL, NULL ),
( '14', 'EBS', 'USD', NULL, NULL ),
( '15', 'CRS', 'USD', NULL, NULL ),
( '16', 'DOC', 'IDR', '2022-07-04 10:29:37', '2022-07-04 10:29:37' );
-- ---------------------------------------------------------


-- Dump data of "master_kontainer" -------------------------
INSERT INTO `master_kontainer`(`id`,`ukuran_id`,`kategori_id`) VALUES 
( '1', '1', '1' ),
( '2', '2', '1' ),
( '3', '1', '2' ),
( '4', '2', '2' );
-- ---------------------------------------------------------


-- Dump data of "master_ukuran" ----------------------------
INSERT INTO `master_ukuran`(`id`,`ukuran`) VALUES 
( '1', '20' ),
( '2', '40' );
-- ---------------------------------------------------------


-- Dump data of "migrations" -------------------------------
INSERT INTO `migrations`(`id`,`migration`,`batch`) VALUES 
( '3', '2014_10_12_000000_create_users_table', '1' ),
( '4', '2014_10_12_100000_create_password_resets_table', '1' ),
( '5', '2022_06_08_234430_entrust_setup_tables', '2' );
-- ---------------------------------------------------------


-- Dump data of "negara" -----------------------------------
INSERT INTO `negara`(`id`,`nama`,`code`) VALUES 
( '1', 'Afghanistan', 'AF' ),
( '2', 'Albania', 'AL' ),
( '3', 'Algeria', 'DZ' ),
( '4', 'Andorra', 'AD' ),
( '5', 'Angola', 'AO' ),
( '6', 'Antigua & Barbuda', 'AG' ),
( '7', 'Argentina', 'AR' ),
( '8', 'Armenia', 'AM' ),
( '9', 'Australia', 'AU' ),
( '10', 'Austria', 'AT' ),
( '11', 'Azerbaijan', 'AZ' ),
( '12', 'Bahamas', 'BS' ),
( '13', 'Bahrain', 'BH' ),
( '14', 'Bangladesh', 'BD' ),
( '15', 'Barbados', 'BB' ),
( '16', 'Belarus', 'BY' ),
( '17', 'Belgium', 'BE' ),
( '18', 'Belize', 'BZ' ),
( '19', 'Benin', 'BJ' ),
( '20', 'Bhutan', 'BT' ),
( '21', 'Bolivia', 'BT' ),
( '22', 'Bosnia & Herzegovina', 'BA' ),
( '23', 'Botswana', 'BW' ),
( '24', 'Brazil', 'BR' ),
( '25', 'Brunei', 'IO' ),
( '26', 'Bulgaria', 'BG' ),
( '27', 'Burkina Faso', 'BF' ),
( '28', 'Burundi', 'BI' ),
( '29', 'Cambodia', 'KH' ),
( '30', 'Cameroon', 'CM' ),
( '31', 'Canada', 'CA' ),
( '32', 'Cape Verde', 'CA' ),
( '33', 'Central African Republic', 'KY' ),
( '34', 'Chad', 'TD' ),
( '35', 'Chile', 'CL' ),
( '36', 'China', 'CN' ),
( '37', 'Colombia', 'CO' ),
( '38', 'Comoros', 'CO' ),
( '39', 'Congo, Democratic Republic of the', 'CG' ),
( '40', 'Congo, Republic of the', 'CG' ),
( '41', 'Costa Rica', 'CR' ),
( '42', 'Côte d\'Ivoire (Ivory Coast),', 'CR' ),
( '43', 'Croatia', 'HR' ),
( '44', 'Cuba', 'CU' ),
( '45', 'Cyprus', 'CY' ),
( '46', 'Czech Republic (Czechia),', 'CY' ),
( '47', 'Denmark', 'DK' ),
( '48', 'Djibouti', 'DJ' ),
( '49', 'Dominica', 'DM' ),
( '50', 'Dominican Republic', 'DM' ),
( '51', 'East Timor', 'DO' ),
( '52', 'Ecuador', 'EC' ),
( '53', 'Egypt', 'EG' ),
( '54', 'El Salvador', 'SV' ),
( '55', 'England', 'SV' ),
( '56', 'Equatorial Guinea', 'GQ' ),
( '57', 'Eritrea', 'ER' ),
( '58', 'Estonia', 'EE' ),
( '59', 'Eswatini (Swaziland),', 'SZ' ),
( '60', 'Ethiopia', 'ET' ),
( '61', 'Federated States of Micronesia', 'FO' ),
( '62', 'Fiji', 'FJ' ),
( '63', 'Finland', 'FI' ),
( '64', 'France', 'FR' ),
( '65', 'Gabon', 'GA' ),
( '66', 'Gambia', 'GA' ),
( '67', 'Georgia', 'GE' ),
( '68', 'Germany', 'DE' ),
( '69', 'Ghana', 'GH' ),
( '70', 'Greece', 'GR' ),
( '71', 'Grenada', 'GD' ),
( '72', 'Guatemala', 'GT' ),
( '73', 'Guinea', 'GN' ),
( '74', 'Guinea-Bissau', 'GW' ),
( '75', 'Guyana', 'GY' ),
( '76', 'Haiti', 'HT' ),
( '77', 'Honduras', 'HN' ),
( '78', 'Hungary', 'HU' ),
( '79', 'Iceland', 'IS' ),
( '80', 'India', 'IN' ),
( '81', 'Indonesia', 'ID' ),
( '82', 'Iran', 'IR' ),
( '83', 'Iraq', 'IQ' ),
( '84', 'Ireland', 'IE' ),
( '85', 'Israel', 'IL' ),
( '86', 'Italy', 'IT' ),
( '87', 'Jamaica', 'JM' ),
( '88', 'Japan', 'JP' ),
( '89', 'Jordan', 'JO' ),
( '90', 'Kazakhstan', 'KZ' ),
( '91', 'Kenya', 'KE' ),
( '92', 'Kiribati', 'KI' ),
( '93', 'Kosovo', 'KR' ),
( '94', 'Kuwait', 'KW' ),
( '95', 'Kyrgyzstan', 'KG' ),
( '96', 'Laos', 'LA' ),
( '97', 'Latvia', 'LV' ),
( '98', 'Lebanon', 'LB' ),
( '99', 'Lesotho', 'LS' ),
( '100', 'Liberia', 'LR' ),
( '101', 'Libya', 'LY' ),
( '102', 'Liechtenstein', 'LI' ),
( '103', 'Lithuania', 'LT' ),
( '104', 'Luxembourg', 'LU' ),
( '105', 'Madagascar', 'MG' ),
( '106', 'Malawi', 'MW' ),
( '107', 'Malaysia', 'MY' ),
( '108', 'Maldives', 'MV' ),
( '109', 'Mali', 'ML' ),
( '110', 'Malta', 'MT' ),
( '111', 'Marshall Islands', 'MT' ),
( '112', 'Mauritania', 'MR' ),
( '113', 'Mauritius', 'MU' ),
( '114', 'Mexico', 'MX' ),
( '115', 'Moldova', 'FM' ),
( '116', 'Monaco', 'MC' ),
( '117', 'Mongolia', 'MN' ),
( '118', 'Montenegro', 'ME' ),
( '119', 'Morocco', 'MA' ),
( '120', 'Mozambique', 'MZ' ),
( '121', 'Myanmar (Burma),', 'MM' ),
( '122', 'Namibia', 'NA' ),
( '123', 'Nauru', 'NR' ),
( '124', 'Nepal', 'NP' ),
( '125', 'Netherlands', 'NP' ),
( '126', 'New Zealand', 'NZ' ),
( '127', 'Nicaragua', 'NI' ),
( '128', 'Niger', 'NI' ),
( '129', 'Nigeria', 'NG' ),
( '130', 'North Korea', 'NF' ),
( '131', 'North Macedonia (Macedonia),', 'NF' ),
( '132', 'Northern Ireland', 'NF' ),
( '133', 'Norway', 'NO' ),
( '134', 'Oman', 'OM' ),
( '135', 'Pakistan', 'PK' ),
( '136', 'Palau', 'PW' ),
( '137', 'Panama', 'PA' ),
( '138', 'Papua New Guinea', 'PG' ),
( '139', 'Paraguay', 'PY' ),
( '140', 'Peru', 'PE' ),
( '141', 'Philippines', 'PE' ),
( '142', 'Poland', 'PL' ),
( '143', 'Portugal', 'PT' ),
( '144', 'Qatar', 'QA' ),
( '145', 'Romania', 'RO' ),
( '146', 'Russia', 'RO' ),
( '147', 'Rwanda', 'RW' ),
( '148', 'Saint Kitts & Nevis', 'KN' ),
( '149', 'Saint Lucia', 'LC' ),
( '150', 'Saint Vincent & the Grenadines', 'VC' ),
( '151', 'Samoa', 'WS' ),
( '152', 'San Marino', 'SM' ),
( '153', 'Sao Tome & Principe', 'ST' ),
( '154', 'Saudi Arabia', 'SA' ),
( '155', 'Scotland', 'SA' ),
( '156', 'Senegal', 'SN' ),
( '157', 'Serbia', 'RS' ),
( '158', 'Seychelles', 'SC' ),
( '159', 'Sierra Leone', 'SL' ),
( '160', 'Singapore', 'SG' ),
( '161', 'Slovakia', 'SK' ),
( '162', 'Slovenia', 'SI' ),
( '163', 'Solomon Islands', 'SB' ),
( '164', 'Somalia', 'SO' ),
( '165', 'South Africa', 'ZA' ),
( '166', 'South Korea', 'GS' ),
( '167', 'South Sudan', 'SS' ),
( '168', 'Spain', 'ES' ),
( '169', 'Sri Lanka', 'LK' ),
( '170', 'Sudan', 'LK' ),
( '171', 'Suriname', 'SR' ),
( '172', 'Sweden', 'SE' ),
( '173', 'Switzerland', 'CH' ),
( '174', 'Syria', 'CH' ),
( '175', 'Taiwan[19]', 'TW' ),
( '176', 'Tajikistan', 'TJ' ),
( '177', 'Tanzania', 'TJ' ),
( '178', 'Thailand', 'TH' ),
( '179', 'Togo', 'TG' ),
( '180', 'Tonga', 'TO' ),
( '181', 'Trinidad & Tobago', 'TT' ),
( '182', 'Tunisia', 'TN' ),
( '183', 'Turkey', 'TR' ),
( '184', 'Turkmenistan', 'TM' ),
( '185', 'Tuvalu', 'TV' ),
( '186', 'Uganda', 'UG' ),
( '187', 'Ukraine', 'UA' ),
( '188', 'United Arab Emirates', 'UA' ),
( '189', 'United Kingdom', 'AE' ),
( '190', 'United States', 'GB' ),
( '191', 'Uruguay', 'UY' ),
( '192', 'Uzbekistan', 'UZ' ),
( '193', 'Vanuatu', 'VU' ),
( '194', 'Vatican City', 'VU' ),
( '195', 'Venezuela', 'VU' ),
( '196', 'Vietnam', 'VN' ),
( '197', 'Wales', 'VI' ),
( '198', 'Yemen', 'YE' ),
( '199', 'Zambia', 'ZM' ),
( '200', 'Zimbabwe', 'ZW' ),
( '201', 'Wakanda', 'WK' );
-- ---------------------------------------------------------


-- Dump data of "password_resets" --------------------------
-- ---------------------------------------------------------


-- Dump data of "pelabuhan" --------------------------------
INSERT INTO `pelabuhan`(`id`,`nama_pelabuhan`,`kota_id`,`alamat`,`kode_pelabuhan`,`created_at`,`updated_at`) VALUES 
( '1', 'Tanjung Perak', '201', NULL, NULL, '2022-07-04 10:26:07', '2022-07-04 10:26:07' ),
( '2', 'Tanjung Priok', '81', NULL, NULL, '2022-07-04 10:27:53', '2022-07-04 10:27:53' ),
( '3', 'Tanjung Emas', '202', NULL, NULL, '2022-07-04 10:29:09', '2022-07-13 09:04:04' ),
( '4', 'SAHATAI', '178', NULL, NULL, '2022-07-05 07:15:57', '2022-07-05 07:15:57' ),
( '5', 'Teluk Lamong', '201', NULL, NULL, '2022-07-12 21:01:03', '2022-07-12 21:01:03' ),
( '7', NULL, NULL, NULL, NULL, '2022-07-12 21:55:41', '2022-07-12 21:55:41' ),
( '16', 'Semen Gresik', '204', NULL, NULL, '2022-07-13 15:08:24', '2022-07-13 15:08:24' );
-- ---------------------------------------------------------


-- Dump data of "periode" ----------------------------------
INSERT INTO `periode`(`id`,`valid_date_1`,`valid_date_2`,`from_pelabuhan_id`,`to_pelabuhan_id`,`shiping_id`,`keterangan`,`created_at`,`updated_at`,`created_by`) VALUES 
( '1', '2022-07-04', '2022-07-31', '3', '1', '1', NULL, '2022-07-04 13:02:27', '2022-07-04 13:02:27', NULL ),
( '2', '2022-07-01', '2022-07-30', '3', '1', '4', NULL, '2022-07-04 13:16:17', '2022-07-13 15:49:05', NULL ),
( '3', '2022-07-01', '2022-07-09', '1', '4', '5', NULL, '1970-01-01 07:00:01', '2022-07-06 02:44:20', NULL ),
( '4', '2022-07-11', '2022-07-16', '1', '4', '4', 'bulan juni update', '1970-01-01 07:00:02', '2022-07-11 20:07:45', NULL ),
( '5', '2022-08-01', '2022-08-31', '3', '1', '4', NULL, '1970-01-01 07:00:02', '2022-07-13 19:41:29', NULL );
-- ---------------------------------------------------------


-- Dump data of "periode_detail" ---------------------------
INSERT INTO `periode_detail`(`id`,`periode_id`,`master_kontainer_id`,`created_at`,`updated_at`) VALUES 
( '1', '1', '1', '2022-07-04 13:02:27', '2022-07-04 13:02:27' ),
( '2', '1', '2', '2022-07-04 13:02:28', '2022-07-04 13:02:28' ),
( '5', '2', '1', '2022-07-04 13:16:17', '2022-07-04 13:16:17' ),
( '6', '2', '2', '2022-07-04 13:16:17', '2022-07-04 13:16:17' ),
( '9', '3', '3', '2022-07-04 15:44:57', '2022-07-04 15:44:57' ),
( '10', '4', '2', '2022-07-11 20:07:45', '2022-07-11 20:07:45' ),
( '11', '5', '1', '2022-07-13 19:41:29', '2022-07-13 19:41:29' ),
( '12', '5', '2', '2022-07-13 19:41:29', '2022-07-13 19:41:29' ),
( '13', '5', '3', '2022-07-13 19:41:29', '2022-07-13 19:41:29' );
-- ---------------------------------------------------------


-- Dump data of "permission_role" --------------------------
INSERT INTO `permission_role`(`permission_id`,`role_id`) VALUES 
( '24', '1' ),
( '37', '1' ),
( '46', '1' ),
( '48', '1' ),
( '49', '1' ),
( '62', '1' ),
( '1', '2' ),
( '2', '2' ),
( '3', '2' ),
( '4', '2' ),
( '6', '2' ),
( '7', '2' ),
( '8', '2' ),
( '9', '2' ),
( '10', '2' ),
( '14', '2' ),
( '19', '2' ),
( '21', '2' ),
( '22', '2' ),
( '23', '2' ),
( '24', '2' ),
( '25', '2' ),
( '24', '6' ),
( '37', '6' ),
( '46', '6' ),
( '47', '6' ),
( '49', '6' ),
( '58', '6' ),
( '4', '7' ),
( '6', '7' ),
( '7', '7' ),
( '24', '7' ),
( '29', '7' ),
( '30', '7' ),
( '35', '7' ),
( '36', '7' ),
( '37', '7' ),
( '38', '7' ),
( '39', '7' ),
( '42', '7' ),
( '44', '7' ),
( '45', '7' ),
( '46', '7' ),
( '50', '7' ),
( '51', '7' ),
( '52', '7' ),
( '53', '7' ),
( '54', '7' ),
( '55', '7' ),
( '56', '7' ),
( '60', '7' ),
( '61', '7' ),
( '1', '8' ),
( '2', '8' ),
( '3', '8' ),
( '7', '8' ),
( '8', '8' ),
( '9', '8' ),
( '10', '8' ),
( '14', '8' ),
( '19', '8' ),
( '21', '8' ),
( '22', '8' ),
( '23', '8' ),
( '24', '8' ),
( '25', '8' );
-- ---------------------------------------------------------


-- Dump data of "permissions" ------------------------------
INSERT INTO `permissions`(`id`,`name`,`display_name`,`description`,`created_at`,`updated_at`,`parent_id`,`is_menu`,`active`,`sequence_number`,`route_permission`,`class_icon`) VALUES 
( '1', 'Permission', 'Manajemen Akses', NULL, NULL, NULL, '0', '1', '1', '1', NULL, 'bx bxs-component' ),
( '2', 'User', 'Master User', '', NULL, '2022-06-24 00:37:19', '1', '1', '1', '2', 'master/user', 'bi bi-circle' ),
( '3', 'Menu', 'Master Menu', NULL, NULL, '2022-06-22 12:18:32', '1', '1', '1', '4', 'master/permission', 'bi bi-circle' ),
( '4', 'Master Data', 'Master Data', NULL, NULL, '2022-06-23 06:13:32', '0', '1', '1', '2', NULL, 'bx bxs-component' ),
( '6', 'Shiping', 'Shiping', NULL, NULL, '2022-06-25 03:11:12', '4', '1', '1', '2', 'transaction/shiping', 'bi bi-circle' ),
( '7', 'edit.user', 'Edit Master User', NULL, NULL, NULL, '0', '0', '1', NULL, 'master/user/edit/{id}', NULL ),
( '8', 'user.add', 'Add User', NULL, NULL, '2022-06-22 12:18:32', '1', '1', '1', '1', 'master/user/create', 'bi bi-circle' ),
( '9', 'add.menu', 'Add Menu', NULL, NULL, '2022-06-22 12:18:32', '1', '1', '1', '3', 'master/permission/create', 'bi bi-circle' ),
( '10', 'edit.menu', 'Edit Menu', NULL, NULL, NULL, '2', '0', '1', NULL, 'master/permission/edit/{id}', NULL ),
( '14', 'roles.create', 'Add Role', NULL, '2022-06-21 06:05:10', '2022-06-22 12:18:32', '1', '1', '1', '5', 'master/roles/create', 'bi bi-circle' ),
( '19', 'roles', 'Master Roles', NULL, '2022-06-21 07:55:26', '2022-06-22 12:18:32', '1', '1', '1', '6', 'master/roles', 'bi bi-circle' ),
( '21', 'delete.menu.{id}', NULL, NULL, '2022-06-21 10:54:42', '2022-06-21 10:55:01', '0', '0', '1', NULL, 'master/permission/delete/{id}', NULL ),
( '22', 'User Role', NULL, NULL, '2022-06-22 12:51:58', '2022-06-22 12:54:16', '0', '0', '1', NULL, 'master/user_role/{id}', NULL ),
( '23', 'Permission Role', NULL, NULL, '2022-06-22 13:40:40', '2022-06-22 13:40:40', '0', '0', '1', NULL, 'master/permission_role/{id}', NULL ),
( '24', 'Profil User', NULL, NULL, '2022-06-23 01:47:45', '2022-06-23 01:47:45', '0', '0', '1', NULL, 'master/user/profil/{id}', NULL ),
( '25', 'Edit Master Roles', NULL, NULL, '2022-06-23 06:10:12', '2022-06-23 06:10:12', '0', '0', '1', NULL, 'master/roles/edit/{id}', NULL ),
( '29', 'Pelabuhan, Kota & Negara', 'Pelabuhan, Kota & Negara', NULL, '2022-06-23 13:42:37', '2022-07-12 22:12:11', '4', '1', '1', '8', 'transaction/pelabuhan', 'bi bi-circle' ),
( '30', 'Data Periode', 'Data Periode', NULL, '2022-06-23 13:46:51', '2022-06-25 01:38:51', '37', '1', '1', '2', 'transaction/periode', 'bi bi-circle' ),
( '32', 'Provinsi', 'Provinsi', NULL, '2022-06-24 08:46:03', '2022-06-25 01:37:41', '4', '1', '0', '4', 'transaction/provinsi', 'bi bi-circle' ),
( '34', 'Edit Provinsi', NULL, NULL, '2022-06-24 08:49:06', '2022-06-24 08:49:06', '0', '0', '1', NULL, 'transaction/provinsi/{id}', NULL ),
( '35', 'Submit Kota', NULL, NULL, '2022-06-24 08:49:19', '2022-07-13 13:15:58', '0', '0', '1', NULL, 'transaction/kota/submit', NULL ),
( '36', 'Submit Negara', NULL, NULL, '2022-06-24 08:49:28', '2022-07-13 15:03:04', '0', '0', '1', NULL, 'transaction/negara/submit', NULL ),
( '37', 'Transaksi', 'Transaksi', NULL, '2022-06-25 01:38:12', '2022-06-25 01:38:12', '0', '1', '1', '3', NULL, 'bx bxs-component' ),
( '38', 'Komponen', 'Komponen', NULL, '2022-06-25 03:06:19', '2022-06-25 03:11:12', '4', '1', '1', '10', 'transaction/komponen', 'bi bi-circle' ),
( '39', 'Submit Komponen', 'Tambah Komponen', NULL, '2022-06-25 03:07:17', '2022-07-13 15:41:33', '0', '0', '1', '9', 'transaction/komponen/submit', 'bi bi-circle' ),
( '42', 'Tambah Pelabuhan', 'Tambah Pelabuhan', NULL, '2022-06-25 03:09:55', '2022-07-12 20:46:32', '0', '0', '1', '7', 'transaction/pelabuhan/submit', 'bi bi-circle' ),
( '44', 'Tambah Data Periode', 'Tambah Data Periode', NULL, '2022-06-25 23:46:41', '2022-06-25 23:46:41', '37', '1', '1', '1', 'transaction/periode/form', 'bi bi-circle' ),
( '45', 'Edit Data Periode', NULL, NULL, '2022-06-25 23:47:07', '2022-07-05 14:16:22', '0', '0', '1', NULL, 'transaction/periode/edit/{id}', NULL ),
( '46', 'Search', 'Search', NULL, '2022-06-27 05:33:59', '2022-06-27 05:33:59', '37', '1', '1', '3', 'transaction/search', 'bi bi-circle' ),
( '47', 'HPP Apply', 'HPP Apply', NULL, '2022-06-29 14:01:21', '2022-06-29 14:01:21', '37', '1', '1', '4', 'transaction/hpp/apply', 'bi bi-circle' ),
( '48', 'Hpp Apply CS', 'Hpp Apply CS', NULL, '2022-07-04 13:21:07', '2022-07-04 13:21:07', '37', '1', '1', '5', 'transaction/hpp/cs', 'bi bi-circle' ),
( '49', 'Download Hpp Apply', NULL, NULL, '2022-07-04 19:43:03', '2022-07-04 19:43:03', '0', '0', '1', NULL, 'transaction/hpp/apply/download/{id}', NULL ),
( '50', 'Delete Pelabuhan', NULL, NULL, '2022-07-04 19:55:52', '2022-07-04 19:55:52', '0', '0', '1', NULL, 'transaction/pelabuhan/delete/{id}', NULL ),
( '51', 'Delete Negara', NULL, NULL, '2022-07-04 19:56:17', '2022-07-04 19:56:17', '0', '0', '1', NULL, 'transaction/negara/delete/{id}', NULL ),
( '52', 'Delete Kota', NULL, NULL, '2022-07-04 19:56:39', '2022-07-04 19:56:39', '0', '0', '1', NULL, 'transaction/kota/delete/{id}', NULL ),
( '53', 'Delete Komponen', NULL, NULL, '2022-07-04 19:57:04', '2022-07-04 19:57:04', '0', '0', '1', NULL, 'transaction/komponen/delete/{id}', NULL ),
( '54', 'Delete Shiping', NULL, NULL, '2022-07-04 19:57:28', '2022-07-04 19:57:28', '0', '0', '1', NULL, 'transaction/shiping/delete/{id}', NULL ),
( '55', 'Delete Periode', NULL, NULL, '2022-07-04 20:04:43', '2022-07-04 20:04:43', '0', '0', '1', NULL, 'transaction/periode/delete/{id}', NULL ),
( '56', 'Delete Hpp Apply', NULL, NULL, '2022-07-04 20:05:16', '2022-07-04 20:05:16', '0', '0', '1', NULL, 'transaction/hpp/apply/delete/{id}', NULL ),
( '58', 'Hpp Applyed', NULL, NULL, '2022-07-05 20:18:47', '2022-07-05 20:18:47', '0', '0', '1', NULL, 'transaction/hpp/{id}', NULL ),
( '59', 'Dashboard', NULL, NULL, '2022-07-12 05:47:55', '2022-07-12 05:47:55', '0', '0', '1', NULL, 'home/dashboard', NULL ),
( '60', 'SubmitPricing', NULL, NULL, '2022-07-12 13:26:53', '2022-07-13 15:11:21', '0', '0', '1', NULL, 'transaction/shiping/submit', NULL ),
( '61', 'Duplicate', NULL, NULL, '2022-07-13 19:40:21', '2022-07-13 19:40:21', '0', '0', '1', NULL, 'transaction/periode/copy/{id}', NULL ),
( '62', 'Search CS', 'Search CS', NULL, '2022-07-13 19:57:26', '2022-07-13 19:57:26', '37', '1', '1', '6', 'transaction/search/cs', 'bi bi-circle' );
-- ---------------------------------------------------------


-- Dump data of "role_user" --------------------------------
INSERT INTO `role_user`(`role_id`,`user_id`) VALUES 
( '1', '5' ),
( '2', '1' ),
( '2', '3' ),
( '6', '4' ),
( '6', '6' ),
( '7', '2' ),
( '8', '7' );
-- ---------------------------------------------------------


-- Dump data of "roles" ------------------------------------
INSERT INTO `roles`(`id`,`name`,`display_name`,`description`,`created_at`,`updated_at`,`dashboard`) VALUES 
( '1', 'CS', 'CS', 'Sebagai CS', '2022-06-21 14:51:16', '2022-07-04 19:30:29', NULL ),
( '2', 'Developer', 'Developer', 'Sebagai developer untuk mengatur menu', '2022-06-21 14:52:18', '2022-06-22 04:13:15', NULL ),
( '6', 'Marketing', 'Markeketing', 'Marketing', '2022-06-22 12:20:18', '2022-06-23 06:11:42', NULL ),
( '7', 'Pricing', 'Pricing', 'Pricing', '2022-06-22 12:20:32', '2022-06-23 06:11:12', '1' ),
( '8', 'Akses Manajemen', 'Akses Manajemen', 'Akses Manajemen', '2022-07-11 20:42:21', '2022-07-11 20:43:00', NULL );
-- ---------------------------------------------------------


-- Dump data of "shiping" ----------------------------------
INSERT INTO `shiping`(`id`,`nama`,`created_at`,`updated_at`,`kode`) VALUES 
( '1', 'WAN HAI', '2022-07-03 20:21:00', '2022-07-03 20:21:00', NULL ),
( '2', 'OOCL', NULL, NULL, NULL ),
( '3', 'KMTC', NULL, NULL, NULL ),
( '4', 'EVERGREEN', NULL, NULL, NULL ),
( '5', 'ONE LINE', NULL, NULL, NULL ),
( '6', 'YANGMING', NULL, NULL, NULL ),
( '7', 'MSC', NULL, NULL, NULL ),
( '8', 'PIL', NULL, NULL, NULL ),
( '9', 'SEALAND', NULL, NULL, NULL ),
( '10', 'MAERSKLINE', NULL, NULL, NULL ),
( '11', 'HAPAG', NULL, NULL, NULL ),
( '12', 'HYUNDAI', NULL, NULL, NULL ),
( '13', 'KAISO IX', NULL, '2022-07-12 20:16:06', NULL ),
( '25', 'SHARPS', '2022-07-13 15:08:44', '2022-07-13 15:08:50', NULL );
-- ---------------------------------------------------------


-- Dump data of "users" ------------------------------------
INSERT INTO `users`(`id`,`name`,`username`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`active`,`avatar`) VALUES 
( '1', 'Root', 'root', 'root@root.com', '$2y$10$IufnGC6The90HzfWHW3h6evnEx7JZiBDw0INTcGkzObfb14FiLTda', 'EM8BMII6aojhFYg2OPeMuESCCNlWnT2KYggksCAoALQ1rr1J6ZU1nq5Qm2pe', '2022-06-08 11:32:26', '2022-06-23 02:33:59', '1', NULL ),
( '2', 'user pricing', 'upricing', 'adim@adim.com', '$2y$10$7FuOVVnU0bkWEuJcEdRUuuaeAaj4bnlYMVHX29uJNBZRnDysAwYlC', 'TjhcVb0Iy4nUHPdFTtEzaLKZwPE4Y2kbFY5F9tuuEZLND0WuOI8gSS3jq4bw', '2022-06-13 03:24:49', '2022-07-11 19:30:47', '1', NULL ),
( '3', 'root1', 'root1', 'root@o.c', '$2y$10$rcNJWhrI6wTmt8DprHE5P.WRmrWhL6XA9ow6SWi6gjL4hRShoIjVe', 'Ix35GZMwXrC1SVLslnGD3iYxTbzj8x3n1neKAFkqTDdgrlY4OhIv1D65hnPv', '2022-07-01 03:00:50', '2022-07-01 03:00:50', '1', NULL ),
( '4', 'User Maerketing', 'umarketing', 'umarketing@d.a', '$2y$10$ACziBDy//JpXIzfCt1hZm.2CCi8lr3rXjZuHbzbkgAdxlYAtXJtIq', NULL, '2022-07-04 19:32:15', '2022-07-04 19:32:15', '1', NULL ),
( '5', 'ucs', 'ucs', 'ucs@a.c', '$2y$10$47uOIDS4wr4q3IVf4LkR4etxd0uDyBbxfshL3V7DDIFlNYTUy62qG', 'YzC4LH65NvvCEqVG9wtCib6O9VH6E00rzQL2CmiC5EeLaljVzGXWv7pRoUzm', '2022-07-04 19:32:40', '2022-07-11 19:28:46', '1', NULL ),
( '6', 'Teguh Sanyoto', 'eggo', 'teguh@faslogem.co.id', '$2y$10$RDCU9hXfUyOrFx.3moiSVun7dkgdE4fjxK3MrgfaVT854M7M/4P6y', 'klbJRUqfq7ATxV9htXXpbIB682gXqXOrelB4nZ3LbKK1q3rUAOv6hMZdqYiy', '2022-07-05 18:59:03', '2022-07-05 19:07:41', '1', NULL ),
( '7', 'uman', 'uman', 'uman@uman.uman', '$2y$10$FK3DacHa.vVaYFrNy.OqOu40X8BD3EdjaF3EzTWj9qcXsA/yTcig.', NULL, '2022-07-11 20:43:18', '2022-07-11 20:43:18', '1', NULL );
-- ---------------------------------------------------------


-- CREATE INDEX "password_resets_email_index" ------------------
CREATE INDEX `password_resets_email_index` USING BTREE ON `password_resets`( `email` );
-- -------------------------------------------------------------


-- CREATE INDEX "permission_role_role_id_foreign" --------------
CREATE INDEX `permission_role_role_id_foreign` USING BTREE ON `permission_role`( `role_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "role_user_role_id_foreign" --------------------
CREATE INDEX `role_user_role_id_foreign` USING BTREE ON `role_user`( `role_id` );
-- -------------------------------------------------------------


-- CREATE LINK "permission_role_permission_id_foreign" ---------
ALTER TABLE `permission_role`
	ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY ( `permission_id` )
	REFERENCES `permissions`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "permission_role_role_id_foreign" ---------------
ALTER TABLE `permission_role`
	ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY ( `role_id` )
	REFERENCES `roles`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "role_user_role_id_foreign" ---------------------
ALTER TABLE `role_user`
	ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY ( `role_id` )
	REFERENCES `roles`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "role_user_user_id_foreign" ---------------------
ALTER TABLE `role_user`
	ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY ( `user_id` )
	REFERENCES `users`( `id` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


